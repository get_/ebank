import 'package:e_kuteba/app/common/util/exports.dart';

class AppImages {
  ///add app images here
  static final String nowifi = 'nowifi'.image;
  static final String logo = 'logo'.image;
  static final String profile = 'profile'.image;
  static final String nodata = 'nodata'.image;
  static final String pass = 'password'.image;
  static final String logout = 'logout'.image;
}
