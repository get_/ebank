import 'package:flutter/material.dart';

class AppColors {
  const AppColors._();

  static const Color kPrimaryColor = Color(0xFF1976D2);
  static const Color mineShaft = Color(0xFF2B2B2B);
  static const Color doveGray = Color(0xFF646464);
  static const Color lightGray = Color(0xFFD3D3D3);
  static const Color caribbeanGreen = Color(0xFF06C5AC);
  static const Color amaranth = Color(0xFFea435d);
  static const Color black = Colors.black;
  static const Color white = Colors.white;

  static const primaryColor = Color.fromARGB(255, 65, 32, 104);
  static const Primarycolor_one = Color(0xFF694536);
  static const Primarycolor_two = Color(0xFF31742b);

  static const primaryBlueAccentColor = Color(0xFF448AFF);
  static const primaryBlackColor = Colors.black;
  static const secondaryColor = Color(0xFFff9a00);
  static const whiteColor = Colors.white;
  static const greenColor = Color(0xffffffff);
  static const lightGreyColor = Color(0xffEBECF0);
}
