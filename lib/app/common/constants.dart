class Constants {
  static const String BASE_URL = 'https://e.et-erp.com/coop1-api';
  static const timeout = Duration(seconds: 30);
  static const String TOKEN = 'token';
  static const ESCHEMA = 'coop1';
  static const REDIRECT_URI = 'coop1://Redirect';
  static const CLIENT_ID = '';
  static const AUTHORIZE_URL = '$BASE_URL/authorize';
  static const TOKEN_URL = '$BASE_URL/token';
}
