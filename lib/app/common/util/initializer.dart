import 'dart:async';

import 'package:e_kuteba/app/common/storage/storage.dart';
import 'package:e_kuteba/app/common/util/localization.dart';
import 'package:e_kuteba/app/modules/basicDetail/controllers/basic_detail_controller.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/data/api_helper_impl.dart';
import 'package:e_kuteba/app/data/interface_controller/api_interface_controller.dart';
import 'package:e_kuteba/app/modules/widgets/custom_error_widget.dart';

class Initializer {
  static final Initializer instance = Initializer._internal();
  factory Initializer() => instance;
  Initializer._internal();

  void init(VoidCallback runApp) {
    ErrorWidget.builder = (errorDetails) {
      return CustomErrorWidget(
        message: errorDetails.exceptionAsString(),
      );
    };

    runZonedGuarded(() async {
      WidgetsFlutterBinding.ensureInitialized();
      FlutterError.onError = (details) {
        FlutterError.dumpErrorToConsole(details);
        printInfo(info: details.stack.toString());
      };

      await _initServices();
      runApp();
    }, (error, stack) {
      printInfo(info: 'runZonedGuarded: ${stack.toString()}');
    });
  }

  Future<void> _initServices() async {
    try {
      await _initStorage();

      _initScreenPreference();
      _initApis();
    } catch (err) {
      rethrow;
    }
  }

  void _initApis() {
    Get.put<ApiHelper>(ApiHelperImpl());
    Get.put<ApiInterfaceController>(ApiInterfaceController());
    Get.put<BasicDetailController>(BasicDetailController());
    // Get.put<LocaleString>(LocaleString());
    // Get.put<BatchController>(BatchController());
    // Get.put<BluetoothconnectionController>(BluetoothconnectionController());
  }

  Future<void> _initStorage() async {
    await GetStorage.init();
    LocaleString langCtrl = LocaleString();
    GetStorage userdata = GetStorage();
    if (Storage.storage.hasData('token')) {
      Storage.storage.read('token');

      userdata.read("lang") == "English"
          ? langCtrl.changeLocale('English')
          : langCtrl.changeLocale('አማርኛ');
    }
  }

  void _initScreenPreference() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
}
