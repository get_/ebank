// ignore_for_file: non_constant_identifier_names, unnecessary_new, unnecessary_null_comparison

import 'package:get/get.dart';
import 'package:e_kuteba/app/common/util/exports.dart';

class Validators {
  const Validators._();

  static String? validateEmpty(String? v) {
    if (v!.isEmpty) {
      return Strings.fieldCantBeEmpty;
    } else {
      return null;
    }
  }

  static String? validateTEmpty<T>(T? v) {
    if (v == null) {
      return Strings.fieldCantBeEmpty;
    } else {
      return null;
    }
  }

  static String? validateEmail(String? v) {
    if (v!.isEmpty) {
      return Strings.emailCantBeEmpty;
    } else if (!GetUtils.isEmail(v)) {
      return Strings.enterValidEmail;
    } else {
      return null;
    }
  }

  static String? validatePhone(String? v) {
    if (v!.isEmpty) {
      return Strings.fieldCantBeEmpty;
    } else if (v.length != 10) {
      return Strings.enterValidNumber;
    } else {
      return null;
    }
  }

  static String? validateEmailPhone(String? v) {
    if (v!.isEmpty) {
      return Strings.fieldCantBeEmpty;
    } else if (GetUtils.isNumericOnly(v)) {
      return validatePhone(v);
    } else {
      return validateEmail(v);
    }
  }

  static String? validatePassword(String? v) {
    if (v!.isEmpty) {
      return Strings.passwordCantBeEmpty;
    } else if (v.length < 8) {
      return Strings.passwordValidation;
    } else {
      return null;
    }
  }

  static String? validateConfirmPassword(
      String? inputpassword, String newpassword) {
    if (inputpassword!.isEmpty || newpassword.isEmpty) {
      return Strings.passwordCantBeEmpty;
    } else if (inputpassword.length < 8 ||
        newpassword.length < 8 ||
        inputpassword != newpassword) {
      return Strings.confirmPasswordValidation;
    } else {
      return null;
    }
  }

  static String? validateCheckbox({
    bool v = false,
    String error = Strings.checkboxValidation,
  }) {
    if (!v) {
      return error;
    } else {
      return null;
    }
  }

  static String? validator(value) {
    if (value.isEmpty) {
      return 'this field must be filled'.tr;
    } else if (value.contains(RegExp(r'[0-9]'))) {
      return 'numbers are not allowed'.tr;
    }
    return null;
  }

  static String? notEmpty(value) {
    if (value == null || value.isEmpty) {
      return 'this field must be filled'.tr;
    }
    return null;
  }

  static String? phoneValidation(value) {
    if (value!.isEmpty) {
      return "Phone number is required".tr;
    } else if (value.length < 10) {
      return "enter correct phone number".tr;
    } else if (RegExp(r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')
        .hasMatch(value)) {
      return null;
    } else {
      return "incorrect value".tr;
    }
  }

  static String? quantityValidation(value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text'.tr;
    } else if (int.tryParse(value) == null) {
      return "incorrect value".tr;
    } else if (int.parse(value) < 0) {
      return "incorrect value".tr;
    }
  }

  static String? emailValidation(value) {
    if (value!.isEmpty) {
      return 'Email is required'.tr;
    } else if (RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(value)) {
      return null;
    } else {
      return 'Enter valid email'.tr;
    }
  }

  static String? ValidateEmail(value) {
    if (value.isEmpty) {
      return 'this field must be filled'.tr;
    } else if (!GetUtils.isEmail(value)) {
      return 'provide valid Email'.tr;
    }
    return null;
  }

  static String? validateDouble(value) {
    if (value!.isEmpty) {
      return "this field must be filled".tr;
    }
    if (!(double.tryParse(value) != null)) {
      return "incorrect value".tr;
    } else if (double.parse(value) < 0) {
      return "enter accurate balance".tr;
    }
    return null;
  }

  static String? ValidatePassword(val) {
    bool hasUppercase = val.contains(new RegExp(r'[A-Z]'));
    bool hasDigits = val.contains(new RegExp(r'[0-9]'));
    bool hasSpecialCharacters =
        val.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'));
    if (val.length < 6) {
      return 'password must be more than 6 character'.tr;
    } else if (!hasUppercase && hasDigits && hasSpecialCharacters) {
      return 'include uppercase letters (A-z)'.tr;
    } else if (hasUppercase && !hasDigits && hasSpecialCharacters) {
      return 'include digits(0-9)'.tr;
    } else if (hasUppercase && hasDigits && !hasSpecialCharacters) {
      return 'include special characters (.,@,#...)'.tr;
    } else if (hasUppercase && !hasDigits && !hasSpecialCharacters) {
      return 'include digits(0-9) and characters (.,@,#...)'.tr;
    } else if (!hasUppercase && hasDigits && !hasSpecialCharacters) {
      return 'include uppercase (A-Z) and characters (.,@,#...)'.tr;
    } else if (!hasUppercase && !hasDigits && hasSpecialCharacters) {
      return 'include uppercase (A-Z) and  digits(0-9)'.tr;
    } else if (!isPasswordCompliant(val)) {
      return 'digit,uppercase and character are must'.tr;
    } else {
      return null;
    }
  }

  static bool isPasswordCompliant(String password) {
    if (password == null || password.isEmpty) {
      return false;
    }
    bool hasUppercase = password.contains(new RegExp(r'[A-Z]'));
    bool hasDigits = password.contains(new RegExp(r'[0-9]'));
    bool hasSpecialCharacters =
        password.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'));
    return hasDigits & hasUppercase & hasSpecialCharacters;
  }

  static bool isNumeric(String result) {
    if (result == null) {
      return false;
    }
    return double.tryParse(result) != null;
  }
}
