import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/faq_controller.dart';

class FaqView extends GetView<FaqController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('FaqView'),
          centerTitle: true,
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Text(
                  'Dewol FAQ'.toUpperCase(),
                  style: const TextStyle(
                      fontSize: 28, fontWeight: FontWeight.bold),
                ),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: controller.questions.length,
                    itemBuilder: (context, index) {
                      return textButton(controller.questions[index]);
                    })
                // Obx(() => Visibility(
                //     maintainSize: true,
                //     maintainAnimation: true,
                //     maintainState: true,
                //     visible: controller.viewVisible.value,
                //     child: Text(
                //       'Why do we use it?It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
                //     ))),
              ],
            ),
          ),
        ));
  }

  Widget visibleText(text) {
    return Obx(() => Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: controller.viewVisible.value,
        child: Text(text)));
  }

  Widget textButton(text) {
    return TextButton(
      child: Text(text),
      onPressed: () {
        controller.viewVisible.isTrue
            ? controller.hideWidget()
            : controller.showWidget();
      },
    );
  }
}
