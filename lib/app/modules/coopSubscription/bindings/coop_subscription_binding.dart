import 'package:get/get.dart';

import '../controllers/coop_subscription_controller.dart';

class CoopSubscriptionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CoopSubscriptionController>(
      () => CoopSubscriptionController(),
    );
  }
}
