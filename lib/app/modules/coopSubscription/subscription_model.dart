// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

List<Subscription> SubscriptionFromJson(String str) => List<Subscription>.from(
    json.decode(str).map((x) => Subscription.fromJson(x)));

String SubscriptionToJson(List<Subscription> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Subscription {
  Subscription({
    required this.id,
    required this.memberId,
    required this.units,
    required this.totalAmount,
    required this.subscriptionDate,
    required this.serviceFee,
  });

  String id;
  String memberId;
  int units;
  double totalAmount;
  int subscriptionDate;
  double serviceFee;

  factory Subscription.fromJson(Map<String, dynamic> json) => Subscription(
        id: json["id"],
        memberId: json["member_id"],
        units: json["units"],
        totalAmount: json["total_amount"],
        subscriptionDate: json["subscription_date"],
        serviceFee: json["service_fee"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "member_id": memberId,
        "units": units,
        "total_amount": totalAmount,
        "subscription_date": subscriptionDate,
        "service_fee": serviceFee,
      };
}
