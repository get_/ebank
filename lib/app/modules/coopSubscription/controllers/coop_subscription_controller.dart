import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/modules/coopSubscription/subscription_model.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../common/util/connection.dart';
import '../../../data/errors/api_error_model.dart';
import '../../widgets/toast.dart';

class CoopSubscriptionController extends GetxController
    with StateMixin<List<Subscription>> {
  ApiHelper _apiHelper = Get.find();

  @override
  void onInit() {
    fetch();
    super.onInit();
  }

  @override
  void onClose() {}
  RxList<Subscription> subscription = <Subscription>[].obs;

  RxString err = ''.obs;

  String todate(millis) {
    var dt = DateTime.fromMillisecondsSinceEpoch(millis * 1000);
    var d12 = DateFormat('MM/dd/yyyy').format(dt);
    return d12;
  }

  RxBool isNotfetched = true.obs;
  fetch() async {
    isNotfetched.value = true;
    try {
      check().then((intenet) async {
        if (intenet != null && intenet) {
          _apiHelper.getCoopSubscription().then((value) {
            if (value.statusCode!.clamp(200, 299) == value.statusCode) {
              isNotfetched.value = false;
              change(SubscriptionFromJson(value.body),
                  status: RxStatus.success());
              subscription.value = SubscriptionFromJson(value.body);
              subscription.sort((date1, date2) =>
                  date2.subscriptionDate.compareTo(date1.subscriptionDate));
            } else if (value.statusCode!.clamp(500, 599) == value.statusCode) {
              isNotfetched.value = false;
              var err = apiErrorFromJson(value.body);
              toast(err.msgs[0]);
            } else if (value.statusCode!.clamp(400, 499) == value.statusCode) {
              isNotfetched.value = false;
              toast('Resource not found.');
            } else {
              isNotfetched.value = false;
              toast('operation failed');
            }
          }, onError: (err) {
            change(null, status: RxStatus.error(err.toString()));
          });
        } else {
          toast('no connection!');
        }
      });
    } catch (e) {}
  }
}
