// ignore_for_file: annotate_overrides, use_key_in_widget_constructors, prefer_const_constructors, non_constant_identifier_names

import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/coopSubscription/controllers/coop_subscription_controller.dart';
import 'package:e_kuteba/app/modules/coopSubscription/subscription_model.dart';
import 'package:e_kuteba/app/modules/widgets/custom_drawer_widget.dart';
import 'package:e_kuteba/app/modules/widgets/no_data.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../home/controllers/home_controller.dart';
import '../../widgets/custom_loading_widget.dart';

class CoopSubscriptionView extends GetView<CoopSubscriptionController> {
  final controller = Get.put(CoopSubscriptionController());
  final hController = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 238, 238, 238),
      resizeToAvoidBottomInset: false,
      appBar: buildAppBar(context, "Subscription".tr),
      drawer: CustomDrawerWidget(context, hController),
      body: Obx(
        () {
          return controller.isNotfetched.value
              ? Center(child: loadingWidget())
              : controller.subscription.isEmpty
                  ? NoDataPage()
                  : ListView.builder(
                      shrinkWrap: true,
                      itemCount: controller.subscription.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _build_subscription(
                            controller.subscription[index], context, index);
                      },
                    );
        },
      ),
    );
  }

  Container _build_subscription(Subscription coop, context, index) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ExpansionTileCard(
          // expandedTextColor: AppColors.primaryColor,
          baseColor: AppColors.whiteColor,
          expandedColor: Color.fromARGB(255, 238, 241, 235),
          borderRadius: BorderRadius.circular(10),
          title: Text(
            "Member".tr,
            // style: TextStyle(color: AppColors.whiteColor),
          ),
          subtitle: Text(
            'Purchase'.tr + ': ' + (index + 1).toString(),
            // style: const TextStyle(color: AppColors.whiteColor),
          ),
          children: <Widget>[
            const Divider(
              thickness: 1.0,
              height: 1.0,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 0.0,
                    vertical: 8.0,
                  ),
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                              padding: const EdgeInsets.fromLTRB(50, 15, 0, 20),
                              child: Center(
                                child: Table(
                                  children: [
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text(
                                                    'Share units Count'.tr))
                                          ]),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text(
                                                    coop.units.toString())),
                                          ]),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text(
                                                    'Total Paid Amount'.tr))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Text(Utils.numberFormat(
                                                      coop.totalAmount)
                                                  .toString())),
                                        ],
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text(
                                                    'Registration Date'.tr))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Text(controller.todate(
                                                  coop.subscriptionDate))),
                                        ],
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(child: Text('(CRV)'.tr))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Text(
                                                  coop.serviceFee.toString())),
                                        ],
                                      ),
                                    ]),
                                  ],
                                ),
                              ))),
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
