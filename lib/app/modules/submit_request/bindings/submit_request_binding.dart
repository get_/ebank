import 'package:get/get.dart';

import '../controllers/submit_request_controller.dart';

class SubmitRequestBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SubmitRequestController>(
      () => SubmitRequestController(),
    );
  }
}
