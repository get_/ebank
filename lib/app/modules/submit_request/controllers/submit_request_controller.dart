import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/data/api_helper_impl.dart';
import 'package:e_kuteba/app/modules/widgets/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class SubmitRequestController extends GetxController {
  late TextEditingController request;

  final ApiHelper _apiHelperImpl = Get.put<ApiHelper>(ApiHelperImpl());
  @override
  void onInit() {
    super.onInit();
    request = TextEditingController();
  }

  @override
  void onClose() {}
  submit() {
    _apiHelperImpl.submitRequest(request.text).then((value) {
      if (value.statusCode!.clamp(200, 299) != value.statusCode) {
        toast('Request could not be pocessed.');
      }
      toast('Your Request is Recorded!');
      Get.back();
    });
  }

  cancle() {
    Get.back();
  }
}
