import 'package:e_kuteba/app/common/values/app_colors.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/submit_request_controller.dart';

class SubmitRequestView extends GetView<SubmitRequestController> {
  final _key = GlobalKey<FormState>();

  SubmitRequestView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: AppColors.primaryColor,
          title: Text('Submit Request'.tr),
          centerTitle: true,
        ),
        body: SubmitRequest(context));
  }

  Widget SubmitRequest(context) {
    return SafeArea(
        child: Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.6,
            child: Card(
                color: AppColors.lightGray,
                child: Form(
                  key: _key,
                  child: TextFormField(
                    controller: controller.request,
                    keyboardType: TextInputType.multiline,
                    textInputAction: TextInputAction.newline,
                    decoration:  InputDecoration.collapsed(
                        hintText: 'write your request'.tr),
                    minLines: 4,
                    maxLines: 30,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'no case added'.tr;
                      }
                    },
                  ),
                )),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(AppColors.primaryColor)),
                onPressed: () => controller.cancle(),
                child: Text('Cancel'.tr),
              ),
              const SizedBox(
                width: 30,
              ),
              ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(AppColors.primaryColor)),
                onPressed: () {
                  if (_key.currentState!.validate()) {
                    controller.submit();
                  }
                },
                child: Text('Submit'.tr),
              ),
            ],
          )
        ],
      ),
    ));
  }
}
