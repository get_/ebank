import 'package:get/get.dart';

import '../controllers/register_saving_request_controller.dart';

class RegisterSavingRequestBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RegisterSavingRequestController>(
      () => RegisterSavingRequestController(),
    );
  }
}
