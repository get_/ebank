import 'package:e_kuteba/app/modules/widgets/custom_drawer_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../home/controllers/home_controller.dart';
import '../controllers/register_saving_request_controller.dart';

class RegisterSavingRequestView
    extends GetView<RegisterSavingRequestController> {
  final hController = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('RegisterSavingRequestView'),
        centerTitle: true,
      ),
      drawer: CustomDrawerWidget(context, hController),
      body: const Center(
        child: Text(
          'RegisterSavingRequestView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
