// ignore_for_file: prefer_const_constructors

import 'package:e_kuteba/app/modules/beneficiaries/beneficiary_model.dart';
import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/widgets/custom_drawer_widget.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:e_kuteba/app/modules/widgets/no_data.dart';
import '../../../common/util/exports.dart';
import '../../home/controllers/home_controller.dart';
import '../../widgets/custom_loading_widget.dart';
import '../controllers/beneficiaries_controller.dart';

class BeneficiariesView extends GetView<BeneficiariesController> {
  final hController = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, 'Beneficiary'.tr),
      drawer: CustomDrawerWidget(context, hController),
      // backgroundColor: AppColors.white,
      body: Theme(
        data: ThemeData(
            primarySwatch: Colors.orange,
            colorScheme: ColorScheme.light(primary: AppColors.primaryColor)),
        child: Obx(
          () => controller.isNotfetched.value
              ? Center(child: loadingWidget())
              : controller.beneficiary.isEmpty
                  ? NoDataPage()
                  : ListView.builder(
                      itemCount: controller.beneficiary.length,
                      itemBuilder: (context, index) {
                        return _buildBeneficiary(
                            controller.beneficiary[index], context, index);
                      }),
        ),
      ),
    );
  }

  Container _buildBeneficiary(Beneficiary beneficiary, context, index) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ExpansionTileCard(
          expandedTextColor: AppColors.primaryColor,
          baseColor: AppColors.whiteColor,
          // expandedColor: Color(0xffb57e47),
          borderRadius: BorderRadius.circular(10),
          title: Text(
            'Beneficiary'.tr + ' : ' + beneficiary.name,
            style: const TextStyle(),
          ),
          children: <Widget>[
            const Divider(
              thickness: 1.0,
              height: 1.0,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 0.0,
                    vertical: 8.0,
                  ),
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                              padding: const EdgeInsets.fromLTRB(50, 15, 0, 20),
                              child: Center(
                                child: Table(
                                  children: [
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(child: Text('Name '.tr))
                                          ]),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text(beneficiary.name)),
                                          ]),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text('Sex'.tr + ' '))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Text(beneficiary.isMale
                                                  ? 'Male'.tr
                                                  : 'Female'.tr)),
                                        ],
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child:
                                                    Text('Relation type '.tr))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Text(beneficiary
                                                  .relationType
                                                  .toString())),
                                        ],
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text('Date of Birth'.tr))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Text(Utils.toDate(
                                                      beneficiary.birthDate)
                                                  .toString())),
                                        ],
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(child: Text('Remark'.tr))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Text(beneficiary.remark)),
                                        ],
                                      ),
                                    ]),
                                  ],
                                ),
                              ))),
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
