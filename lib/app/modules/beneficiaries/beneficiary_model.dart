// To parse required this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

List<Beneficiary> beneficiaryFromJson(String str) => List<Beneficiary>.from(json.decode(str).map((x) => Beneficiary.fromJson(x)));

String beneficiaryToJson(List<Beneficiary> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Beneficiary {
    Beneficiary({
        required  this.id,
        required this.memberId,
        required this.name,
        required this.isMale,
        required this.relationType,
        required this.birthDate,
        required this.remark,
    });

    String id;
    String memberId;
    String name;
    bool isMale;
    int relationType;
    int birthDate;
    String remark;

    factory Beneficiary.fromJson(Map<String, dynamic> json) => Beneficiary(
        id: json["id"],
        memberId: json["member_id"],
        name: json["name"],
        isMale: json["is_male"],
        relationType: json["relation_type"],
        birthDate: json["birth_date"],
        remark: json["remark"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "member_id": memberId,
        "name": name,
        "is_male": isMale,
        "relation_type": relationType,
        "birth_date": birthDate,
        "remark": remark,
    };
}
