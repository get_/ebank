import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/modules/beneficiaries/beneficiary_model.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../../common/util/connection.dart';
import '../../../data/errors/api_error_model.dart';
import '../../widgets/toast.dart';

class BeneficiariesController extends GetxController
    with StateMixin<List<Beneficiary>> {
  ApiHelper _apiHelper = Get.find();

  @override
  void onInit() {
    // if (Storage.storage.hasData('token')) {
    fetch();
    // }

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  RxBool isNotfetched = false.obs;
  RxList<Beneficiary> beneficiary = <Beneficiary>[].obs;
  fetch() async {
    isNotfetched.value = true;
    try {
      check().then((intenet) async {
        if (intenet != null && intenet) {
          _apiHelper.getBeneficiaries().then((value) {
            if (value.statusCode!.clamp(200, 299) == value.statusCode) {
              isNotfetched.value = false;
              change(beneficiaryFromJson(value.body),
                  status: RxStatus.success());
              beneficiary.value = beneficiaryFromJson(value.body);
            } else if (value.statusCode!.clamp(500, 599) == value.statusCode) {
              isNotfetched.value = false;
              var err = apiErrorFromJson(value.body);
              toast(err.msgs[0]);
            } else if (value.statusCode!.clamp(400, 499) == value.statusCode) {
              isNotfetched.value = false;
              toast('Resource not found.');
            } else {
              isNotfetched.value = false;
              toast('operation failed');
            }
          }, onError: (err) {
            change(null, status: RxStatus.error(err.toString()));
          });
        } else {
          toast('no connection!');
        }
      });
    } catch (e) {}
  }
}
