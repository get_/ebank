import 'package:get/get.dart';

import '../controllers/coop_purchase_controller.dart';

class CoopPurchaseBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CoopPurchaseController>(
      () => CoopPurchaseController(),
    );
  }
}
