import 'dart:convert';

// ignore: non_constant_identifier_names
List<CoopPurchase> SharePurchaseFromJson(String str) => List<CoopPurchase>.from(
    json.decode(str).map((x) => CoopPurchase.fromJson(x)));
// ignore: non_constant_identifier_names
String SharePurchaseToJson(List<CoopPurchase> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CoopPurchase {
  CoopPurchase({
    required this.id,
    required this.memberId,
    required this.units,
    required this.totalAmount,
    required this.bankId,
    required this.purchaseDate,
    required this.serviceFee,
    required this.crv,
  });

  String id;
  String memberId;
  int units;
  double totalAmount;
  String bankId;
  int purchaseDate;
  double serviceFee;
  String crv;

  factory CoopPurchase.fromJson(Map<String, dynamic> json) => CoopPurchase(
        id: json["id"],
        memberId: json["member_id"],
        units: json["units"],
        totalAmount: json["total_amount"],
        bankId: json["bank_id"],
        purchaseDate: json["purchase_date"],
        serviceFee: json["service_fee"],
        crv: json["crv"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "shareholder_id": memberId,
        "units": units,
        "total_amount": totalAmount,
        "bank_id": bankId,
        "purchase_date": purchaseDate,
        "service_fee": serviceFee,
        "crv": crv,
      };
}
