import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/modules/coopPurchase/coop_purchase_model.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

import '../../../common/util/connection.dart';
import '../../../data/errors/api_error_model.dart';
import '../../widgets/toast.dart';

class CoopPurchaseController extends GetxController
    with StateMixin<List<CoopPurchase>> {
  final count = 0.obs;

  ApiHelper _apiHelper = Get.find();

  @override
  void onInit() {
    fetch();
    super.onInit();
  }

  @override
  void onClose() {}
  String todate(millis) {
    var dt = DateTime.fromMillisecondsSinceEpoch(millis * 1000);
    var d12 = DateFormat('MM/dd/yyyy').format(dt);

    return d12;
  }

  RxList<CoopPurchase> sharePurchase = <CoopPurchase>[].obs;

  RxBool isNotfetched = true.obs;
  fetch() async {
    isNotfetched.value = true;
    try {
      check().then((intenet) async {
        if (intenet != null && intenet) {
          _apiHelper.getCoopPurchase().then((value) {
            printInfo(info: value.body);
            if (value.statusCode!.clamp(200, 299) == value.statusCode) {
              isNotfetched.value = false;
              change(SharePurchaseFromJson(value.body),
                  status: RxStatus.success());
              sharePurchase.value = SharePurchaseFromJson(value.body);
              sharePurchase.sort((date1, date2) =>
                  date2.purchaseDate.compareTo(date1.purchaseDate));
            } else if (value.statusCode!.clamp(500, 599) == value.statusCode) {
              isNotfetched.value = false;
              var err = apiErrorFromJson(value.body);
              toast(err.msgs[0]);
            } else if (value.statusCode!.clamp(400, 499) == value.statusCode) {
              isNotfetched.value = false;
              toast('Resource not found.');
            } else {
              isNotfetched.value = false;
              toast('operation failed');
            }
          }, onError: (err) {
            change(null, status: RxStatus.error(err.toString()));
          });
        } else {
          toast('no connection!');
        }
      });
    } catch (e) {}
  }
}
