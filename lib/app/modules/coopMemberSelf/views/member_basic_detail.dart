// // ignore_for_file: annotate_overrides, prefer_const_constructors

// import 'package:e_kuteba/app/common/util/exports.dart';
// import 'package:e_kuteba/app/commonWidgets/appbar.dart';
// import 'package:e_kuteba/app/commonWidgets/drawer/drawer.dart';
// import 'package:e_kuteba/app/commonWidgets/horizontal_list.dart';
// import 'package:e_kuteba/app/modules/coopMemberSelf/controllers/coop_member_self_controller.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:google_fonts/google_fonts.dart';

// class BasicDetail extends GetView<CoopMemberSelfController> {
//   final controller = Get.put(CoopMemberSelfController());

//   BasicDetail({Key? key}) : super(key: key);
//   // final hom = Get.put(HomeController());

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: buildAppBar(context, 'Basic Member Detail'),
//         drawer: CustomDrawer(),
//         body: controller.obx((state) {
//           return state!.firstName.isEmpty
//               ? Center(child: CircularProgressIndicator())
//               : SingleChildScrollView(child: horizontalList(4));
//         }));
//   }

//   Widget customVerticalCard(
//     BuildContext context,
//     member,
//   ) {
//     return Container(
//       margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           const Text(
//             'Basic Member Information',
//             style: TextStyle(color: Color(0xffff9a00), fontSize: 24),
//           ),
//           const SizedBox(
//             height: 14,
//           ),
//           Text(
//             'Name',
//             style: GoogleFonts.poppins(
//                 textStyle: const TextStyle(
//               fontSize: 20,
//               color: AppColors.primaryColor,
//             )),
//           ),
//           Text(member.firstName + ' ' + member.middleName,
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text('Date of birth',
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                 fontSize: 20,
//                 color: AppColors.primaryColor,
//               ))),
//           Text(member.birthDate.toString(),
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text('Natinality',
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                 fontSize: 20,
//                 color: AppColors.primaryColor,
//               ))),
//           Text(member.nationality,
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text('Email',
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                 fontSize: 20,
//                 color: AppColors.primaryColor,
//               ))),
//           Text(member.email,
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text('Gender',
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                 fontSize: 20,
//                 color: AppColors.primaryColor,
//               ))),
//           Text(member.isMale ? "Male" : "Female",
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text('Address',
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                 fontSize: 20,
//                 color: AppColors.primaryColor,
//               ))),
//           Text(member.address,
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text('Work Exprience',
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                 fontSize: 20,
//                 color: AppColors.primaryColor,
//               ))),
//           Text(member.workExperience,
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text('Family Size',
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                 fontSize: 20,
//                 color: AppColors.primaryColor,
//               ))),
//           Text(member.familySize.toString(),
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Column(
//                 children: [
//                   Text('Telephone',
//                       style: GoogleFonts.poppins(
//                           textStyle: const TextStyle(
//                         fontSize: 20,
//                         color: AppColors.primaryColor,
//                       ))),
//                   Text(member.telephone,
//                       style: GoogleFonts.poppins(
//                           textStyle: const TextStyle(
//                               fontSize: 20,
//                               color: AppColors.primaryColor,
//                               fontWeight: FontWeight.bold))),
//                 ],
//               ),
//               const Icon(
//                 Icons.call,
//                 color: Color(0xffff9a00),
//               )
//             ],
//           )
//         ],
//       ),
//     );
//   }
// }
