// ignore_for_file: annotate_overrides

import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/modules/basicDetail/controllers/basic_detail_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../basicDetail/member_model.dart';
import '../../widgets/appbar.dart';

class MoreInfo extends GetView<BasicDetailController> {
  final controller = Get.put(BasicDetailController());

  MoreInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBar(context, 'More'),
        body: Obx(() {
          return SingleChildScrollView(
              child: customVerticalCard(context, controller.member.value));
        }));
  }

  Widget customVerticalCard(
    BuildContext context,
    Member member,
  ) {
    return Container(
      margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'More Information',
            style: TextStyle(color: Color(0xffff9a00), fontSize: 24),
          ),
          const SizedBox(
            height: 14,
          ),
          Text(
            'Monthly Income',
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(Utils.numberFormat(member.monthlyIncome).toString(),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text('Immediate Contact',
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                fontSize: 20,
                color: AppColors.primaryColor,
              ))),
          Text(member.immediateContact,
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text('Referral',
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                fontSize: 20,
                color: AppColors.primaryColor,
              ))),
          Text(member.referral,
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text('Saving Book No',
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                fontSize: 20,
                color: AppColors.primaryColor,
              ))),
          Text(member.savingBookNo,
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
