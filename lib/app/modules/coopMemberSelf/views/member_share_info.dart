import 'package:e_kuteba/app/modules/basicDetail/controllers/basic_detail_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../common/util/exports.dart';
import '../../basicDetail/member_model.dart';
import '../../widgets/appbar.dart';


class MemberShareInfo extends StatelessWidget {
  final controller = Get.put(BasicDetailController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBar(context, 'Member Share Info'),
        body: Obx(() {
          return SingleChildScrollView(
              child: customVerticalCard(context, controller.member.value));
        }));
  }

  Widget customVerticalCard(
    BuildContext context,
    Member member,
  ) {
    return Container(
      margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Total Subscribed Amount',
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(Utils.numberFormat(member.subscriptionTotal).toString(),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Total Paid Amount',
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(Utils.numberFormat(member.totalPurchase).toString(),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Share Units',
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(member.units.toString(),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
