// // ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors

// import 'dart:ui';

// import 'package:e_kuteba/app/common/util/exports.dart';
// import 'package:e_kuteba/app/commonWidgets/appbar.dart';
// import 'package:e_kuteba/app/commonWidgets/drawer/drawer.dart';
// import 'package:e_kuteba/app/modules/widgets/custom_drawer_widget.dart';
// import 'package:e_kuteba/app/modules/widgets/no_connection.dart';
// import 'package:flutter/material.dart';

// import 'package:get/get.dart';
// import 'package:google_fonts/google_fonts.dart';

// import '../controllers/coop_member_self_controller.dart';

// class CoopMemberSelfView extends GetView<CoopMemberSelfController> {
//   @override
//   final controller = Get.put(CoopMemberSelfController());

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: buildAppBar(context, 'Basic Member Detail'),
//       drawer: CustomDrawerWidget(context),
//       body: Container(
//         height: MediaQuery.of(context).size.height,
//         width: MediaQuery.of(context).size.width,
//         child: Obx(
//           () => controller.isNotfetched.value
//               ? Center(
//                   child: CircularProgressIndicator(),
//                 )
//               : controller.status_code.value == 500
//                   ? Center(
//                       child: InternalServerError(),
//                     )
//                   : controller.member.value.firstName.isEmpty
//                       ? Center(
//                           child: NoConenction(),
//                         )
//                       : Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Text(
//                               'Name',
//                               style: GoogleFonts.poppins(
//                                   textStyle: const TextStyle(
//                                 fontSize: 20,
//                                 color: AppColors.primaryColor,
//                               )),
//                             ),
//                             Text(
//                                 controller.member.value.firstName +
//                                     ' ' +
//                                     controller.member.value.middleName,
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                         fontSize: 20,
//                                         color: AppColors.primaryColor,
//                                         fontWeight: FontWeight.bold))),
//                             const SizedBox(
//                               height: 10,
//                             ),
//                             Text('Date of birth',
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                   fontSize: 20,
//                                   color: AppColors.primaryColor,
//                                 ))),
//                             Text(controller.member.value.birthDate.toString(),
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                         fontSize: 20,
//                                         color: AppColors.primaryColor,
//                                         fontWeight: FontWeight.bold))),
//                             const SizedBox(
//                               height: 10,
//                             ),
//                             Text('Natinality',
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                   fontSize: 20,
//                                   color: AppColors.primaryColor,
//                                 ))),
//                             Text(controller.member.value.nationality,
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                         fontSize: 20,
//                                         color: AppColors.primaryColor,
//                                         fontWeight: FontWeight.bold))),
//                             const SizedBox(
//                               height: 10,
//                             ),
//                             Text('Email',
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                   fontSize: 20,
//                                   color: AppColors.primaryColor,
//                                 ))),
//                             Text(controller.member.value.email,
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                         fontSize: 20,
//                                         color: AppColors.primaryColor,
//                                         fontWeight: FontWeight.bold))),
//                             const SizedBox(
//                               height: 10,
//                             ),
//                             Text('Gender',
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                   fontSize: 20,
//                                   color: AppColors.primaryColor,
//                                 ))),
//                             Text(
//                                 controller.member.value.isMale
//                                     ? "Male"
//                                     : "Female",
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                         fontSize: 20,
//                                         color: AppColors.primaryColor,
//                                         fontWeight: FontWeight.bold))),
//                             const SizedBox(
//                               height: 10,
//                             ),
//                             Text('Address',
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                   fontSize: 20,
//                                   color: AppColors.primaryColor,
//                                 ))),
//                             Text(controller.member.value.address,
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                         fontSize: 20,
//                                         color: AppColors.primaryColor,
//                                         fontWeight: FontWeight.bold))),
//                             const SizedBox(
//                               height: 10,
//                             ),
//                             Text('Work Exprience',
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                   fontSize: 20,
//                                   color: AppColors.primaryColor,
//                                 ))),
//                             Text(controller.member.value.workExperience,
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                         fontSize: 20,
//                                         color: AppColors.primaryColor,
//                                         fontWeight: FontWeight.bold))),
//                             const SizedBox(
//                               height: 10,
//                             ),
//                             Text('Family Size',
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                   fontSize: 20,
//                                   color: AppColors.primaryColor,
//                                 ))),
//                             Text(controller.member.value.familySize.toString(),
//                                 style: GoogleFonts.poppins(
//                                     textStyle: const TextStyle(
//                                         fontSize: 20,
//                                         color: AppColors.primaryColor,
//                                         fontWeight: FontWeight.bold))),
//                             const SizedBox(
//                               height: 10,
//                             ),
//                             Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: [
//                                 Column(
//                                   children: [
//                                     Text(
//                                       'Telephone',
//                                       style: GoogleFonts.poppins(
//                                         textStyle: const TextStyle(
//                                           fontSize: 20,
//                                           color: AppColors.primaryColor,
//                                         ),
//                                       ),
//                                     ),
//                                     Text(controller.member.value.telephone,
//                                         style: GoogleFonts.poppins(
//                                             textStyle: const TextStyle(
//                                                 fontSize: 20,
//                                                 color: AppColors.primaryColor,
//                                                 fontWeight: FontWeight.bold))),
//                                   ],
//                                 ),
//                                 const Icon(
//                                   Icons.call,
//                                   color: Color(0xffff9a00),
//                                 )
//                               ],
//                             )
//                           ],
//                         ),
//         ),
//       ),
//     );
//   }
// }
