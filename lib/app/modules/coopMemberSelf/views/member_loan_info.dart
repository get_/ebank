// ignore_for_file: annotate_overrides

import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/modules/basicDetail/controllers/basic_detail_controller.dart';
import 'package:e_kuteba/app/modules/coopMemberSelf/controllers/coop_member_self_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../basicDetail/member_model.dart';
import '../../widgets/appbar.dart';

class LoanInfo extends GetView<BasicDetailController> {
  final controller = Get.put(BasicDetailController());

  LoanInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBar(context, ' Loan Info'),
        body: Obx(() {
          return SingleChildScrollView(
              child: customVerticalCard(context, controller.member.value));
        }));
  }

  Widget customVerticalCard(
    BuildContext context,
    Member member,
  ) {
    return Container(
      margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Loan Amount',
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                    fontSize: 16,
                    color: AppColors.primaryColor,
                  ))),
              Text(Utils.numberFormat(member.approvedAmount).toString(),
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                          fontSize: 16,
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.bold))),
              const SizedBox(
                height: 10,
              ),
              Text('Remaining Amount',
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                    fontSize: 16,
                    color: AppColors.primaryColor,
                  ))),
              Text('To be Done...',
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                          fontSize: 16,
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.bold))),
              const SizedBox(
                height: 10,
              ),
              const SizedBox(
                height: 10,
              ),
              Text('Monthly Payment',
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                    fontSize: 16,
                    color: AppColors.primaryColor,
                  ))),
              Text(Utils.numberFormat(member.monthlyPayment).toString(),
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                          fontSize: 16,
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.bold))),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.4,
            width: 2,
            color: AppColors.doveGray,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'Deposit Amount',
                style: GoogleFonts.poppins(
                    textStyle: const TextStyle(
                  fontSize: 16,
                  color: AppColors.primaryColor,
                )),
              ),
              Text(Utils.numberFormat(member.depositAmount).toString(),
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                          fontSize: 16,
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.bold))),
              const SizedBox(
                height: 10,
              ),
              Text('Interest Amount',
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                    fontSize: 16,
                    color: AppColors.primaryColor,
                  ))),
              Text(Utils.numberFormat(member.interesetAmount).toString(),
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                          fontSize: 16,
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.bold))),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
