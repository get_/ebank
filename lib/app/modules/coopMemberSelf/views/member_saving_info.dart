// // ignore_for_file: annotate_overrides

// import 'package:e_kuteba/app/common/util/exports.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:google_fonts/google_fonts.dart';

// import '../../basicDetail/controllers/basic_detail_controller.dart';
// import '../../basicDetail/member_model.dart';
// import '../../widgets/appbar.dart';

// class SavingInfo extends GetView<BasicDetailController> {
//   final controller = Get.put(BasicDetailController());

//   SavingInfo({Key? key}) : super(key: key);
//   // final hom = Get.put(HomeController());

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: buildAppBar(context, 'Member Saving Info'),
//         body: Obx(() {
//           return SingleChildScrollView(
//               child: customVerticalCard(context, controller.member.value));
//         }));
//   }

//   Widget customVerticalCard(
//     BuildContext context,
//     Member member,
//   ) {
//     return Container(
//       margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           // const Text(
//           //   'Member Saving Information',
//           //   style: TextStyle(color: Color(0xffff9a00), fontSize: 24),
//           // ),
//           // const SizedBox(
//           //   height: 14,
//           // ),
//           Text(
//             'Mandatory Saving',
//             style: GoogleFonts.poppins(
//                 textStyle: const TextStyle(
//               fontSize: 20,
//               color: AppColors.primaryColor,
//             )),
//           ),
//           Text(Utils.numberFormat(member.mandatorySaving).toString(),
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text(
//             'Voluntary Saving',
//             style: GoogleFonts.poppins(
//                 textStyle: const TextStyle(
//               fontSize: 20,
//               color: AppColors.primaryColor,
//             )),
//           ),
//           Text(Utils.numberFormat(member.voluntarySaving).toString(),
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text(
//             'Child Saving',
//             style: GoogleFonts.poppins(
//                 textStyle: const TextStyle(
//               fontSize: 20,
//               color: AppColors.primaryColor,
//             )),
//           ),
//           Text(Utils.numberFormat(member.childSaving).toString(),
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text(
//             'Time Limit Deposit',
//             style: GoogleFonts.poppins(
//                 textStyle: const TextStyle(
//               fontSize: 20,
//               color: AppColors.primaryColor,
//             )),
//           ),
//           Text(Utils.numberFormat(member.timeLimitedSaving).toString(),
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text(
//             'Saving Amount',
//             style: GoogleFonts.poppins(
//                 textStyle: const TextStyle(
//               fontSize: 20,
//               color: AppColors.primaryColor,
//             )),
//           ),
//           Text(Utils.numberFormat(member.savingAmount).toString(),
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//           Text(
//             'Interest',
//             style: GoogleFonts.poppins(
//                 textStyle: const TextStyle(
//               fontSize: 20,
//               color: AppColors.primaryColor,
//             )),
//           ),
//           Text(Utils.numberFormat(member.interesetAmount).toString(),
//               style: GoogleFonts.poppins(
//                   textStyle: const TextStyle(
//                       fontSize: 20,
//                       color: AppColors.primaryColor,
//                       fontWeight: FontWeight.bold))),
//           const SizedBox(
//             height: 10,
//           ),
//         ],
//       ),
//     );
//   }
// }
