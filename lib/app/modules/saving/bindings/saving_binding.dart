import 'package:get/get.dart';

import '../controllers/saving_controller.dart';

class SavingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SavingController>(
      () => SavingController(),
    );
  }
}
