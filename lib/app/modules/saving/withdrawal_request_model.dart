// To parse this JSON data, do
//
//     final withdrawalRequest = withdrawalRequestFromJson(jsonString);

import 'dart:convert';

WithdrawalRequest withdrawalRequestFromJson(String str) =>
    WithdrawalRequest.fromJson(json.decode(str));

String withdrawalRequestToJson(WithdrawalRequest data) =>
    json.encode(data.toJson());

class WithdrawalRequest {
  WithdrawalRequest({
    required this.status,
    required this.remark,
    required this.savingTypeId,
    required this.memberId,
    required this.amount,
    required this.date,
  });

  int status;
  String remark;
  String savingTypeId;
  String memberId;
  double amount;
  double date;

  factory WithdrawalRequest.fromJson(Map<String, dynamic> json) =>
      WithdrawalRequest(
        status: json["status"],
        remark: json["remark"],
        savingTypeId: json["saving_type_id"],
        memberId: json["member_id"],
        amount: json["amount"],
        date: json["date"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "remark": remark,
        "saving_type_id": savingTypeId,
        "member_id": memberId,
        "amount": amount,
        "date": date,
      };
}
