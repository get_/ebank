import 'dart:convert';

List<Saving> savingFromJson(String str) => List<Saving>.from(json.decode(str).map((x) => Saving.fromJson(x)));

String savingToJson(List<Saving> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Saving {
    Saving({
        required this.id,
        required this.memberId,
        required this.savingAmount,
        required this.bankId,
        required this.transactionReference,
        required this.regDate,
        required this.remark,
        required this.savingTypeId,
    });

    String id;
    String memberId;
    double savingAmount;
    String bankId;
    String transactionReference;
    int regDate;
    String remark;
    String savingTypeId;

    factory Saving.fromJson(Map<String, dynamic> json) => Saving(
        id: json["id"],
        memberId: json["member_id"],
        savingAmount: json["saving_amount"],
        bankId: json["bank_id"],
        transactionReference: json["transaction_reference"],
        regDate: json["reg_date"],
        remark: json["remark"],
        savingTypeId: json["saving_type_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "member_id": memberId,
        "saving_amount": savingAmount,
        "bank_id": bankId,
        "transaction_reference": transactionReference,
        "reg_date": regDate,
        "remark": remark,
        "saving_type_id": savingTypeId,
    };
}
