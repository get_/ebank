// ignore_for_file: unnecessary_null_comparison

import 'package:e_kuteba/app/common/util/connection.dart';
import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/data/errors/api_error_model.dart';
import 'package:e_kuteba/app/modules/basicDetail/controllers/basic_detail_controller.dart';
import 'package:e_kuteba/app/modules/saving/saving_model.dart';
import 'package:e_kuteba/app/modules/saving/withdrawal_request_model.dart';
import 'package:e_kuteba/app/modules/saving_request/saving_types_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

import '../../saving_request/banks_model.dart';
import '../../widgets/toast.dart';

class SavingController extends GetxController with StateMixin<List<Saving>> {
  ApiHelper _apiHelper = Get.find();
  final ScrollController scrollController = ScrollController();
  final _selfController = Get.find<BasicDetailController>();
  @override
  void onInit() {
    fetch();
    getbanks();
    getSavingTypes();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  int page = 1;
  int maxPage = 0;

  List<Saving> savings = [];
  RxString bankTypeId = ''.obs;
  RxList<Bank> banktype = <Bank>[].obs;

  void getbanks() async {
    await _apiHelper.getBanks().then((value) {
      banktype.value = banksFromJson(value.body).data;
    });
  }

  String getbankTypeId(val) {
    var vv = '';
    for (var item in banktype) {
      if (item.id == val) {
        bankTypeId.value = item.name;
        vv = item.name;
      }
    }
    return vv;
  }

  String todate(millis) {
    var dt = DateTime.fromMillisecondsSinceEpoch(millis * 1000);

    var d12 = DateFormat('MM/dd/yyyy').format(dt);

    return d12;
  }

  RxBool isNotfetched = true.obs;
  RxInt status_code = 0.obs;
  RxList<Saving> saving = <Saving>[].obs;
  fetch() async {
    try {
      check().then((intenet) async {
        if (intenet != null && intenet) {
          _apiHelper.getSaving(page).then((value) {
            isNotfetched.value = false;
            if (value.statusCode!.clamp(200, 299) == value.statusCode) {
              isNotfetched.value = false;
              status_code.value = 200;
              change(savingFromJson(value.body), status: RxStatus.success());
              saving.value = savingFromJson(value.body);

              saving.sort(
                  (date1, date2) => date2.regDate.compareTo(date1.regDate));
            } else if (value.statusCode!.clamp(500, 599) == value.statusCode) {
              status_code.value = 500;
              isNotfetched.value = false;
              var err = apiErrorFromJson(value.body);
              toast(err.msgs[0] == '' ? 'Internal Server Error' : err.msgs[0]);
            } else if (value.statusCode!.clamp(400, 499) == value.statusCode) {
              status_code.value = 500;
              isNotfetched.value = false;
              toast('Resource not found.');
            } else {
              isNotfetched.value = false;
              toast('operation failed');
            }
          }, onError: (err) {
            change(null, status: RxStatus.error(err.toString()));
          });
        } else {
          toast('no connection!');
        }
      });
    } catch (e) {}
  }

  SavingTypes types = SavingTypes(data: [], total: 0);
  getSavingTypes() {
    _apiHelper.getSavingTypes().then((value) {
      if (value.statusCode == 200) {
        types = savingTypesFromJson(value.body);
      }
    });
  }

  RxString savingTypeId = ''.obs;
  String getSavingTypeId(val) {
    var typeName = '';
    for (var item in types.data) {
      if (item.id == val) {
        savingTypeId.value = item.name;
        typeName = item.name;
      }
    }
    return typeName;
  }

  RxString startDate = 'Date'.tr.obs;
  double startDateToEpoch = 0;
  Rx<DateTime> currentDate = DateTime.now().obs;
  Future<void> selectStartDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: DateTime(2020),
        lastDate: DateTime(2050),
        builder: (context, picker) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: const ColorScheme.dark(
                primary: AppColors.secondaryColor,
                onPrimary: Colors.white,
                surface: AppColors.secondaryColor,
                onSurface: Colors.white,
              ),
              dialogBackgroundColor: AppColors.primaryColor,
            ),
            child: picker!,
          );
        }).then((selectedDate) {
      if (selectedDate != null) {
        var parsedDate = DateTime.parse(selectedDate.toString());
        startDate.value = DateFormat("dd-MM-yyyy").format(parsedDate);
        var start_date = (selectedDate).millisecondsSinceEpoch;
        var start = (start_date / 1000).round();
        startDateToEpoch = start as double;
      }
    });
  }

  RxBool sending = false.obs;
  sendRequest(remark, amount, date) {
    sending.value = true;
    WithdrawalRequest wr = WithdrawalRequest(
        status: 100,
        remark: remark,
        savingTypeId: '',
        memberId: _selfController.member.value.id!,
        amount: double.parse(amount),
        date: date);
    _apiHelper.withdrawalRequest(wr).then((value) {
      ;
      if (value.statusCode == 204) {
        toast('Succesfully submited.');
        Get.back();
      } else if (value.statusCode == 404) {
        toast('something went wrong, please try again');
      } else if (value.statusCode == 500) {
        toast('Server error, please try again');
      } else if (value.statusCode == 503) {
        toast('Service Unavailable');
      } else {
        var err = apiErrorFromJson(value.body);
        toast(err.msgs[0]==''?'something went wrong':err.msgs[0]);
      }
      sending.value = false;
    });
  }
}
