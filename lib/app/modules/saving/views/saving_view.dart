// ignore_for_file: prefer_const_constructors



import 'package:e_kuteba/app/modules/widgets/no_data.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:e_kuteba/app/modules/saving/saving_model.dart';
import 'package:e_kuteba/app/modules/widgets/custom_drawer_widget.dart';
import 'package:e_kuteba/app/modules/widgets/custom_loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../common/util/validators.dart';
import '../../../common/values/app_colors.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/saving_controller.dart';

class SavingView extends GetView<SavingController> {
  @override
  final controller = Get.put(SavingController());
  final hController = Get.put(HomeController());
  SavingView({Key? key}) : super(key: key);
  TextEditingController amount = TextEditingController();
  TextEditingController remark = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  AutovalidateMode autoValidate = AutovalidateMode.disabled;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: ElevatedButton(
                onPressed: () async {
                  await showModalBottomSheet(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.vertical(
                              top: Radius.circular(20.0))),
                      backgroundColor: Colors.white,
                      context: context,
                      isScrollControlled: true,
                      builder: (context) {
                        return withdrawalForm(context);
                      });
                },
                child: Row(
                  children: [
                    Icon(Icons.attach_money),
                    Text('Withdraw'.tr),
                  ],
                )),
          ),
        ],
        iconTheme: IconThemeData(
            color: Color.fromARGB(255, 255, 255, 255), opacity: 1),
        elevation: 0,
        backgroundColor: AppColors.primaryColor,
        title: Text(
          'Saving'.tr,
          style: TextStyle(color: AppColors.whiteColor),
        ),
      ),
      drawer: CustomDrawerWidget(context, hController),
      body: Obx(
        () {
          return controller.isNotfetched.value
              ? Center(child: loadingWidget())
              : controller.saving.isEmpty
                  ? NoDataPage()
                  : ListView.builder(
                      shrinkWrap: true,
                      itemCount: controller.saving.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _build_subscription(
                            controller.saving[index], context, index);
                      },
                    );
        },
      ),
      // floatingActionButton: FloatingActionButton(
      //   shape: Shap,
      //   onPressed: () {},
      //   child: Text('Withdrow'),
      // ),
    );
  }

  Container _build_subscription(Saving coop, context, index) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 15),
        child: ExpansionTileCard(
          // expandedTextColor: AppColors.primaryColor,
          baseColor: AppColors.whiteColor,
          expandedColor: Color.fromARGB(255, 238, 241, 235),
          borderRadius: BorderRadius.circular(10),
          title: Text(
            "Saving".tr,
            // style: TextStyle(color: AppColors.whiteColor),
          ),
          subtitle: Text(
            'Saving Number'.tr + ': ' + (index + 1).toString(),
            // style: const TextStyle(color: AppColors.whiteColor),
          ),
          children: <Widget>[
            const Divider(
              thickness: 1.0,
              height: 1.0,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 0.0,
                    vertical: 8.0,
                  ),
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                              padding: const EdgeInsets.fromLTRB(50, 15, 0, 20),
                              child: Center(
                                child: Table(
                                  children: [
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(child: Text('Bank'.tr))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                            child: Text(controller
                                                .getbankTypeId(coop.bankId)),
                                          )
                                        ],
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text('Saving Type'.tr))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Text(
                                                  controller.getSavingTypeId(
                                                      coop.savingTypeId))),
                                        ],
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text('Saving Amount'.tr))
                                          ]),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text(coop.savingAmount
                                                    .toString())),
                                          ]),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text(
                                                    'Transaction Reference'.tr))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Text(
                                                  coop.transactionReference)),
                                        ],
                                      ),
                                    ]),
                                    TableRow(children: [
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Flexible(
                                                child: Text(
                                                    'Registration Date'.tr))
                                          ]),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Flexible(
                                              child: Text(controller
                                                  .todate(coop.regDate))),
                                        ],
                                      ),
                                    ])
                                  ],
                                ),
                              ))),
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget withdrawalForm(context) {
    return Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.55,
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              autovalidateMode: autoValidate,
              child: Column(children: [
                Text(
                  'New Withdrawal'.tr,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: () {
                      controller.selectStartDate(Get.context!);
                    },
                    child: Container(
                      height: 45,
                      width: MediaQuery.of(context).size.width * 0.9,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.grey.shade200,
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            child: Icon(Icons.date_range),
                          ),
                          Obx(() => Text(controller.startDate.value))
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Padding(
                  padding: EdgeInsets.all(12),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    controller: amount,
                    decoration: InputDecoration(
                      fillColor: Colors.grey,
                      border: OutlineInputBorder(),
                      labelText: 'amount'.tr,
                    ),
                    validator: (value) {
                      return Validators.validateDouble(value);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(12),
                  child: TextFormField(
                    keyboardType: TextInputType.multiline,
                    controller: remark,
                    maxLines: 2,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'remark'.tr,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                      onPressed: () {
                        amount.clear();
                        remark.clear();

                        Get.back();
                      },
                      child: Text('Cancel'.tr),
                    ),
                    Container(
                      height: 40.0,
                      margin: EdgeInsets.all(10),
                      child: RaisedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            controller.sendRequest(remark.text, amount.text,
                                controller.startDateToEpoch);
                            amount.clear();
                            remark.clear();

                            // Get.back();
                          } else {
                            autoValidate = AutovalidateMode.onUserInteraction;
                          }
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                        padding: EdgeInsets.all(0.0),
                        child: Ink(
                          decoration: BoxDecoration(
                              color: AppColors.primaryColor,
                              // gradient: LinearGradient(
                              //   colors: const [
                              //     AppColors.primaryColor,
                              //     Color.fromARGB(255, 206, 203, 209),
                              //   ],
                              //   begin: Alignment.centerLeft,
                              //   end: Alignment.centerRight,
                              // ),
                              borderRadius: BorderRadius.circular(30.0)),
                          child: Container(
                            constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width * 0.3,
                                minHeight: 40.0),
                            alignment: Alignment.center,
                            child: Text(
                              "Submit".tr,
                              textAlign: TextAlign.center,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 15),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ]),
            ),
          ),
        ));
  }
}
