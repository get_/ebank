// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

LoanRequestModel loanRequestFromJson(String? str) => LoanRequestModel.fromJson(json.decode(str!));

String? loanRequestToJson(LoanRequestModel data) => json.encode(data.toJson());

class LoanRequestModel {
    LoanRequestModel({
        this.carLicensePlateNumber,
        this.carOwnerFirstName,
        this.carOwnerLastName,
        this.carOwnerMiddleName,
        this.currentCarPrice,
        this.district,
        this.durationMonth,
        this.externalWarranty,
        this.fullInsuranceMoney,
        this.guarantorFirstName,
        this.guarantorLastName,
        this.guarantorMiddleName,
        this.guarantorMonthlyIncome,
        this.guarantorWorkplace,
        this.homeService,
        this.houseNo,
        this.householderFirstName,
        this.householderLastName,
        this.householderMiddleName,
        this.institutionSharesPurchased,
        this.loanAmount,
        this.loanTypeId,
        this.memberId,
        this.memberWarranty,
        this.region,
        this.remark,
        this.requestDate,
        this.shareAmount,
        this.sharePrice,
        this.shareholderFirstName,
        this.shareholderLastName,
        this.shareholderMiddleName,
        this.spouseFirstName,
        this.spouseLastName,
        this.spouseMiddleName,
        this.spouseMonthlyIncome,
        this.spouseWorkplace,
        this.status,
        this.subcity,
        this.totalPurchase,
        this.typeOfCar,
    });

    String? carLicensePlateNumber;
    String? carOwnerFirstName;
    String? carOwnerLastName;
    String? carOwnerMiddleName;
    int? currentCarPrice;
    String? district;
    int? durationMonth;
    int? externalWarranty;
    int? fullInsuranceMoney;
    String? guarantorFirstName;
    String? guarantorLastName;
    String? guarantorMiddleName;
    int? guarantorMonthlyIncome;
    String? guarantorWorkplace;
    int? homeService;
    String? houseNo;
    String? householderFirstName;
    String? householderLastName;
    String? householderMiddleName;
    String? institutionSharesPurchased;
    int? loanAmount;
    String? loanTypeId;
    String? memberId;
    String? memberWarranty;
    int? region;
    String? remark;
    double? requestDate;
    int? shareAmount;
    int? sharePrice;
    String? shareholderFirstName;
    String? shareholderLastName;
    String? shareholderMiddleName;
    String? spouseFirstName;
    String? spouseLastName;
    String? spouseMiddleName;
    int? spouseMonthlyIncome;
    String? spouseWorkplace;
    int? status;
    String? subcity;
    int? totalPurchase;
    String? typeOfCar;

    factory LoanRequestModel.fromJson(Map<String?, dynamic> json) => LoanRequestModel(
        carLicensePlateNumber: json["car_license_plate_number"],
        carOwnerFirstName: json["car_owner_first_name"],
        carOwnerLastName: json["car_owner_last_name"],
        carOwnerMiddleName: json["car_owner_middle_name"],
        currentCarPrice: json["current_car_price"],
        district: json["district"],
        durationMonth: json["duration_month"],
        externalWarranty: json["external_warranty"],
        fullInsuranceMoney: json["full_insurance_money"],
        guarantorFirstName: json["guarantor_first_name"],
        guarantorLastName: json["guarantor_last_name"],
        guarantorMiddleName: json["guarantor_middle_name"],
        guarantorMonthlyIncome: json["guarantor_monthly_income"],
        guarantorWorkplace: json["guarantor_workplace"],
        homeService: json["home_service"],
        houseNo: json["house_no"],
        householderFirstName: json["householder_first_name"],
        householderLastName: json["householder_last_name"],
        householderMiddleName: json["householder_middle_name"],
        institutionSharesPurchased: json["institution_shares_purchased"],
        loanAmount: json["loan_amount"],
        loanTypeId: json["loan_type_id"],
        memberId: json["member_id"],
        memberWarranty: json["member_warranty"],
        region: json["region"],
        remark: json["remark"],
        requestDate: json["request_date"].toDouble(),
        shareAmount: json["share_amount"],
        sharePrice: json["share_price"],
        shareholderFirstName: json["shareholder_first_name"],
        shareholderLastName: json["shareholder_last_name"],
        shareholderMiddleName: json["shareholder_middle_name"],
        spouseFirstName: json["spouse_first_name"],
        spouseLastName: json["spouse_last_name"],
        spouseMiddleName: json["spouse_middle_name"],
        spouseMonthlyIncome: json["spouse_monthly_income"],
        spouseWorkplace: json["spouse_workplace"],
        status: json["status"],
        subcity: json["subcity"],
        totalPurchase: json["total_purchase"],
        typeOfCar: json["type_of_car"],
    );

    Map<String?, dynamic> toJson() => {
        "car_license_plate_number": carLicensePlateNumber,
        "car_owner_first_name": carOwnerFirstName,
        "car_owner_last_name": carOwnerLastName,
        "car_owner_middle_name": carOwnerMiddleName,
        "current_car_price": currentCarPrice,
        "district": district,
        "duration_month": durationMonth,
        "external_warranty": externalWarranty,
        "full_insurance_money": fullInsuranceMoney,
        "guarantor_first_name": guarantorFirstName,
        "guarantor_last_name": guarantorLastName,
        "guarantor_middle_name": guarantorMiddleName,
        "guarantor_monthly_income": guarantorMonthlyIncome,
        "guarantor_workplace": guarantorWorkplace,
        "home_service": homeService,
        "house_no": houseNo,
        "householder_first_name": householderFirstName,
        "householder_last_name": householderLastName,
        "householder_middle_name": householderMiddleName,
        "institution_shares_purchased": institutionSharesPurchased,
        "loan_amount": loanAmount,
        "loan_type_id": loanTypeId,
        "member_id": memberId,
        "member_warranty": memberWarranty,
        "region": region,
        "remark": remark,
        "request_date": requestDate,
        "share_amount": shareAmount,
        "share_price": sharePrice,
        "shareholder_first_name": shareholderFirstName,
        "shareholder_last_name": shareholderLastName,
        "shareholder_middle_name": shareholderMiddleName,
        "spouse_first_name": spouseFirstName,
        "spouse_last_name": spouseLastName,
        "spouse_middle_name": spouseMiddleName,
        "spouse_monthly_income": spouseMonthlyIncome,
        "spouse_workplace": spouseWorkplace,
        "status": status,
        "subcity": subcity,
        "total_purchase": totalPurchase,
        "type_of_car": typeOfCar,
    };
}
