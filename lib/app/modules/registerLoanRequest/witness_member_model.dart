// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

// ignore_for_file: unnecessary_new, unnecessary_null_comparison, prefer_conditional_assignment

import 'dart:convert';

MembersModel memberFromJson(String? str) =>
    MembersModel.fromJson(json.decode(str!));

String? memberToJson(MembersModel data) => json.encode(data.toJson());

class MembersModel {
  MembersModel({
    this.data,
    this.total,
  });

  List<Datum>? data;
  int? total;

  factory MembersModel.fromJson(Map<String?, dynamic> json) => MembersModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        total: json["total"],
      );

  Map<String?, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "total": total,
      };
}

class Datum {
  Datum({
    this.id,
    this.savingTotal,
    this.purchaseTotal,
    this.units,
    this.subscriptionTotal,
    this.savingBookNo,
    this.memberid,
    this.firstName,
    this.middleName,
    this.lastName,
    this.nationality,
    this.isMale,
    this.telephone,
    this.email,
    this.address,
    this.membershipDate,
    this.regDate,
    this.loginId,
  });

  String? id;
  double? savingTotal;
  double? purchaseTotal;
  double? units;
  double? subscriptionTotal;
  String? savingBookNo;
  int? memberid;
  String? firstName;
  String? middleName;
  String? lastName;
  Nationality? nationality;
  bool? isMale;
  String? telephone;
  String? email;
  Address? address;
  int? membershipDate;
  int? regDate;
  String? loginId;

  factory Datum.fromJson(Map<String?, dynamic> json) => Datum(
        id: json["id"],
        savingTotal: json["saving_total"],
        purchaseTotal: json["purchase_total"],
        units: json["units"],
        subscriptionTotal: json["subscription_total"],
        savingBookNo: json["saving_book_no"],
        memberid: json["memberid"],
        firstName: json["first_name"],
        middleName: json["middle_name"],
        lastName: json["last_name"],
        nationality: nationalityValues.map[json["nationality"]],
        isMale: json["is_male"],
        telephone: json["telephone"],
        email: json["email"],
        address: addressValues.map[json["address"]],
        membershipDate: json["membership_date"],
        regDate: json["reg_date"],
        loginId: json["login_id"],
      );

  Map<String?, dynamic> toJson() => {
        "id": id,
        "saving_total": savingTotal,
        "purchase_total": purchaseTotal,
        "units": units,
        "subscription_total": subscriptionTotal,
        "saving_book_no": savingBookNo,
        "memberid": memberid,
        "first_name": firstName,
        "middle_name": middleName,
        "last_name": lastName,
        "nationality": nationalityValues.reverse[nationality],
        "is_male": isMale,
        "telephone": telephone,
        "email": email,
        "address": addressValues.reverse[address],
        "membership_date": membershipDate,
        "reg_date": regDate,
        "login_id": loginId,
      };
}

enum Address { A_A }

final addressValues = EnumValues({"A.A": Address.A_A});

enum Nationality { ET }

final nationalityValues = EnumValues({"ET": Nationality.ET});

class EnumValues<T> {
  late Map<String?, T> map;
  late Map<T, String?> reverseMap;

  EnumValues(this.map);

  Map<T, String?> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
