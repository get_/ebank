import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/loan_type_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../../common/util/connection.dart';
import '../../../data/errors/api_error_model.dart';
import '../../widgets/toast.dart';

class LoanTypeController extends GetxController {
  final ApiHelper _apiHelper = Get.find();
  TextEditingController loanAmountController = TextEditingController();
  TextEditingController paymentMonthController = TextEditingController();

  @override
  void onInit() {
    fetch();

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  var items = ['select loan type...'];
  RxString dropdownvalue1 = 'select loan type...'.obs;
  RxString loanTypeId = ''.obs;
  RxList<LoanType> loanTypes = <LoanType>[].obs;

  RxBool isNotfetched = false.obs;
  RxInt status_code = 0.obs;
  RxBool isLoading = false.obs;

  fetch() async {
    isLoading.value = true;
    isNotfetched.value = true;
    try {
      check().then((intenet) async {
        if (intenet != null && intenet) {
          _apiHelper.getLoanTypes().then((value) {
            isLoading.value = false;
            Logger().d(value.body.toString());
            if (value.statusCode!.clamp(200, 299) == value.statusCode) {
              status_code.value = 200;
              loanTypes.value = loanTypFromJson(value.body).data;
              loanTypeId.value = loanTypes[0].id;
              items.clear();
              for (var item in loanTypes) {
                items.add(item.name);
                Logger().d(item.name);
                dropdownvalue1.value = items[0];
              }
              isNotfetched.value = false;
            } else if (value.statusCode!.clamp(500, 599) == value.statusCode) {
              status_code.value = 500;
              isNotfetched.value = false;
              var err = apiErrorFromJson(value.body);
              toast(err.msgs[0]);
            } else if (value.statusCode!.clamp(400, 499) == value.statusCode) {
              isNotfetched.value = false;
              status_code.value = 400;
              Logger().d(value.statusCode);
              toast('Resource not found.');
            } else {
              isNotfetched.value = false;
              Logger().d(value.statusCode);
              toast('operation failed');
            }
          }, onError: (err) {
            // change(null, status: RxStatus.error(err.toString()));
          });
        } else {
          toast('no connection!');
        }
      });
    } catch (e) {}
  }

  getLoanTypeId(val) {
    for (var item in loanTypes) {
      if (item.name == val) {
        Logger().d(item.name);
        Logger().d(item.id);
        loanTypeId.value = item.id;
      }
    }
  }
}
