// ignore_for_file: unrelated_type_equality_checks, unnecessary_null_comparison

import 'package:e_kuteba/app/common/util/connection.dart';
import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/data/errors/api_error_model.dart';
import 'package:e_kuteba/app/modules/basicDetail/controllers/basic_detail_controller.dart';
import 'package:e_kuteba/app/modules/home/views/home_view.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/controllers/loan_types_controller.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/controllers/member_witness_controller.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/loan_request_model.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/loan_type_model.dart';
import 'package:e_kuteba/app/modules/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

import '../../../common/util/exports.dart';

class WarrantyController extends GetxController {
  final ApiHelper _apiHelper = Get.find();

  MemberWitnessController memberWitnessController =
      Get.put(MemberWitnessController());
  LoanTypeController loanTypeController = Get.put(LoanTypeController());
  BasicDetailController coopMemberSelfController =
      Get.put(BasicDetailController());

  ///monthly income
  TextEditingController guarantorfNameController = TextEditingController();
  TextEditingController guarantorlNameController = TextEditingController();
  TextEditingController guarantormNameController = TextEditingController();
  TextEditingController guarantorworkplaceController = TextEditingController();
  TextEditingController guarantormonthlyIncomeController =
      TextEditingController(text: '0');

  TextEditingController spousefNameController = TextEditingController();
  TextEditingController spouselNameController = TextEditingController();
  TextEditingController spousemNameController = TextEditingController();
  TextEditingController spouseworkplaceController = TextEditingController();
  TextEditingController spousemonthlyIncomeController =
      TextEditingController(text: '0');
  TextEditingController remarkController = TextEditingController();

  ///share
  TextEditingController sharefNameController = TextEditingController();
  TextEditingController sharelNameController = TextEditingController();
  TextEditingController sharemNameController = TextEditingController();

  TextEditingController totalSharePurchaseController =
      TextEditingController(text: '0');
  TextEditingController sharePriceController = TextEditingController(text: '0');
  TextEditingController totalPurchaseAmountController =
      TextEditingController(text: '0');
  TextEditingController institutionController = TextEditingController();

  ///carlibre
  TextEditingController carOwnerfNameController = TextEditingController();
  TextEditingController carOwnerlNameController = TextEditingController();
  TextEditingController carOwnermNameController = TextEditingController();

  TextEditingController typeofCarController = TextEditingController();
  TextEditingController carLicenseController = TextEditingController();
  TextEditingController currentCarPriceController =
      TextEditingController(text: '0');
  TextEditingController fullInstitutionController =
      TextEditingController(text: '0');

  ///house map
  TextEditingController houseHolderfNameController = TextEditingController();
  TextEditingController houseHolderlNameController = TextEditingController();
  TextEditingController houseHoldermNameController = TextEditingController();

  TextEditingController regionCarController = TextEditingController(text: '0');
  TextEditingController subcityController = TextEditingController();
  TextEditingController districtController = TextEditingController();
  TextEditingController houseNumberController = TextEditingController();
  TextEditingController homeServiceController =
      TextEditingController(text: '0');

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  var items = ['Monthly Income', 'Share', 'Car Libre', 'House Map'];
  RxString dropdownvalue1 = 'Monthly Income'.obs;
  RxList<LoanType> loanTypes = <LoanType>[].obs;

  void getLoanTypes() {
    _apiHelper.getLoanTypes().then((value) {
      loanTypes.value = loanTypFromJson(value.body).data;
      items.clear();
      for (var item in loanTypes) {
        items.add(item.name);
        Logger().d(item.name);
        dropdownvalue1.value = items[0];
      }
    });
  }
  //  RxString startDate = 'Select Date'.tr.obs;
  // double startDateToEpoch = 0.0;
  // Rx<DateTime> currentDate = DateTime.now().obs;
  // Future<void> selectStartDate(BuildContext context) async {
  //   final DateTime? pickedDate = await showDatePicker(
  //       context: context,
  //       initialDate: currentDate.value,
  //       firstDate: DateTime(2020),
  //       lastDate: DateTime(2050),
  //       builder: (context, picker) {
  //         return Theme(
  //           data: ThemeData.dark().copyWith(
  //             colorScheme: const ColorScheme.dark(
  //               primary: AppColors.secondaryColor,
  //               onPrimary: Colors.white,
  //               surface: AppColors.secondaryColor,
  //               onSurface: Colors.white,
  //             ),
  //             dialogBackgroundColor: AppColors.primaryColor,
  //           ),
  //           child: picker!,
  //         );
  //       }).then((selectedDate) {
  //     if (selectedDate != null) {
  //       var parseEndDate =
  //           DateTime.parse(selectedDate.add(const Duration(hours: 24)).toString());
  //       var parsedDate = DateTime.parse(selectedDate.toString());
  //       startDate.value = DateFormat("dd-MM-yyyy").format(parsedDate);
  //       var start_date = (selectedDate).millisecondsSinceEpoch;
  //       var start = (start_date / 1000).round();
  //       startDateToEpoch = start.toDouble();
  //     }
  //   });
  // }

  void PostRegist(context) async {
    ////////////////here////////////////////
    DateTime now = DateTime.now();
    Logger().d(now.millisecondsSinceEpoch);
    LoanRequestModel loanRequestModel = LoanRequestModel(
        carLicensePlateNumber: carLicenseController.text,
        carOwnerFirstName: carOwnerfNameController.text,
        carOwnerLastName: carOwnerlNameController.text,
        carOwnerMiddleName: carOwnermNameController.text,
        currentCarPrice: int.parse(currentCarPriceController.text),
        district: districtController.text,
        durationMonth:
            int.parse(loanTypeController.paymentMonthController.text),
        externalWarranty: warrantNumber(dropdownvalue1.value),
        fullInsuranceMoney: int.parse(fullInstitutionController.text),
        guarantorFirstName: guarantorfNameController.text,
        guarantorLastName: guarantorlNameController.text,
        guarantorMiddleName: guarantormNameController.text,
        guarantorMonthlyIncome:
            int.parse(guarantormonthlyIncomeController.text),
        guarantorWorkplace: guarantorworkplaceController.text,
        homeService: int.parse(homeServiceController.text),
        houseNo: houseNumberController.text,
        householderFirstName: houseHolderfNameController.text,
        householderLastName: houseHolderlNameController.text,
        householderMiddleName: houseHoldermNameController.text,
        institutionSharesPurchased: institutionController.text,
        loanAmount: int.parse(loanTypeController.loanAmountController.text),
        loanTypeId: loanTypeController.loanTypeId.value,
        memberId: coopMemberSelfController.member.value.id,
        memberWarranty: memberWitnessController.memeberId.value,
        region: int.parse(regionCarController.text),
        remark: remarkController.text,
        requestDate:
            double.parse(now.millisecondsSinceEpoch.toString()), ///////////,

        shareAmount: int.parse(totalSharePurchaseController.text),
        sharePrice: int.parse(sharePriceController.text),
        shareholderFirstName: sharefNameController.text,
        shareholderLastName: sharelNameController.text,
        shareholderMiddleName: sharemNameController.text,
        spouseFirstName: spousefNameController.text,
        spouseLastName: spouselNameController.text,
        spouseMiddleName: spousemNameController.text,
        spouseMonthlyIncome: int.parse(spousemonthlyIncomeController.text),
        spouseWorkplace: spouseworkplaceController.text,
        status: 1,
        subcity: subcityController.text,
        totalPurchase: int.parse(totalPurchaseAmountController.text),
        typeOfCar: typeofCarController.text);

    try {
      check().then((intenet) async {
        if (intenet != null && intenet) {
          await _apiHelper.postRegisterLoanRequest(loanRequestModel).then(
            (value) async {
              Logger().d("outerrrr...", loanTypeController.loanTypeId.value);

              if (value.statusCode!.clamp(200, 299) == value.statusCode) {
                Logger().d("inrrrrrr", loanTypeController.loanTypeId.value);

                await _apiHelper
                    .postLoanRequest(loanRequestModel)
                    .then((value) {
                  Logger().d(loanTypeController.loanTypeId.value);
                });
                toast('You register loan Successfully'.tr);
                printInfo(info: value.body.toString());
                Get.to(() => HomeView());
              } else if (value.statusCode!.clamp(500, 599) ==
                  value.statusCode) {
                var err = apiErrorFromJson(value.body);
                ShowsSnackBar(context,
                    err.msgs[0] == '' ? 'Internal Server Error!' : err.msgs[0]);
              } else if (value.statusCode!.clamp(400, 499) ==
                  value.statusCode) {
                var err = apiErrorFromJson(value.body);
                Logger().d(value.statusCode);
                ShowsSnackBar(context,
                    err.msgs[0] == '' ? 'Resource not found.' : err.msgs[0]);
              } else {
                var err = apiErrorFromJson(value.body);
                toast(err.msgs[0]);
                Logger().d(value.statusCode);
                toast('operation failed');
              }
            },
          );
        } else {
          ShowsSnackBar(context, 'No connection');
        }
      });
    } catch (e) {
      toast(e.toString());
    }
  }

  int warrantNumber(String warrant) {
    var val = 100;
    switch (warrant) {
      case 'Monthly Income':
        val = 100;
        break;
      case 'Share':
        val = 102;
        break;
      case 'Car Libre':
        val = 101;
        break;
      case 'House Map':
        val = 103;
        break;
      default:
        val = 100;
    }
    return val;
  }
}
