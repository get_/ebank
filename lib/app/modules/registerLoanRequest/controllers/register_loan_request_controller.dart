import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/loan_request_model.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/witness_member_model.dart';
import 'package:e_kuteba/app/modules/widgets/toast.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class RegisterLoanRequestController extends GetxController {
  final ApiHelper _apiHelper = Get.find();
  RxInt activeStepIndex = 0.obs;

  @override
  void onInit() {
    getMembers(0);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  Rx<LoanRequestModel> loanRequestModel = LoanRequestModel().obs;

  RxBool checkboxSelected = false.obs;
  RxList<Datum> selectedDoc = <Datum>[].obs;
  onRowSelected(selected, Datum data) {
    checkboxSelected.value = true;
    if (selected) {
      selectedDoc.clear();
      selectedDoc.add(data);
    } else {
      selectedDoc.clear();
    }
  }

  int page = 0;
  int maxPage = 0;
  RxList<Datum> members = <Datum>[].obs;
  void getMembers(page) async {
    List<Datum> member = <Datum>[];
    if (maxPage == 0) {
      _apiHelper.getMembers(page).then((value) {
        for (var item in memberFromJson(value.body).data!) {
          member.add(item);
        }
      });
      if (member.isNotEmpty) {
        members.addAll(member);

        page++;
      } else {
        maxPage = page;
      }
    } else {
      toast('no more data');
    }
  }

}
