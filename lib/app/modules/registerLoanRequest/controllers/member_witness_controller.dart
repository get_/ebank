import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/witness_member_model.dart';
import 'package:e_kuteba/app/modules/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class MemberWitnessController extends GetxController {
  final ApiHelper _apiHelper = Get.find();
  TextEditingController searchMemberController = TextEditingController();

  @override
  void onInit() {
    getMembers(0);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  RxBool checkboxSelected = false.obs;
  RxList<Datum> selectedDoc = <Datum>[].obs;
  RxString memeberId = ''.obs;
  onRowSelected(selected, Datum data) {
    checkboxSelected.value = true;
    if (selected) {
      selectedDoc.clear();
      selectedDoc.add(data);
      memeberId.value = data.id!;
    } else {
      selectedDoc.clear();
    }
  }

  RxBool isSearch = false.obs;

  RxInt page = 0.obs;
  int maxPage = 0;
  RxInt totalData = 0.obs;
  RxInt currentPage = 0.obs;
  RxInt currentNumber = 0.obs;
  RxList<Datum> members = <Datum>[].obs;
  RxList<Datum> members_copy = <Datum>[].obs;

  RxList<Datum> searchMembers = <Datum>[].obs;

  searchMemeber(val) {
    searchMembers.clear();

    for (var element in members_copy) {
      if (element.firstName!.contains(val)) {
       searchMembers.add(element);
      }
    }
  }

  void getMembers(pag) async {
    List<Datum> member = <Datum>[];
    if (maxPage == 0) {
      _apiHelper.getMembers(pag).then((value) {
      if (pag == 0) {
          totalData.value = memberFromJson(value.body).total!;
        }

        for (var item in memberFromJson(value.body).data!) {
          member.add(item);
      }
        if (memberFromJson(value.body).data!.isNotEmpty) {
          members.clear();
          members.addAll(member);
          members_copy.addAll(member);
        } else {
          maxPage = pag;
        }
      });
    } else {
      toast('no more data');
    }
  }
}
