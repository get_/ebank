import 'package:e_kuteba/app/modules/registerLoanRequest/controllers/loan_types_controller.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/controllers/member_witness_controller.dart';
import 'package:get/get.dart';

import '../controllers/register_loan_request_controller.dart';

class RegisterLoanRequestBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RegisterLoanRequestController>(
      () => RegisterLoanRequestController(),
    );
    Get.lazyPut<LoanTypeController>(
      () => LoanTypeController(),
    );
    Get.lazyPut<MemberWitnessController>(() => MemberWitnessController());
  }
}
