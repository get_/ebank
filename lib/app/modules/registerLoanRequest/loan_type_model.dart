
import 'dart:convert';

LoanTypeModel loanTypFromJson(String str) => LoanTypeModel.fromJson(json.decode(str));

String loanTypToJson(LoanTypeModel data) => json.encode(data.toJson());

class LoanTypeModel {
  LoanTypeModel({
    required  this.data,
    required  this.total,
  });

  List<LoanType> data;
  int total;

  factory LoanTypeModel.fromJson(Map<String, dynamic> json) => LoanTypeModel(
        data: List<LoanType>.from(json["data"].map((x) => LoanType.fromJson(x))),
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total,
      };
}

class LoanType {
  LoanType({
    required this.id,
    required this.name,
    required this.description,
    required this.requiredMonths,
    required this.requiredSaving,
    required this.paymentMonths,
    required this.interstAmount,
    required this.remark,
    required this.requiredMaximumAmount,
    required this.requiredMinimumSharePercentage,
  });

  String id;
  String name;
  String description;
  int requiredMonths;
  double requiredSaving;
  int paymentMonths;
  double interstAmount;
  String remark;
  double requiredMaximumAmount;
  double requiredMinimumSharePercentage;

  factory LoanType.fromJson(Map<String, dynamic> json) => LoanType(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        requiredMonths: json["required_months"],
        requiredSaving: json["required_saving"],
        paymentMonths: json["payment_months"],
        interstAmount: json["interst_amount"],
        remark: json["remark"],
        requiredMaximumAmount: json["required_maximum_amount"],
        requiredMinimumSharePercentage:
            json["required_minimum_share_percentage"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "required_months": requiredMonths,
        "required_saving": requiredSaving,
        "payment_months": paymentMonths,
        "interst_amount": interstAmount,
        "remark": remark,
        "required_maximum_amount": requiredMaximumAmount,
        "required_minimum_share_percentage": requiredMinimumSharePercentage,
      };
}