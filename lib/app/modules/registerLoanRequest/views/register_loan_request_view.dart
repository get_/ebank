import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/common/util/validators.dart';
import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/controllers/loan_types_controller.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/views/member_witness.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../widgets/custom_loading_widget.dart';
import '../../widgets/custom_server_error_widget.dart';

class RegisterLoanRequestView extends GetView<LoanTypeController> {
  final controller = Get.put(LoanTypeController());

  final _formKey = GlobalKey<FormState>();

  var autovalidate = AutovalidateMode.disabled;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: buildAppBar(context, 'New Loan Request'.tr),
      // drawer: CustomDrawerWidget(context),
      body: Obx(
        () => controller.isLoading.value
            ? loadingWidget()
            : controller.loanTypes.length <= 1
                ? ServerErrorWidget()
                : SingleChildScrollView(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Form(
                          key: _formKey,
                          autovalidateMode: autovalidate,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              // Text('LoanType: '),
                              SizedBox(
                                height: 15,
                              ),
                              Obx(() => Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: DropdownButtonFormField(
                                      validator: (value) =>
                                          value == 'select loan type...'
                                              ? 'field required'
                                              : null,
                                      hint: Text('data'),
                                      value: controller.dropdownvalue1.value,
                                      isDense: true,
                                      isExpanded: true,
                                      items:
                                          controller.items.map((String items) {
                                        return DropdownMenuItem(
                                          value: items,
                                          child: Text(items,
                                              textAlign: TextAlign.start),
                                        );
                                      }).toList(),
                                      onChanged: (newValue) {
                                        controller.getLoanTypeId(newValue);
                                        controller.dropdownvalue1.value =
                                            newValue! as String;
                                      },
                                    ),
                                  )),
                              SizedBox(
                                height: 15,
                              ),
                              TextFormField(
                                controller: controller.loanAmountController,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  helperText: 'Maximium Amount 100,000 Birr'.tr,
                                  label: Text('Loan Amount'.tr),
                                  hintText: 'Enter Amount'.tr,
                                  border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(4.0)),
                                  ),
                                ),
                                validator: (value) {
                                  return Validators.validateDouble(value);
                                },
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.number,
                                validator: (value) {
                                  return Validators.notEmpty(value);
                                },
                                controller: controller.paymentMonthController,
                                decoration: InputDecoration(
                                  label: Text('Payment Month'.tr),
                                  hintText: 'Enter month'.tr,
                                  border: OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(4.0)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            Get.to(() => WitnessMemberSelectionPage());
          } else {
            autovalidate = AutovalidateMode.onUserInteraction;
          }
        },
        backgroundColor: AppColors.primaryColor,
        // icon: Icon(Icons.arrow_forward_sharp),
        tooltip: 'next',
        label: Text('Next'.tr),
      ),
    );
  }
}
