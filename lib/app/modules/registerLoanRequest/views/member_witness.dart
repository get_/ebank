// // ignore_for_file: annotate_overrides, prefer_const_constructors

// ignore_for_file: prefer_const_constructors

import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/controllers/member_witness_controller.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/views/warranty_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class WitnessMemberSelectionPage extends GetView<MemberWitnessController> {
  WitnessMemberSelectionPage({Key? key}) : super(key: key);

  final controller = Get.put(MemberWitnessController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: buildAppBar(context, 'New Loan Request'.tr),
      body: Theme(
        data: ThemeData(
            primarySwatch: Colors.orange,
            colorScheme:
                const ColorScheme.light(primary: AppColors.primaryColor)),
        child: Obx(
          () => controller.members.isEmpty
              ? const Center(child: CircularProgressIndicator())
              : SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Row(
                          // ignore: prefer_const_literals_to_create_immutables
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                'Member Selection'.tr,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SizedBox(
                                  height: 50,
                                  child: TextField(
                                    onChanged: (val) {
                                      controller.isSearch.value = true;
                                    controller.searchMemeber(val);
                                    },
                                    controller:
                                        controller.searchMemberController,
                                    decoration: InputDecoration(
                                        fillColor: Colors.amberAccent,
   hintText: "Search".tr,
                                        prefixIcon: Icon(Icons.search),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10.0)))),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                DataTable(
                                    showBottomBorder: true,
                                    showCheckboxColumn: true,
                                    headingRowColor: MaterialStateProperty.all(
                                        Colors.grey.shade300),
                                    columns: <DataColumn>[
                                      DataColumn(
                                        label: Text(
                                          'FName'.tr,
                                          style: const TextStyle(
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                      DataColumn(
                                        label: Text(
                                          'MName'.tr,
                                          style: const TextStyle(
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                      DataColumn(
                                        label: Text(
                                          'LName'.tr,
                                          style: const TextStyle(
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                      DataColumn(
                                        label: Text(
                                          'Nationality'.tr,
                                          style: const TextStyle(
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                      DataColumn(
                                        label: Text(
                                          'Gender'.tr,
                                          style: const TextStyle(
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                      DataColumn(
                                        label: Text(
                                          'Telephone'.tr,
                                          style: const TextStyle(
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                      DataColumn(
                                        label: Text(
                                          'Email'.tr,
                                          style: const TextStyle(
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                      DataColumn(
                                        label: Text(
                                          'Membership Date'.tr,
                                          style: const TextStyle(
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                    
                                    ],
                                    rows: controller.isSearch.value
                                        ? controller.searchMembers.map(
                                            (element) {
                                            return DataRow(
                                                selected: controller.selectedDoc
                                                    .contains(element),
                                                onSelectChanged: (b) {
                                                  controller.onRowSelected(
                                                      b, element);
                                                },
                                                cells: <DataCell>[
                                                  DataCell(
                                                      Text(element.firstName!)),
                                                  DataCell(Text(
                                                      element.middleName!)),
                                                  DataCell(
                                                      Text(element.lastName!)),
                                                  DataCell(Text(element
                                                      .nationality!
                                                      .toString())),
                                                  DataCell(Text(element.isMale!
                                                      ? 'M'
                                                      : 'F')),
                                                  DataCell(
                                                      Text(element.telephone!)),
                                                  DataCell(
                                                      Text(element.email!)),
                                                  DataCell(Text(element.regDate!
                                                      .toString())),
                                             
                                                ],
                                              );
                                            },
                                          ).toList()
                                        : controller.members.map(
                                            (element) {
                                            return DataRow(
                                                selected: controller.selectedDoc
                                                    .contains(element),
                                                onSelectChanged: (b) {
                                                  controller.onRowSelected(
                                                      b, element);
                                                },
                                                cells: <DataCell>[
                                                  DataCell(
                                                      Text(element.firstName!)),
                                                  DataCell(Text(
                                                      element.middleName!)),
                                                  DataCell(
                                                      Text(element.lastName!)),
                                                  DataCell(Text(element
                                                      .nationality!
                                                      .toString())),
                                                  DataCell(Text(element.isMale!
                                                      ? 'M'
                                                      : 'F')),
                                                  DataCell(
                                                      Text(element.telephone!)),
                                                  DataCell(
                                                      Text(element.email!)),
                                                  DataCell(Text(Utils.toDate(
                                                          element.regDate!)
                                                      .toString())),
                                                ],
                                              );
                                            },
                                          ).toList()),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                InkWell(
                                    onTap: () {
                                      var valu = controller.page.value >= 0
                                          ? controller.page.value - 1
                                          : 0;
                                      controller.getMembers(valu);
                                    },
                                    child:
                                        Icon(Icons.arrow_back_ios_new_rounded)),
                                Text(controller.members_copy.length.toString() +
                                    '/' +
                                    controller.totalData.toString()),
                                InkWell(
                                    onTap: () {
                                      controller.page.value += 1;
                                      controller
                                          .getMembers(controller.page.value);
                                    },
                                    child:
                                        Icon(Icons.arrow_forward_ios_rounded))
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(left: 30),
        child: FloatingActionButton.extended(
          heroTag: "btn2",
          backgroundColor: AppColors.primaryColor,
          label: Text('Next'.tr),
          // icon: const Icon(Icons.arrow_forward),
          onPressed: () {
            // if (controller.selectedDoc.isNotEmpty) {
            Get.to(() => WarrantytView());
            // } else {
            //   final snackBar = SnackBar(
            //     content: const Text('Please select one member witness'),
            //     action: SnackBarAction(
            //       label: 'OK',
            //       onPressed: () {
            //         ScaffoldMessenger.of(context).hideCurrentSnackBar();
            //       },
            //     ),
            //   );
            //   ScaffoldMessenger.of(context).showSnackBar(snackBar);
            // }
          },
          tooltip: 'next',
        ),
      ),
    );
  }
}
