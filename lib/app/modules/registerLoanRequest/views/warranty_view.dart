// ignore_for_file: use_key_in_widget_constructors, must_be_immutable, prefer_const_constructors, unnecessary_brace_in_string_interps, prefer_const_literals_to_create_immutables, unrelated_type_equality_checks

import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/common/util/validators.dart';
import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/controllers/warranty_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WarrantytView extends GetView<WarrantyController> {
  @override
  final controller = Get.put(WarrantyController());
  final _formKey = GlobalKey<FormState>();
  var autovalidate = AutovalidateMode.disabled;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: buildAppBar(context, 'New Loan Request'.tr),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Form(
            key: _formKey,
            autovalidateMode: autovalidate,
            child: ListView(
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 15,
                ),
                InputDecorator(
                  decoration: InputDecoration(
                      label: Text('Warranty'.tr),
                      border: OutlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(4.0)),
                        // contentPadding: EdgeInsets.all(10),
                      )),
                  child: Obx(() => DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          hint: Text('data'),
                          value: controller.dropdownvalue1.value,
                          isDense: true,
                          isExpanded: true,
                          items: controller.items.map((String items) {
                            return DropdownMenuItem(
                              value: items,
                              child: Text(items, textAlign: TextAlign.start),
                            );
                          }).toList(),
                          onChanged: (newValue) {
                            controller.dropdownvalue1.value = newValue!;
                          },
                        ),
                      )),
                ),
                SizedBox(
                  height: 15,
                ),
                Obx(
                  () => Container(
                    height: MediaQuery.of(context).size.height * 0.85,
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Column(
                      children: [
                        if (controller.dropdownvalue1.value ==
                            'Monthly Income') ...[
                          monthlyIncome(context),
                        ] else if (controller.dropdownvalue1.value ==
                            'Share') ...[
                          share(context),
                        ] else if (controller.dropdownvalue1.value ==
                            'Car Libre') ...[
                          carLibre(context),
                        ] else ...[
                          houseMap(context),
                        ]
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            controller.PostRegist(context);
          } else {
            autovalidate = AutovalidateMode.onUserInteraction;
          }
        },
        backgroundColor: AppColors.primaryColor,
        tooltip: 'Submit',
        label: Text('Submit'.tr),
      ),
    );
  }

  Column carLibre(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                controller: controller.carOwnerfNameController,
                decoration: InputDecoration(
                  label: Text('First Name'.tr),
                ),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                controller: controller.carOwnermNameController,
                decoration: InputDecoration(
                  label: Text('Middle Name'.tr),
                ),
              ),
            ),
          ],
        ),
        TextFormField(
          validator: (value) {
            return Validators.notEmpty(value);
          },
          controller: controller.carOwnerlNameController,
          decoration: InputDecoration(
            label: Text('Last Name'.tr),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                controller: controller.typeofCarController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(label: Text('Type of Car')),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                controller: controller.carLicenseController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(
                  label: Text('Car license plate'.tr),
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: controller.currentCarPriceController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(label: Text('Current car price')),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: controller.fullInstitutionController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(
                  label: Text('Full institution silver'.tr),
                ),
              ),
            ),
          ],
        ),
        TextFormField(
          controller: controller.remarkController,
          decoration: InputDecoration(
            label: Text('Remark'.tr),
          ),
        ),
      ],
    );
  }

  Column share(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                controller: controller.sharefNameController,
                decoration: InputDecoration(
                  label: Text('First Name'.tr),
                ),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                controller: controller.sharemNameController,
                decoration: InputDecoration(
                  label: Text('Middle Name'.tr),
                ),
              ),
            ),
          ],
        ),
        TextFormField(
          validator: (value) {
            return Validators.notEmpty(value);
          },
          controller: controller.sharelNameController,
          decoration: InputDecoration(
            label: Text('Last Name'.tr),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: controller.totalSharePurchaseController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration:
                    InputDecoration(label: Text('Total Share Purchase'.tr)),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: controller.sharePriceController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(
                  label: Text('Share Price'.tr),
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: controller.totalPurchaseAmountController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration:
                    InputDecoration(label: Text('Total Purchase amount'.tr)),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                controller: controller.institutionController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(
                  label: Text('institution'.tr),
                ),
              ),
            ),
          ],
        ),
        TextFormField(
          controller: controller.remarkController,
          decoration: InputDecoration(
            label: Text('Remark'.tr),
          ),
        ),
      ],
    );
  }

  Column monthlyIncome(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                controller: controller.guarantorfNameController,
                decoration: InputDecoration(
                  label: Text('First Name'.tr),
                ),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                controller: controller.guarantormNameController,
                decoration: InputDecoration(
                  label: Text('Middle Name'.tr),
                ),
              ),
            ),
          ],
        ),
        TextFormField(
          validator: (value) {
            return Validators.notEmpty(value);
          },
          controller: controller.guarantorlNameController,
          decoration: InputDecoration(
            label: Text('Last Name'.tr),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                controller: controller.guarantorworkplaceController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(
                  label: Text('Workplace'.tr),
                ),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: controller.guarantormonthlyIncomeController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(
                  label: Text('monthly Income'.tr),
                ),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(18.0),
          child: Text(
            'Spouse'.tr,
            style: TextStyle(
                fontSize: 20,
                color: AppColors.primaryColor,
                fontWeight: FontWeight.bold),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                controller: controller.spousefNameController,
                decoration: InputDecoration(
                  label: Text('First Name'.tr),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                controller: controller.spousemNameController,
                decoration: InputDecoration(
                  label: Text('Middle Name'.tr),
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                controller: controller.spouselNameController,
                decoration: InputDecoration(label: Text('Last Name'.tr)),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                controller: controller.spouseworkplaceController,
                decoration: InputDecoration(
                  label: Text('Workplace'.tr),
                ),
              ),
            ),
          ],
        ),
        TextFormField(
          keyboardType: TextInputType.number,
          controller: controller.spousemonthlyIncomeController,
          decoration: InputDecoration(
            label: Text('monthly Income'.tr),
          ),
        ),
        TextFormField(
          controller: controller.remarkController,
          decoration: InputDecoration(
            label: Text('Remark'.tr),
          ),
        ),
      ],
    );
  }

  Column houseMap(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                controller: controller.houseHolderfNameController,
                decoration: InputDecoration(
                  label: Text('First Name'.tr),
                ),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                controller: controller.houseHoldermNameController,
                decoration: InputDecoration(
                  label: Text('Middle Name'.tr),
                ),
              ),
            ),
          ],
        ),
        TextFormField(
          validator: (value) {
            return Validators.notEmpty(value);
          },
          controller: controller.houseHolderlNameController,
          decoration: InputDecoration(
            label: Text('Last Name'.tr),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(18.0),
          child: Text(
            'Address of the House'.tr,
            style: TextStyle(
                fontSize: 20,
                color: AppColors.primaryColor,
                fontWeight: FontWeight.bold),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: controller.regionCarController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(
                  label: Text('Region'.tr),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                controller: controller.subcityController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(
                  label: Text('Subcity'.tr),
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                controller: controller.districtController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(label: Text('District'.tr)),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: controller.houseNumberController,
                validator: (value) {
                  return Validators.notEmpty(value);
                },
                decoration: InputDecoration(
                  label: Text('House number'),
                ),
              ),
            ),
          ],
        ),
        TextFormField(
          keyboardType: TextInputType.number,
          controller: controller.homeServiceController,
          validator: (value) {
            return Validators.notEmpty(value);
          },
          decoration: InputDecoration(
            label: Text('Home Service'.tr),
          ),
        ),
        TextFormField(
          controller: controller.remarkController,
          decoration: InputDecoration(
            label: Text('Remark'.tr),
          ),
        ),
      ],
    );
  }
}
