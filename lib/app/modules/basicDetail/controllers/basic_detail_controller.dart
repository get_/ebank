import 'package:e_kuteba/app/common/constants.dart';
import 'package:e_kuteba/app/modules/basicDetail/member_model.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../../../common/util/connection.dart';
import '../../../common/util/exports.dart';
import '../../../data/api_helper.dart';
import '../../../data/errors/api_error_model.dart';
import '../../../routes/app_pages.dart';
import '../../widgets/toast.dart';

class BasicDetailController extends GetxController with StateMixin<Member> {
  final ApiHelper _apiHelper = Get.find();
  @override
  void onInit() {
    if (Storage.storage.hasData(Constants.TOKEN)) {
      fetch();
    }
    super.onInit();
  }

  @override
  void onClose() {}

  Rx<Member> member = Member(
          loanInterestAmount: 0.0,
          remainingAmount: 0.0,
          id: '',
          memberid: 0,
          firstName: '',
          middleName: '',
          lastName: '',
          birthDate: 0,
          nationality: '',
          isMale: false,
          telephone: '',
          email: '',
          address: '',
          membershipDate: 0,
          regDate: 0,
          familySize: 0,
          maritalStatus: 0,
          spiritualEducation: false,
          workExperience: '',
          monthlyIncome: 0,
          immediateContact: '',
          referral: '',
          savingBookNo: '0',
          educationId: '',
          educationalLevel: 0,
          loginId: '',
          subscriptionTotal: 0,
          savingTotal: 0,
          units: 0,
          purchaseTotal: 0,
          hasGeneralCaseOpen: true,
          totalSubscription: 0,
          totalPurchase: 0,
          savingAmount: 0,
          depositAmount: 0,
          approvedAmount: 0,
          principal: 0,
          monthlyPayment: 0,
          interesetAmount: 0,
          dividend: 0.0,
          totalInterest: 0,
          mandatorySaving: 0,
          voluntarySaving: 0,
          childSaving: 0,
          timeLimitedSaving: 0)
      .obs;

  // RxBool isNotfetched = false.obs;
  RxInt status_code = 0.obs;
  RxBool isLoading = false.obs;

  fetch() async {
    isLoading.value = true;
    try {
      check().then((intenet) async {
        if (intenet != null && intenet) {
          _apiHelper.getCoopMemberSelf().then((value) {
            isLoading.value = false;
           if (value.statusCode!.clamp(200, 299) == value.statusCode) {
              status_code.value = 200;
              member.value = memberFromJson(value.body);

            } else if (value.statusCode!.clamp(500, 599) == value.statusCode) {
              status_code.value = 500;
              var err = apiErrorFromJson(value.body);
              toast(err.msgs[0]);
            } else if (value.statusCode!.clamp(400, 499) == value.statusCode) {
              status_code.value = 400;
              toast('Resource not found.');
            } else {
              toast('operation failed');
            }
          }, onError: (err) {
            change(null, status: RxStatus.error(err.toString()));
          });
        } else {
          toast('connection issue');
        }
      });
    } catch (e) {
      toast(e.toString());
    }
  }

  void onLogoutClick() {
    Get.offAllNamed(Routes.DASHBOARD);
    Storage.clearStorage();
  }
}
