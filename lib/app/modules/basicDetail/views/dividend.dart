import 'package:e_kuteba/app/modules/basicDetail/controllers/basic_detail_controller.dart';
import 'package:e_kuteba/app/modules/basicDetail/member_model.dart';
import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../common/util/exports.dart';

class Dividend extends GetView<BasicDetailController> {
  const Dividend({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBar(context, 'Dividend'.tr),
        body: Obx(() {
          return SingleChildScrollView(
              child: customVerticalCard(context, controller.member.value));
        }));
  }

  Widget customVerticalCard(
    BuildContext context,
    Member member,
  ) {
    return Container(
      margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 10,
          ),
          Text(
            'Dividend Total'.tr,
            style: TextStyle(color: Color(0xffff9a00), fontSize: 24),
          ),
          Text(Utils.numberFormat(member.dividend),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
        ],
      ),
    );
  }
}
