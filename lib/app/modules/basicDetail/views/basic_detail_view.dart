// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:avatar_glow/avatar_glow.dart';
import 'package:e_kuteba/app/modules/widgets/custom_loading_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../common/values/app_colors.dart';
import '../../widgets/appbar.dart';
import '../controllers/basic_detail_controller.dart';

class BasicDetailView extends GetView<BasicDetailController> {
  @override
  final controller = Get.put(BasicDetailController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, 'Basic Profile'.tr),
      // drawer: CustomDrawerWidget(context),
      body: Theme(
        data: ThemeData(
            primarySwatch: Colors.orange,
            colorScheme: ColorScheme.light(primary: AppColors.primaryColor)),
        child: SingleChildScrollView(
          child: Obx(() => controller.isLoading.value
              ? Center(child: loadingWidget())
              : Stack(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.1,
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 0.1,
                          MediaQuery.of(context).size.width * 0.1,
                          MediaQuery.of(context).size.width * 0.1,
                          0),
                      // TODO add box shadow
                      decoration: BoxDecoration(
                          color: AppColors.primaryColor,
                          boxShadow: [BoxShadow(color: Colors.grey)]),
                    ),
                    Center(
                      child: Container(
                        child: CircleAvatar(
                          radius: 25,
                          backgroundColor: AppColors.doveGray,
                          child: AvatarGlow(
                            endRadius: 80,
                            duration: Duration(seconds: 2),
                            glowColor: Colors.white24,
                            repeat: true,
                            repeatPauseDuration: Duration(seconds: 1),
                            startDelay: Duration(seconds: 1),
                            child: Material(
                              elevation: 30,
                              shape: CircleBorder(),
                              child: CircleAvatar(
                                backgroundColor: AppColors.whiteColor,
                                minRadius: 10,
                                maxRadius: 45,
                                child: Image(
                                  image: AssetImage('assets/images/avator.png'),
                                  width: 84,
                                ),
                              ),
                            ),
                          ),
                        ),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.13,
                        margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.025),
                        decoration: BoxDecoration(
                            color: AppColors.lightGray, shape: BoxShape.circle),
                      ),
                    ),
                    Column(
                      children: [
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.17),
                            child: Text(
                                controller.member.value.firstName +
                                    " " +
                                    controller.member.value.middleName,
                                style: TextStyle(
                                    color: AppColors.primaryColor,
                                    fontSize: 20)),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Center(
                          child: Text('Edit Profile'.tr,
                              style: TextStyle(
                                color: AppColors.Primarycolor_one,
                              )),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text('Nationality'.tr,
                                    style: TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 16)),
                                Text(controller.member.value.nationality,
                                    style: TextStyle(
                                      color: AppColors.Primarycolor_one,
                                    )),
                                SizedBox(
                                  height: 20,
                                ),
                                Text('Sex'.tr,
                                    style: TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 16)),
                                Text(
                                    controller.member.value.isMale
                                        ? "Male".tr
                                        : "Female".tr,
                                    style: TextStyle(
                                      color: AppColors.Primarycolor_one,
                                    )),
                                SizedBox(
                                  height: 20,
                                ),
                                Text('Status'.tr,
                                    style: TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 16)),
                                Text(
                                    controller.member.value.maritalStatus == 1
                                        ? "Married".tr
                                        : "Unmarried".tr,
                                    style: TextStyle(
                                      color: AppColors.Primarycolor_one,
                                    )),
                              ],
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: 2,
                              height: MediaQuery.of(context).size.height * 0.2,
                              color: AppColors.Primarycolor_two,
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Phone Number'.tr,
                                    style: TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 16)),
                                Text(controller.member.value.telephone,
                                    style: TextStyle(
                                      color: AppColors.Primarycolor_one,
                                    )),
                                // SizedBox(
                                //   height: 20,
                                // ),
                                // Text('Date Of Birth'.tr,
                                //     style: TextStyle(
                                //         color: AppColors.primaryColor,
                                //         fontSize: 16)),
                                // Text(
                                //     controller.member.value.birthDate
                                //         .toString(),
                                //     style: TextStyle(
                                //       color: AppColors.Primarycolor_one,
                                //     )),
                                SizedBox(
                                  height: 20,
                                ),
                                Text('Email'.tr,
                                    style: TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 16)),
                                Text(controller.member.value.email,
                                    style: TextStyle(
                                      color: AppColors.Primarycolor_one,
                                    )),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                )),
        ),
      ),
    );
  }
}
