import 'package:e_kuteba/app/modules/basicDetail/member_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../common/util/exports.dart';
import '../../widgets/appbar.dart';
import '../../basicDetail/controllers/basic_detail_controller.dart';

class MemberShareInfo extends StatelessWidget {
  final controller = Get.put(BasicDetailController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBar(context, 'Member Share Info'.tr),
        body: Obx(() {
          return SingleChildScrollView(
              child: customVerticalCard(context, controller.member.value));
        }));
  }

  Widget customVerticalCard(
    BuildContext context,
    Member member,
  ) {
    return Container(
      margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Total Subscribed Amount'.tr,
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(
              Utils.numberFormat(
                member.subscriptionTotal,
              ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Total Paid Amount'.tr,
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(
              member.subscriptionTotal == null || member.subscriptionTotal == ''
                  ? 'ETB 0.0'
                  : Utils.numberFormat(
                      member.totalPurchase,
                    ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Share Units'.tr,
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(member.units.toInt().toString(),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
