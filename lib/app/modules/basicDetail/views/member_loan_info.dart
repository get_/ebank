// ignore_for_file: annotate_overrides, unrelated_type_equality_checks

import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/basicDetail/member_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../basicDetail/controllers/basic_detail_controller.dart';

class LoanInfo extends GetView<BasicDetailController> {
  final controller = Get.put(BasicDetailController());

  LoanInfo({Key? key}) : super(key: key);
  // final hom = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, 'Loan Info'.tr),
      body: Obx(
        () {
          return SingleChildScrollView(
              child: customVerticalCard(context, controller.member.value));
        },
      ),
    );
  }

  Widget customVerticalCard(
    BuildContext context,
    Member member,
  ) {
    return Container(
      margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Member Loan Information'.tr,
            style: const TextStyle(color: Color(0xffff9a00), fontSize: 24),
          ),
          const SizedBox(
            height: 14,
          ),
          Text('Loan Amount'.tr,
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                fontSize: 20,
                color: AppColors.primaryColor,
              ))),
          Text(
              member.approvedAmount == '' || member.approvedAmount == null
                  ? member.approvedAmount
                  : Utils.numberFormat(
                      member.approvedAmount,
                    ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text('Loan Interest Amount'.tr,
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                fontSize: 20,
                color: AppColors.primaryColor,
              ))),
          Text(
              member.loanInterestAmount == '' ||
                      member.loanInterestAmount == null
                  ? member.loanInterestAmount
                  : Utils.numberFormat(
                      member.loanInterestAmount,
                    ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text('Remaining Amount'.tr,
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                fontSize: 20,
                color: AppColors.primaryColor,
              ))),
          Text(
              member.remainingAmount == '' || member.remainingAmount == null
                  ? member.remainingAmount
                  : Utils.numberFormat(
                      member.remainingAmount,
                    ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          const SizedBox(
            height: 10,
          ),
          Text('Monthly Payment'.tr,
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                fontSize: 20,
                color: AppColors.primaryColor,
              ))),
          Text(
              member.monthlyPayment == null || member.monthlyPayment == ''
                  ? 'ETB 0.00'
                  : Utils.numberFormat(
                      member.monthlyPayment,
                    ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
