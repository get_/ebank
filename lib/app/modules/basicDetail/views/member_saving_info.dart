// ignore_for_file: annotate_overrides, prefer_const_constructors

import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/basicDetail/member_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../basicDetail/controllers/basic_detail_controller.dart';


class SavingInfo extends GetView<BasicDetailController> {
  final controller = Get.put(BasicDetailController());

  SavingInfo({Key? key}) : super(key: key);
  // final hom = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBar(context, 'Member Saving Info'.tr),
        body: Obx(() {
          return SingleChildScrollView(
              child: customVerticalCard(context, controller.member.value));
        }));
  }

  Widget customVerticalCard(
    BuildContext context,
    Member member,
  ) {
    return Container(
      margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Mandatory Saving'.tr,
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(
              Utils.numberFormat(
                 member.mandatorySaving == ''
                  ? '0.0'
                  : 
                member.mandatorySaving,
              ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Voluntary Saving'.tr,
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(
            Utils.numberFormat(
                 member.voluntarySaving == ''
                  ? '0.0'
                  : 
               member.voluntarySaving,
              ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Children Saving'.tr,
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(
            Utils.numberFormat(
                 member.childSaving == ''
                  ? '0.0'
                  : 
                member.childSaving,
              ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Time Limit Deposit'.tr,
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(
          Utils.numberFormat(
                 member.timeLimitedSaving == ''
                  ? '0.0'
                  : 
                member.timeLimitedSaving,
              ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Saving Amount'.tr,
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
              fontSize: 20,
              color: AppColors.primaryColor,
            )),
          ),
          Text(
             Utils.numberFormat(
                 member.timeLimitedSaving == ''
                  ? '0.0'
                  : 
                member.savingAmount,
              ),
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      fontSize: 20,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold))),
          const SizedBox(
            height: 10,
          ),
          // Text(
            // 'Interest',
          //   style: GoogleFonts.poppins(
          //       textStyle: const TextStyle(
          //     fontSize: 20,
          //     color: AppColors.primaryColor,
          //   )),
          // ),
          // Text(member.interesetAmount.toString(),
          //     style: GoogleFonts.poppins(
          //         textStyle: const TextStyle(
          //             fontSize: 20,
          //             color: AppColors.primaryColor,
          //             fontWeight: FontWeight.bold))),
          // const SizedBox(
          //   height: 10,
          // ),
        ],
      ),
    );
  }
}
