// ignore_for_file: prefer_const_constructors

import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/widgets/custom_drawer_widget.dart';
import 'package:e_kuteba/app/modules/widgets/custom_loading_widget.dart';
import 'package:e_kuteba/app/modules/widgets/no_data.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../common/util/exports.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/approved_loans_controller.dart';

class ApprovedLoansView extends GetView<ApprovedLoansController> {
  final hController = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, 'Approved Loan'.tr),
      drawer: CustomDrawerWidget(context, hController),
      body: Theme(
        data: ThemeData(
            primarySwatch: Colors.orange,
            colorScheme: ColorScheme.light(primary: AppColors.primaryColor)),
        child: Obx(
          () => controller.isNotfetched.value
              ? const Center(child: loadingWidget())
              : controller.apprrovedLoans.isEmpty
                  ? NoDataPage()
                  : Padding(
                      padding: const EdgeInsets.all(10),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              DataTable(
                                  showBottomBorder: true,
                                  showCheckboxColumn: true,
                                  headingRowColor: MaterialStateProperty.all(
                                      Colors.grey.shade300),
                                  columns: <DataColumn>[
                                    DataColumn(
                                      label: Text(
                                        'approved Amount'.tr,
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic),
                                      ),
                                    ),
                                    DataColumn(
                                      label: Text(
                                        'paid Amount'.tr,
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic),
                                      ),
                                    ),
                                    DataColumn(
                                      label: Text(
                                        'duration Month'.tr,
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic),
                                      ),
                                    ),
                                    DataColumn(
                                      label: Text(
                                        'payment start Date'.tr,
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic),
                                      ),
                                    ),
                                    DataColumn(
                                      label: Text(
                                        'payment end Date'.tr,
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic),
                                      ),
                                    ),
                                  ],
                                  rows: controller.apprrovedLoans
                                      .map(
                                        (element) => DataRow(
                                          cells: <DataCell>[
                                            DataCell(Text(Utils.numberFormat(
                                                element.approvedAmount))),
                                            DataCell(Text(Utils.numberFormat(
                                                    element.paidAmount) +
                                                'ETB')),
                                            DataCell(Text(element.durationMonth
                                                .toString())),
                                            DataCell(Text(controller.todate(
                                                element.paymentStartDate))),
                                            DataCell(Text(controller.todate(
                                                element.paymentEndDate))),
                                          ],
                                        ),
                                      )
                                      .toList()),
                            ],
                          ),
                        ),
                      ),
                    ),
        ),
      ),
    );
  }
}
