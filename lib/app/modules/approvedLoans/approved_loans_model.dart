// To parse required this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

List<ApprovedLoans> approvedLoansFromJson(String str) => List<ApprovedLoans>.from(
    json.decode(str).map((x) => ApprovedLoans.fromJson(x)));

String approvedLoansToJson(List<ApprovedLoans> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ApprovedLoans {
  ApprovedLoans({
    required this.id,
    required this.memberId,
    required this.loanRequestId,
    required this.approvedAmount,
    required this.paidAmount,
    required this.durationMonth,
    required this.approvedDate,
    required this.paymentStartDate,
    required this.remark,
    required this.paymentEndDate,
    required this.loanPaybackPeriod,
  });

  String id;
  String memberId;
  String loanRequestId;
  double approvedAmount;
  double paidAmount;
  int durationMonth;
  int approvedDate;
  int paymentStartDate;
  String remark;
  int paymentEndDate;
  int loanPaybackPeriod;

  factory ApprovedLoans.fromJson(Map<String, dynamic> json) => ApprovedLoans(
        id: json["id"],
        memberId: json["member_id"],
        loanRequestId: json["loan_request_id"],
        approvedAmount: json["approved_amount"],
        paidAmount: json["paid_amount"],
        durationMonth: json["duration_month"],
        approvedDate: json["approved_date"],
        paymentStartDate: json["payment_start_date"],
        remark: json["remark"],
        paymentEndDate: json["payment_end_date"],
        loanPaybackPeriod: json["loan_payback_period"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "member_id": memberId,
        "loan_request_id": loanRequestId,
        "approved_amount": approvedAmount,
        "paid_amount": paidAmount,
        "duration_month": durationMonth,
        "approved_date": approvedDate,
        "payment_start_date": paymentStartDate,
        "remark": remark,
        "payment_end_date": paymentEndDate,
        "loan_payback_period": loanPaybackPeriod,
      };
}
