import 'package:get/get.dart';

import '../controllers/approved_loans_controller.dart';

class ApprovedLoansBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ApprovedLoansController>(
      () => ApprovedLoansController(),
    );
  }
}
