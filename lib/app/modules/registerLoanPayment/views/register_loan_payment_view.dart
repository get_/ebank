// ignore_for_file: prefer_const_constructors

import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../common/util/exports.dart';
import '../../../common/util/validators.dart';
import '../../widgets/custom_top_snackbar.dart';
import '../controllers/register_loan_payment_controller.dart';

class RegisterLoanPaymentView extends GetView<RegisterLoanPaymentController> {
  final _formKey = GlobalKey<FormState>();

  var autovalidate = AutovalidateMode.disabled;
  List<Step> stepList() => [
        Step(
          state: controller.activeStepIndex.value <= 0
              ? StepState.editing
              : StepState.complete,
          isActive: controller.activeStepIndex.value >= 0,
          title: Text(
            'Basic Fields'.tr,
            style: TextStyle(fontFamily: 'Bebas', letterSpacing: 2),
          ),
          content: Container(
            child: Form(
              key: _formKey,
              autovalidateMode: autovalidate,
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      // Text('Registration Date'.tr + ': '),
                      // Padding(
                      //   padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                      //   child: GestureDetector(
                      //     onTap: () {
                      //       controller.selectStartDate(Get.context!);
                      //     },
                      //     child: Container(
                      //       height: 45,
                      //       width: MediaQuery.of(Get.context!).size.width * 0.9,
                      //       decoration: BoxDecoration(
                      //         borderRadius: BorderRadius.circular(15),
                      //         color: Colors.grey.shade200,
                      //       ),
                      //       child: Row(
                      //         children: [
                      //           Padding(
                      //             padding: const EdgeInsets.symmetric(
                      //                 horizontal: 10, vertical: 10),
                      //             child: Icon(Icons.date_range),
                      //           ),
                      //           Obx(() => Text(controller.startDate.value))
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      // SizedBox(
                      //   width: 15,
                      // ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            return Validators.notEmpty(value);
                          },
                          controller: controller.depositAmountController,
                          decoration: InputDecoration(
                            label: Text('Deposit Amount'),
                            hintText: 'Enter Amount',
                            border: OutlineInputBorder(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(4.0)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: InputDecorator(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(4.0)),
                            // contentPadding: EdgeInsets.all(10),
                          )),
                          child: Obx(() => DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  value: controller.dropdownvalue1.value,
                                  isDense: true,
                                  isExpanded: true,
                                  items: controller.items.map((String items) {
                                    return DropdownMenuItem(
                                      value: items,
                                      child: items == 'select bank...'
                                          ? Row(
                                              children: const [
                                                Center(
                                                    child:
                                                        CircularProgressIndicator()),
                                                Text('Loading...')
                                              ],
                                            )
                                          : Text(items,
                                              textAlign: TextAlign.start),
                                    );
                                  }).toList(),
                                  onChanged: (newValue) {
                                    controller.getbankTypeId(newValue);
                                    controller.dropdownvalue1.value = newValue!;
                                  },
                                ),
                              )),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          ),
        ),
        Step(
          state: controller.activeStepIndex.value <= 1
              ? StepState.editing
              : StepState.complete,
          isActive: controller.activeStepIndex.value >= 1,
          title: Text(
            'Loan Selection'.tr,
            style: TextStyle(
              fontFamily: 'Bebas',
              letterSpacing: 2,
            ),
          ),
          content: Container(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: DataTable(
                      showBottomBorder: true,
                      showCheckboxColumn: true,
                      headingRowColor:
                          MaterialStateProperty.all(Colors.grey.shade300),
                      columns: <DataColumn>[
                        DataColumn(
                          label: Text(
                            'approvedAmount'.tr,
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'paidAmount'.tr,
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'durationMonth'.tr,
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'approvedDate'.tr,
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                      ],
                      rows: controller.acontroller.apprrovedLoans
                          .map(
                            (element) => DataRow(
                              selected:
                                  controller.selectedDoc.contains(element),
                              onSelectChanged: (b) {
                                controller.onRowSelected(b, element);
                              },
                              cells: <DataCell>[
                                DataCell(Text(
                                    Utils.numberFormat(element.approvedAmount)
                                        .toString())),
                                DataCell(Text(
                                    Utils.numberFormat(element.paidAmount)
                                        .toString())),
                                DataCell(
                                    Text(element.durationMonth.toString())),
                                DataCell(Text(Utils.convertToDate(
                                    Utils.toDate(element.approvedDate)
                                        .toString()))),
                              ],
                            ),
                          )
                          .toList()),
                ),
              ),
            ),
          ),
        ),
        Step(
            state: StepState.complete,
            isActive: controller.activeStepIndex.value >= 2,
            title: Text(
              'Confirm Details'.tr,
              style: TextStyle(
                fontFamily: 'Bebas',
                letterSpacing: 2,
              ),
            ),
            content: Container(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(children: [
                  Text(
                    'Bank'.tr + ": ",
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(controller.dropdownvalue1.value),
                ]),
                Row(children: [
                  Text(
                    "Approved Amount".tr + ": ",
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  controller.selectedDoc.isNotEmpty
                      ? Expanded(
                          child: Text(Utils.numberFormat(
                                  controller.selectedDoc[0].approvedAmount)
                              .toString()),
                        )
                      : Expanded(child: Text('loan is not selected'.tr)),
                ]),
                Row(children: [
                  Text(
                    "Deposite Amount".tr + ": ",
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(controller.depositAmountController.text.isEmpty
                      ? "no deposit amount added"
                      : Utils.numberFormat(double.parse(
                              controller.depositAmountController.text))
                          .toString()),
                ]),
              ],
            )))
      ];

  BuildContext? ctx;

  @override
  Widget build(BuildContext context) {
    ctx = context;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: buildAppBar(context, 'New Loan Payment'.tr),
      bottomSheet: Obx(() => controller.sending.value
          ? const SizedBox(
              height: 7,
              child: LinearProgressIndicator(
                backgroundColor: AppColors.primaryColor,
                color: AppColors.Primarycolor_one,
              ),
            )
          : const Visibility(
              visible: false,
              replacement: SizedBox.shrink(),
              child: SizedBox(),
            )),
      body: Theme(
        data: ThemeData(
            primarySwatch: Colors.orange,
            colorScheme: ColorScheme.light(primary: AppColors.primaryColor)),
        child: Obx(() => Stepper(
              type: StepperType.vertical,
              currentStep: controller.activeStepIndex.value,
              steps: stepList(),
              onStepContinue: () {
                if (_formKey.currentState!.validate()) {
                  if (controller.activeStepIndex.value <
                      (stepList().length - 1)) {
                    controller.activeStepIndex.value += 1;
                  } else {
                    if ((controller.selectedDoc.isEmpty)) {
                      ShowTopErrorSnackBar(
                          context, 'please select approoved loan!'.tr);
                    } else if ((controller.selectedDoc.isEmpty) &&
                        (DateTime.tryParse(controller.startDate.value) ==
                            null)) {
                      ShowTopErrorSnackBar(
                          context, 'please select start date and doctor'.tr);
                    } else {
                      controller.RegisterLoanPayment(context);
                    }
                  }
                } else {
                  autovalidate = AutovalidateMode.onUserInteraction;
                }
              },
              onStepCancel: () {
                if (controller.activeStepIndex.value == 0) {
                  return;
                }

                controller.activeStepIndex.value -= 1;
              },
              onStepTapped: (int index) {
                controller.activeStepIndex.value = index;
              },
              controlsBuilder: (context, details) {
                final isLastStep =
                    controller.activeStepIndex.value == stepList().length - 1;

                return Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: details.onStepContinue,
                        child:
                            (isLastStep) ? Text('Submit'.tr) : Text('Next'.tr),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    if (controller.activeStepIndex.value > 0)
                      Expanded(
                        child: ElevatedButton(
                          onPressed: details.onStepCancel,
                          child: Text('Back'.tr),
                        ),
                      ),
                  ],
                );
              },
            )),
      ),
    );
  }
}
