import 'package:get/get.dart';

import '../controllers/register_loan_payment_controller.dart';

class RegisterLoanPaymentBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RegisterLoanPaymentController>(
      () => RegisterLoanPaymentController(),
    );
  }
}
