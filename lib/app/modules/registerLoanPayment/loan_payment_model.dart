// To parse required this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

LoanPaymentModel loanPaymentModelFromJson(String str) => LoanPaymentModel.fromJson(json.decode(str));

String loanPaymentModelToJson(LoanPaymentModel data) => json.encode(data.toJson());

class LoanPaymentModel {
    LoanPaymentModel({
        required this.approvedLoanId,
        required this.bankId,
        required this.depositAmount,
        required this.memberId,
        required this.penaltyAmount,
        required this.remark,
    });

    String approvedLoanId;
    String bankId;
    double depositAmount;
    String memberId;
    double penaltyAmount;
    String remark;

    factory LoanPaymentModel.fromJson(Map<String, dynamic> json) => LoanPaymentModel(
        approvedLoanId: json["approved_loan_id"],
        bankId: json["bank_id"],
        depositAmount: json["deposit_amount"],
        memberId: json["member_id"],
        penaltyAmount: json["penalty_amount"],
        remark: json["remark"],
    );

    Map<String, dynamic> toJson() => {
        "approved_loan_id": approvedLoanId,
        "bank_id": bankId,
        "deposit_amount": depositAmount,
        "member_id": memberId,
        "penalty_amount": penaltyAmount,
        "remark": remark,
    };
}
