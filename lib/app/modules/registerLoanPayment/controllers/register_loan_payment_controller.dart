
import 'package:e_kuteba/app/modules/registerLoanPayment/loan_payment_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

import '../../../common/util/connection.dart';
import '../../../common/util/exports.dart';
import '../../../data/api_helper.dart';
import '../../../data/errors/api_error_model.dart';
import '../../approvedLoans/approved_loans_model.dart';
import '../../approvedLoans/controllers/approved_loans_controller.dart';
import '../../saving_request/banks_model.dart';
import '../../widgets/toast.dart';

class RegisterLoanPaymentController extends GetxController {
  final acontroller = Get.put(ApprovedLoansController());
  final ApiHelper _apiHelper = Get.find();
  TextEditingController depositAmountController = TextEditingController();
  @override
  void onInit() {
    getbanks();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  RxBool sending = false.obs;

  void RegisterLoanPayment(context) async {
    sending.value = true;

    LoanPaymentModel loanPaymentModel = LoanPaymentModel(
        approvedLoanId: selectedDoc[0].id,
        bankId: bankTypeId.value,
        depositAmount: double.parse(depositAmountController.text),
        memberId: selectedDoc[0].memberId,
        penaltyAmount: 0,
        remark: 'remark');
    try {
      check().then((intenet) async {
        if (intenet != null && intenet) {
          await _apiHelper.postRegisterLoanPayment(loanPaymentModel).then(
            (value) {
              sending.value = false;
              if (value.statusCode!.clamp(200, 299) == value.statusCode) {
                toast('You register loan Payment Successfully'.tr);
                Get.back();
              } else if (value.statusCode!.clamp(500, 599) ==
                  value.statusCode) {
                var err = apiErrorFromJson(value.body);
                ShowsSnackBar(context,
                    err.msgs[0] == '' ? 'Internal Server Error!' : err.msgs[0]);
              } else if (value.statusCode!.clamp(400, 499) ==
                  value.statusCode) {
                var err = apiErrorFromJson(value.body);
                ShowsSnackBar(context,
                    err.msgs[0] == '' ? 'Resource not found.' : err.msgs[0]);
              } else {
                toast('operation failed');
              }
            },
          );
        } else {
          ShowsSnackBar(context, 'No connection');
        }
      });
    } catch (e) {
      toast(e.toString());
    }
  }

  int warrantNumber(String warrant) {
    var val = 100;
    switch (warrant) {
      case 'Monthly Income':
        val = 100;
        break;
      case 'Share':
        val = 102;
        break;
      case 'Car Libre':
        val = 101;
        break;
      case 'House Map':
        val = 103;
        break;
      default:
        val = 100;
    }
    return val;
  }

  RxInt activeStepIndex = 0.obs;

  RxBool checkboxSelected = false.obs;
  RxList<ApprovedLoans> selectedDoc = <ApprovedLoans>[].obs;
  onRowSelected(selected, ApprovedLoans data) {
    checkboxSelected.value = true;
    if (selected) {
      selectedDoc.clear();
      selectedDoc.add(data);
    } else {
      selectedDoc.clear();
    }
  }

  //for bank
  var items = ['select bank...'];
  RxString dropdownvalue1 = 'select bank...'.obs;
  RxString bankTypeId = ''.obs;
  RxList<Bank> banktype = <Bank>[].obs;

  void getbanks() async {
    await _apiHelper.getBanks().then((value) {
      banktype.value = banksFromJson(value.body).data;
      items.clear();
      for (var item in banktype) {
        items.add(item.name);
        dropdownvalue1.value = items[0];
      }
    });
  }

  getbankTypeId(val) {
    for (var item in banktype) {
      if (item.name == val) {
        bankTypeId.value = item.id;
      }
    }
  }

  RxString startDate = 'select date'.tr.obs;
  double startDateToEpoch = 0;
  Rx<DateTime> currentDate = DateTime.now().obs;
  Future<void> selectStartDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: DateTime(2020),
        lastDate: DateTime(2050),
        builder: (context, picker) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: const ColorScheme.dark(
                primary: AppColors.secondaryColor,
                onPrimary: Colors.white,
                surface: AppColors.secondaryColor,
                onSurface: Colors.white,
              ),
              dialogBackgroundColor: AppColors.primaryColor,
            ),
            child: picker!,
          );
        }).then((selectedDate) {
      if (selectedDate != null) {
        var parsedDate = DateTime.parse(selectedDate.toString());
        startDate.value = DateFormat("dd-MM-yyyy").format(parsedDate);
        var start_date = (selectedDate).millisecondsSinceEpoch;
        var start = (start_date / 1000).round();
        startDateToEpoch = start as double;
      }
    });
  }
}
