// ignore_for_file: prefer_const_constructors

import 'package:avatar_glow/avatar_glow.dart';
import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/dashboard_controller.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class DashboardView extends GetView<DashboardController> {
  final controller = Get.put(DashboardController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Color.fromARGB(255, 234, 252, 233),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(48.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Center(
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Center(
                        child: Image(
                          image: AssetImage('assets/images/welcome.png'),
                        ),
                      ),
                      Center(
                        child: AnimatedTextKit(
                          animatedTexts: [
                            TypewriterAnimatedText(
                              'Hello!',
                              textStyle: const TextStyle(
                                fontSize: 32.0,
                                color: AppColors.Primarycolor_two,
                                fontWeight: FontWeight.bold,
                              ),
                              speed: const Duration(milliseconds: 100),
                            ),
                            TypewriterAnimatedText(
                              'Welcome to',
                              textStyle: const TextStyle(
                                fontSize: 32.0,
                                color: AppColors.Primarycolor_two,
                                fontWeight: FontWeight.bold,
                              ),
                              speed: const Duration(milliseconds: 100),
                            ),
                            TypewriterAnimatedText(
                              'Sheger!',
                              textStyle: const TextStyle(
                                fontSize: 32.0,
                                color: AppColors.Primarycolor_two,
                                fontWeight: FontWeight.bold,
                              ),
                              speed: const Duration(milliseconds: 100),
                            ),
                          ],
                          totalRepeatCount: 4,
                          pause: const Duration(milliseconds: 100),
                          displayFullTextOnTap: true,
                          stopPauseOnTap: true,
                        ),
                      ),
                      Center(
                        child: SizedBox(
                          height: 12,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: (() => controller.onTap()),
                child: Container(
                  height: 40,
                  width: 230,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100.0),
                    color: AppColors.primaryColor,
                  ),
                  child: Center(
                    child: Text(
                      'Login'.toUpperCase(),
                      style: const TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: AppColors.whiteColor,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
