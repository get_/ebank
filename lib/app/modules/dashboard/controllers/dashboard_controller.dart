import 'package:e_kuteba/app/common/constants.dart';
import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:oauth2_client/access_token_response.dart';
import 'package:oauth2_client/oauth2_client.dart';

import '../../basicDetail/controllers/basic_detail_controller.dart';

class DashboardController extends GetxController {
  @override
  void onClose() {}

  void onTap() {
    OAuth2Client client = MyOAuth2Client(
      customUriScheme: Constants.ESCHEMA,
      redirectUri: Constants.REDIRECT_URI,
    );
    late AccessTokenResponse tknResp = AccessTokenResponse();
    client.getTokenWithImplicitGrantFlow(
        enableState: false,
        clientId: Constants.CLIENT_ID,
        scopes: ['user']).then((value) {
      tknResp = value;
      Storage.storage.write(Constants.TOKEN, tknResp.accessToken.toString());
       Get.offAllNamed(Routes.HOME);
      Get.put(BasicDetailController()).fetch();
    });
  }
}

class MyOAuth2Client extends OAuth2Client {
  MyOAuth2Client({required String redirectUri, required String customUriScheme})
      : super(
            authorizeUrl:
                Constants.AUTHORIZE_URL, //Your service's authorization url
            tokenUrl: Constants.TOKEN_URL, //Your service access token url
            redirectUri: redirectUri,
            customUriScheme: customUriScheme) {
    this.accessTokenRequestHeaders = {'Accept': 'application/json'};
  }
}
