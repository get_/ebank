// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

List<Children> ChildrenFromJson(String str) =>
    List<Children>.from(json.decode(str).map((x) => Children.fromJson(x)));

String ChildrenToJson(List<Children> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Children {
  Children({
    required this.id,
    required this.memberId,
    required this.name,
    required this.isMale,
    required this.birthDate,
  });

  String id;
  String memberId;
  String name;
  bool isMale;
  int birthDate;

  factory Children.fromJson(Map<String, dynamic> json) => Children(
        id: json["id"],
        memberId: json["member_id"],
        name: json["name"],
        isMale: json["is_male"],
        birthDate: json["birth_date"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "member_id": memberId,
        "name": name,
        "is_male": isMale,
        "birth_date": birthDate,
      };
}
