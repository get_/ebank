// ignore_for_file: prefer_final_fields, unnecessary_overrides

import 'package:e_kuteba/app/common/constants.dart';
import 'package:e_kuteba/app/common/util/connection.dart';
import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/data/errors/api_error_model.dart';
import 'package:e_kuteba/app/modules/children/children_model.dart';
import 'package:e_kuteba/app/modules/widgets/toast.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class ChildrenController extends GetxController
    with StateMixin<List<Children>> {
  ApiHelper _apiHelper = Get.find();

  @override
  void onInit() {
    if (Storage.storage.hasData(Constants.TOKEN)) {
      fetch();
    }

    super.onInit();
  }

  @override
  void onReady() {
    fetch();
    super.onReady();
  }

  @override
  void onClose() {}


  RxBool isLoading = true.obs;
  RxBool isNotfetched = true.obs;
  RxList<Children> children = <Children>[].obs;
  fetch() async {
    isLoading.value = true;
    try {
      check().then((intenet) async {
        if (intenet != null && intenet) {
          _apiHelper.getchildren().then((value) {
           if (value.statusCode!.clamp(200, 299) == value.statusCode) {
              isLoading.value = false;
              change(ChildrenFromJson(value.body), status: RxStatus.success());
              children.value = ChildrenFromJson(value.body);
           } else if (value.statusCode!.clamp(500, 599) == value.statusCode) {
              isLoading.value = false;
              var err = apiErrorFromJson(value.body);
              toast(err.msgs[0]);
            } else if (value.statusCode!.clamp(400, 499) == value.statusCode) {
              isLoading.value = false;
            toast('Resource not found.');
            } else {
              isLoading.value = false;
             toast('operation failed');
            }
          }, onError: (err) {
            change(null, status: RxStatus.error(err.toString()));
          });
        } else {
          toast('no connection!');
        }
      });
    } catch (e) {}
  }
}
