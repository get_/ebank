// ignore_for_file: prefer_const_constructors

import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/widgets/custom_drawer_widget.dart';
import 'package:e_kuteba/app/modules/widgets/custom_loading_widget.dart';
import 'package:e_kuteba/app/modules/widgets/no_data.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../common/util/exports.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/children_controller.dart';

class ChildrenView extends GetView<ChildrenController> {
  final hController = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, 'Children'.tr),
      drawer: CustomDrawerWidget(context, hController),
      body: Theme(
        data: ThemeData(
            primarySwatch: Colors.orange,
            colorScheme: ColorScheme.light(primary: AppColors.primaryColor)),
        child: Obx(
          () => controller.isLoading.value
              ? Center(child: loadingWidget())
              : controller.children.isNotEmpty
                  ? Padding(
                      padding: const EdgeInsets.all(10),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              DataTable(
                                  showBottomBorder: true,
                                  showCheckboxColumn: true,
                                  headingRowColor: MaterialStateProperty.all(
                                      Colors.grey.shade300),
                                  columns: <DataColumn>[
                                    DataColumn(
                                      label: Text(
                                        'Name'.tr,
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic),
                                      ),
                                    ),
                                    DataColumn(
                                      label: Text(
                                        'Sex'.tr,
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic),
                                      ),
                                    ),
                                    DataColumn(
                                        label: Text(
                                      'Date of Birth'.tr,
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic),
                                    )),
                                  ],
                                  rows: controller.children
                                      .map(
                                        (element) => DataRow(
                                          cells: <DataCell>[
                                            DataCell(Text(element.name)),
                                            DataCell(Text(element.isMale
                                                ? 'Male'.tr
                                                : 'Female'.tr)),
                                            DataCell(Text(
                                                Utils.toDate(element.birthDate)
                                                    .toString())),
                                          ],
                                        ),
                                      )
                                      .toList()),
                            ],
                          ),
                        ),
                      ),
                    )
                  : Center(
                      child: NoDataPage(),
                    ),
        ),
      ),
    );
  }
}
