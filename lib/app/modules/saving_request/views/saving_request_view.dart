import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/saving_request/banks_model.dart';
import 'package:e_kuteba/app/modules/saving_request/saving_types_model.dart';
import 'package:e_kuteba/app/modules/widgets/custom_drawer_widget.dart';
import 'package:e_kuteba/app/modules/widgets/custom_top_snackbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/saving_request_controller.dart';

class SavingRequestView extends GetView<SavingRequestController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, 'Saving Request'.tr),
      bottomSheet: Obx(() => controller.sending.value
          ? const SizedBox(
              height: 7,
              child: LinearProgressIndicator(
                backgroundColor: AppColors.primaryColor,
                color: AppColors.Primarycolor_one,
              ),
            )
          : const Visibility(
              visible: false,
              replacement: SizedBox.shrink(),
              child: SizedBox(),
            )),
      // drawer: CustomDrawerWidget(context),
      body: Theme(
        data: ThemeData(
            primarySwatch: Colors.orange,
            colorScheme: ColorScheme.light(primary: AppColors.primaryColor)),
        child: Obx(() => Stepper(
              type: StepperType.vertical,
              currentStep: controller.activeStepIndex.value,
              steps: stepList(),
              onStepContinue: () {
                if (controller.activeStepIndex.value <
                    (stepList().length) - 1) {
                  controller.activeStepIndex.value += 1;
                } else {
                  if (controller.amount.text.isEmpty) {
                    ShowTopErrorSnackBar(context, 'Enter amount of saving.'.tr);
                  } else if (double.tryParse(controller.amount.text) == null) {
                    ShowTopErrorSnackBar(
                        context, 'Enter a valid amount of savings.'.tr);
                  } else if (controller.selectedBank.value.id == '') {
                    ShowTopErrorSnackBar(context, 'Select Bank'.tr);
                  } else if (controller.referance.text.isEmpty) {
                    ShowTopErrorSnackBar(context, 'Enter reference number'.tr);
                  } else if (controller.selectedSavingType.value.id == '') {
                    ShowTopErrorSnackBar(context, 'Select saving type'.tr);
                  } else if (controller.startDateToEpoch == 0) {
                    ShowTopErrorSnackBar(context, 'Select date'.tr);
                  } else {
                    controller.sendRequest();
                  }
                }
              },
              onStepCancel: () {
                if (controller.activeStepIndex.value == 0) {
                  return;
                } else if (controller.activeStepIndex.value ==
                    (stepList().length) - 1) {
                  Get.back();
                } else {
                  controller.activeStepIndex.value -= 1;
                }
              },
              onStepTapped: (int index) {
                controller.activeStepIndex.value = index;
              },
              // controlsBuilder: (context, details) {
              //   final isLastStep =
              //       controller.activeStepIndex.value == stepList().length - 1;

              //   return Row(
              //     children: [
              //       Expanded(
              //         child: ElevatedButton(
              //           onPressed: details.onStepContinue,
              //           child: (isLastStep)
              //               ? const Text('Submit')
              //               : const Text('Next'),
              //         ),
              //       ),
              //       const SizedBox(
              //         width: 10,
              //       ),
              //       if (controller.activeStepIndex.value > 0)
              //         Expanded(
              //           child: ElevatedButton(
              //             onPressed: details.onStepCancel,
              //             child: const Text('Back'),
              //           ),
              //         ),
              //     ],
              //   );
              // },

              // controlsBuilder: (BuildContext context, {VoidCallback onStepContinue,VoidCallback onStepCancel}) {
              // final isLastStep = controller.activeStepIndex.value == stepList().length - 1;
              //   return
              // },
            )),
      ),
    );
  }

  List<Step> stepList() => [
        Step(
          state: controller.activeStepIndex.value <= 0
              ? StepState.editing
              : StepState.complete,
          isActive: controller.activeStepIndex.value >= 0,
          title: Text(
            'Saving Amount'.tr,
            style: TextStyle(fontFamily: 'Bebas', letterSpacing: 2),
          ),
          content: Container(
            child: TextField(
              keyboardType: TextInputType.number,
              controller: controller.amount,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'amount'.tr,
              ),
            ),
          ),
        ),
        Step(
            state: controller.activeStepIndex.value <= 1
                ? StepState.editing
                : StepState.complete,
            isActive: controller.activeStepIndex.value >= 1,
            title: Text(
              'Select Bank'.tr,
              style: TextStyle(
                fontFamily: 'Bebas',
                letterSpacing: 2,
              ),
            ),
            content:
                // Container(
                //   height: MediaQuery.of(Get.context!).size.height * 0.25,
                //   child:
                Padding(
              padding: const EdgeInsets.all(10),
              child: SizedBox(
                height: 200,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Obx(
                      () =>
                          //  controller.banks.data.isEmpty
                          // ? const Text('Loading...')
                          //  Center(
                          //     child: const CircularProgressIndicator(
                          //       color: AppColors.Primarycolor_one,
                          //     ),
                          //   )
                          // :
                          DropdownButton<Bank>(
                        value: controller.selectedBank.value.id == ''
                            ? null
                            : controller.selectedBank.value,
                        isDense: true,
                        isExpanded: true,
                        hint: Text('Select Bank'.tr),
                        items: controller.banks.data.map((Bank item) {
                          return DropdownMenuItem(
                            value: item,
                            child: Text(item.name, textAlign: TextAlign.start),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          controller.selectedBank.value = newValue!;
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    TextField(
                      controller: controller.referance,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'referance'.tr,
                      ),
                    ),
                  ],
                ),
              ),
              // ),
            )),
        Step(
            state: StepState.complete,
            isActive: controller.activeStepIndex.value >= 2,
            title: Text(
              'Saving Type'.tr,
              style: TextStyle(
                fontFamily: 'Bebas',
                letterSpacing: 2,
              ),
            ),
            content: Container(
              height: MediaQuery.of(Get.context!).size.height * 0.1,
              child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Obx(() =>
                      // controller.types.data.isEmpty
                      // ? const Text('Loading...')
                      // const CircularProgressIndicator(
                      //     color: AppColors.Primarycolor_one,
                      //   )
                      // :

                      DropdownButton<SavingType>(
                        value: controller.selectedSavingType.value.id == ''
                            ? null
                            : controller.selectedSavingType.value,
                        isDense: true,
                        isExpanded: true,
                        hint: Text('Saving Type'.tr),
                        items: controller.types.data.map((SavingType items) {
                          return DropdownMenuItem(
                            value: items,
                            child: Text(items.name, textAlign: TextAlign.start),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          controller.selectedSavingType.value = newValue!;
                        },
                      ))),
            )),
        Step(
          state: controller.activeStepIndex.value <= 0
              ? StepState.editing
              : StepState.complete,
          isActive: controller.activeStepIndex.value >= 0,
          title: Text(
            'Date And Remark'.tr,
            style: TextStyle(fontFamily: 'Bebas', letterSpacing: 2),
          ),
          content: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        controller.selectStartDate(Get.context!);
                      },
                      child: Container(
                        height: 45,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey.shade200,
                        ),
                        child: Row(
                          children: [
                            const Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 10),
                              child: Icon(Icons.date_range),
                            ),
                            Obx(() => Text(controller.startDate.value))
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                child: TextField(
                  controller: controller.remark,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'remark'.tr,
                  ),
                ),
              ),
            ],
          ),
        ),
      ];
}
