import 'package:get/get.dart';

import '../controllers/saving_request_controller.dart';

class SavingRequestBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SavingRequestController>(
      () => SavingRequestController(),
    );
  }
}
