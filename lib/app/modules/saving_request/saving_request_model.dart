// To parse this JSON data, do
//
//     final savingRequest = savingRequestFromJson(jsonString);

import 'dart:convert';

SavingRequest savingRequestFromJson(String str) =>
    SavingRequest.fromJson(json.decode(str));

String savingRequestToJson(SavingRequest data) => json.encode(data.toJson());

class SavingRequest {
  SavingRequest({
    required this.memberId,
    this.remark,
    required this.savingTypeId,
    required this.bankId,
    required this.savingAmount,
    required this.transactionReference,
    required this.regDate,
  });

  String memberId;
  String? remark;
  String savingTypeId;
  String bankId;
  double savingAmount;
  String transactionReference;
  double regDate;

  factory SavingRequest.fromJson(Map<String, dynamic> json) => SavingRequest(
        memberId: json["member_id"],
        remark: json["remark"],
        savingTypeId: json["saving_type_id"],
        bankId: json["bank_id"],
        savingAmount: json["saving_amount"],
        transactionReference: json["transaction_reference"],
        regDate: json["reg_date"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "member_id": memberId,
        "remark": remark,
        "saving_type_id": savingTypeId,
        "bank_id": bankId,
        "saving_amount": savingAmount,
        "transaction_reference": transactionReference,
        "reg_date": regDate,
      };
}
