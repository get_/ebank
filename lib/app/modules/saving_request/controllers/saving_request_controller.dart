import 'package:e_kuteba/app/common/values/app_colors.dart';
import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/modules/basicDetail/controllers/basic_detail_controller.dart';
import 'package:e_kuteba/app/modules/coopMemberSelf/controllers/coop_member_self_controller.dart';
import 'package:e_kuteba/app/modules/saving_request/saving_request_model.dart';
import 'package:e_kuteba/app/modules/saving_request/saving_types_model.dart';
import 'package:e_kuteba/app/modules/widgets/toast.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:flutter/material.dart';
import '../banks_model.dart';
import 'package:e_kuteba/app/data/errors/api_error_model.dart';

class SavingRequestController extends GetxController {
  final ApiHelper _apiHelperImpl = Get.find();
  final BasicDetailController _selfController = Get.find();
  late TextEditingController amount;
  late TextEditingController referance;
  late TextEditingController remark;
  RxInt activeStepIndex = 0.obs;
  @override
  void onInit() {
    getSavingTypes();
    getBanks();
    _selfController.fetch();

    amount = TextEditingController();
    referance = TextEditingController();
    remark = TextEditingController();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    getSavingTypes();
    getBanks();
  }

  SavingTypes types = SavingTypes(data: [], total: 0);
  Rx<SavingType> selectedSavingType = SavingType(
          id: '',
          name: '',
          description: '',
          interestRate: 0,
          initialMinimumAmount: 0)
      .obs;
  Banks banks = Banks(data: [], total: 0);
  Rx<Bank> selectedBank =
      Bank(id: '', name: '', shortcode: '', description: '').obs;
  @override
  void onClose() {}
  getSavingTypes() {
    _apiHelperImpl.getSavingTypes().then((value) {
 if (value.statusCode == 200) {
        types = savingTypesFromJson(value.body);
      }
    });
  }

  getBanks() {
    _apiHelperImpl.getBanks().then((value) {
  if (value.statusCode == 200) {
        banks = banksFromJson(value.body);
      }
    });
  }

  RxBool sending = false.obs;

  sendRequest() {
    sending.value = true;

    SavingRequest sr = SavingRequest(
        memberId: _selfController.member.value.id!,
        savingTypeId: selectedSavingType.value.id,
        bankId: selectedBank.value.id,
        savingAmount: double.parse(amount.text),
        transactionReference: referance.text,
        regDate: startDateToEpoch.toDouble());
  _apiHelperImpl.savingRequest(sr).then((value) {
      sending.value = false;
      if (value.statusCode == 204) {
        toast('Succesfully submited.'.tr);
        Get.back();
      } else if (value.statusCode == 404) {
        toast('try again'.tr);
      } else if (value.statusCode == 500) {
        toast('try again'.tr);
      } else {
        ApiError err = apiErrorFromJson(value.body);
        toast(err.msgs[0]);
      }
    });
  }

  RxString startDate = 'Select Date'.tr.obs;
  int startDateToEpoch = 0;
  Rx<DateTime> currentDate = DateTime.now().obs;
  Future<void> selectStartDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: DateTime(2020),
        lastDate: DateTime(2050),
        builder: (context, picker) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: const ColorScheme.dark(
                primary: AppColors.secondaryColor,
                onPrimary: Colors.white,
                surface: AppColors.secondaryColor,
                onSurface: Colors.white,
              ),
              dialogBackgroundColor: AppColors.primaryColor,
            ),
            child: picker!,
          );
        }).then((selectedDate) {
      if (selectedDate != null) {
        var parsedDate = DateTime.parse(selectedDate.toString());
        startDate.value = DateFormat("dd-MM-yyyy").format(parsedDate);
        var start_date = (selectedDate).millisecondsSinceEpoch;
        var start = (start_date / 1000).round();
        startDateToEpoch = start;
      }
    });
  }
}
