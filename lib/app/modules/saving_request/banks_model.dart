// To parse this JSON data, do
//
//     final banks = banksFromJson(jsonString);

import 'dart:convert';

Banks banksFromJson(String str) => Banks.fromJson(json.decode(str));

String banksToJson(Banks data) => json.encode(data.toJson());

class Banks {
  Banks({
    required this.data,
    required this.total,
  });

  List<Bank> data;
  int total;

  factory Banks.fromJson(Map<String, dynamic> json) => Banks(
        data: List<Bank>.from(json["data"].map((x) => Bank.fromJson(x))),
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total,
      };
}

class Bank {
  Bank({
    required this.id,
    required this.name,
    required this.shortcode,
    required this.description,
  });

  String id;
  String name;
  String shortcode;
  String description;

  factory Bank.fromJson(Map<String, dynamic> json) => Bank(
        id: json["id"],
        name: json["name"],
        shortcode: json["shortcode"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "shortcode": shortcode,
        "description": description,
      };
}
