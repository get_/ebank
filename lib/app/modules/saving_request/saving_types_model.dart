// To parse this JSON data, do
//
//     final savingTypes = savingTypesFromJson(jsonString);

import 'dart:convert';

SavingTypes savingTypesFromJson(String str) =>
    SavingTypes.fromJson(json.decode(str));

String savingTypesToJson(SavingTypes data) => json.encode(data.toJson());

class SavingTypes {
  SavingTypes({
    required this.data,
    required this.total,
  });

  List<SavingType> data;
  int total;

  factory SavingTypes.fromJson(Map<String, dynamic> json) => SavingTypes(
        data: List<SavingType>.from(
            json["data"].map((x) => SavingType.fromJson(x))),
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total": total,
      };
}

class SavingType {
  SavingType({
    required this.id,
    required this.name,
    required this.description,
    required this.interestRate,
    required this.initialMinimumAmount,
  });

  String id;
  String name;
  String description;
  double interestRate;
  double initialMinimumAmount;

  factory SavingType.fromJson(Map<String, dynamic> json) => SavingType(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        interestRate: json["interest_rate"],
        initialMinimumAmount: json["initial_minimum_amount"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "interest_rate": interestRate,
        "initial_minimum_amount": initialMinimumAmount,
      };
}
