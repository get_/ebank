// ignore_for_file: unused_import, prefer_const_constructors, prefer_const_literals_to_create_immutables, annotate_overrides

import 'package:avatar_glow/avatar_glow.dart';
import 'package:e_kuteba/app/data/api_helper.dart';
import 'package:e_kuteba/app/modules/basicDetail/views/dividend.dart';
import 'package:e_kuteba/app/modules/coopMemberSelf/views/coop_member_self_view.dart';
import 'package:e_kuteba/app/modules/coopMemberSelf/views/member_home_view.dart';
import 'package:e_kuteba/app/modules/coopPurchase/views/coop_purchase_view.dart';
import 'package:e_kuteba/app/modules/coopSubscription/views/coop_subscription_view.dart';
import 'package:e_kuteba/app/modules/coopPurchase/coop_purchase_model.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../widgets/appbar.dart';
import '../../../routes/app_pages.dart';
import '../../basicDetail/controllers/basic_detail_controller.dart';
import '../../basicDetail/views/member_loan_info.dart';
import '../../basicDetail/views/member_more_info.dart';
import '../../basicDetail/views/member_saving_info.dart';
import '../../basicDetail/views/member_share_info.dart';
import '../../widgets/custom_drawer_widget.dart';
import '../../widgets/custom_loading_widget.dart';
import '../../widgets/custom_server_error_widget.dart';
import '../controllers/home_controller.dart';
import '../../../common/values/app_colors.dart';

class HomeView extends GetView<HomeController> {
  final bcontroller = Get.put(BasicDetailController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.whiteColor,
        appBar: buildAppBar(context, 'Home'),
        drawer: CustomDrawerWidget(context, controller),
        body: Theme(
          data: ThemeData(
            primarySwatch: Colors.orange,
            colorScheme: ColorScheme.light(primary: AppColors.primaryColor),
          ),
          child: SingleChildScrollView(
            child: Obx(
              () => bcontroller.isLoading.value
                  ? loadingWidget()
                  : bcontroller.member.value.firstName.isEmpty
                      ? ServerErrorWidget()
                      : Stack(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height * 0.25,
                              padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.17,
                              ),
                              decoration:
                                  BoxDecoration(color: AppColors.primaryColor),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      CircleAvatar(
                                        radius: 35,
                                        backgroundColor: AppColors.doveGray,
                                        child: AvatarGlow(
                                          endRadius: 80,
                                          duration: Duration(seconds: 2),
                                          glowColor: Colors.white24,
                                          repeat: true,
                                          repeatPauseDuration:
                                              Duration(seconds: 1),
                                          startDelay: Duration(seconds: 1),
                                          child: Material(
                                            elevation: 30,
                                            shape: CircleBorder(),
                                            child: CircleAvatar(
                                                backgroundColor:
                                                    AppColors.whiteColor,
                                                minRadius: 10,
                                                maxRadius: 45,
                                                child: Image(
                                                  image: AssetImage(
                                                      'assets/images/avator.png'),
                                                  width: 60,
                                                )),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.03,
                                      ),
                                      Expanded(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              bcontroller
                                                      .member.value.firstName +
                                                  " " +
                                                  bcontroller
                                                      .member.value.middleName,
                                              style: TextStyle(
                                                  color: AppColors.white,
                                                  fontSize: 20),
                                            ),
                                            SizedBox(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.03,
                                            ),
                                            Text(
                                              bcontroller.member.value.email,
                                              style: TextStyle(
                                                  color: AppColors.white),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.09,
                                  ),
                                  Divider(
                                    color: AppColors.white,
                                  ),
                                  // Row(
                                  //   mainAxisAlignment: MainAxisAlignment.center,
                                  //   children: [
                                  //     Text(
                                  //       'Total Saving in Birr: ',
                                  //       style: TextStyle(
                                  //           color: AppColors.white,
                                  //           fontSize: 16,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //     Obx(() => Text(
                                  //           bcontroller.member.value.savingTotal
                                  //               .toString(),
                                  //           style: TextStyle(
                                  //             color: AppColors.white,
                                  //             fontSize: 16,
                                  //           ),
                                  //         )),
                                  //     Text(
                                  //       'BR',
                                  //       style: TextStyle(
                                  //         color: AppColors.white,
                                  //         fontSize: 16,
                                  //       ),
                                  //     ),
                                  //   ],
                                  // ),
                                ],
                              ),
                            ),
                            Center(
                              child: Container(
                                margin: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.24,
                                ),
                                height:
                                    MediaQuery.of(context).size.height * 0.04,
                                width: MediaQuery.of(context).size.width * 0.17,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(
                                        MediaQuery.of(context).size.height *
                                            0.041),
                                    bottomRight: Radius.circular(
                                        MediaQuery.of(context).size.height *
                                            0.041),
                                  ),

                                  // shape: BoxShape.circle,
                                  color: AppColors.primaryColor,
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: AppColors.whiteColor,
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                height:
                                    MediaQuery.of(context).size.height * 0.6,
                                width: MediaQuery.of(context).size.width * 0.9,
                                margin: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.15,
                                ),
                                child: Center(
                                  child: ListView(
                                    children: [
                                      ListTile(
                                          trailing: Icon(
                                              Icons.arrow_forward_ios_rounded),
                                          leading: Icon(
                                            Icons.info_outline_rounded,
                                            color: AppColors.primaryColor,
                                          ),
                                          title: Text(
                                            'Basic Details'.tr,
                                            style: TextStyle(
                                                color:
                                                    AppColors.Primarycolor_one),
                                          ),
                                          onTap: () =>
                                              Get.toNamed(Routes.BASIC_DETAIL)),
                                      Divider(),
                                      ListTile(
                                          trailing: Icon(
                                              Icons.arrow_forward_ios_rounded),
                                          leading: Icon(
                                            Icons.currency_exchange_outlined,
                                            color: AppColors.primaryColor,
                                          ),
                                          title: Text(
                                            'Saving Information'.tr,
                                            style: TextStyle(
                                                color:
                                                    AppColors.Primarycolor_one),
                                          ),
                                          onTap: () =>
                                              Get.to(() => SavingInfo())),
                                      Divider(),
                                      ListTile(
                                          trailing: Icon(
                                              Icons.arrow_forward_ios_rounded),
                                          leading: Icon(
                                            Icons.share,
                                            color: AppColors.primaryColor,
                                          ),
                                          title: Text(
                                            'Share Information'.tr,
                                            style: TextStyle(
                                                color:
                                                    AppColors.Primarycolor_one),
                                          ),
                                          onTap: () =>
                                              Get.to(() => MemberShareInfo())),
                                      Divider(),
                                      ListTile(
                                          trailing: Icon(
                                              Icons.arrow_forward_ios_rounded),
                                          leading: Icon(
                                            Icons.credit_score,
                                            color: AppColors.primaryColor,
                                          ),
                                          title: Text(
                                            'Loan Information'.tr,
                                            style: TextStyle(
                                                color:
                                                    AppColors.Primarycolor_one),
                                          ),
                                          onTap: () =>
                                              Get.to(() => LoanInfo())),
                                      Divider(),
                                      ListTile(
                                          trailing: Icon(
                                              Icons.arrow_forward_ios_rounded),
                                          leading: Icon(
                                            Icons.safety_divider_rounded,
                                            color: AppColors.primaryColor,
                                          ),
                                          title: Text(
                                            'Dividend'.tr,
                                            style: TextStyle(
                                                color:
                                                    AppColors.Primarycolor_one),
                                          ),
                                          onTap: () =>
                                              Get.to(() => Dividend())),
                                      Divider(),
                                      ListTile(
                                          trailing: Icon(
                                              Icons.arrow_forward_ios_rounded),
                                          leading: Icon(
                                            Icons.more_horiz_sharp,
                                            color: AppColors.primaryColor,
                                          ),
                                          title: Text(
                                            'More'.tr,
                                            style: TextStyle(
                                                color:
                                                    AppColors.Primarycolor_one),
                                          ),
                                          onTap: () =>
                                              Get.to(() => MoreInfo())),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
            ),
          ),
        ));
  }
}
