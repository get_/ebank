import 'package:e_kuteba/app/common/constants.dart';
import 'package:e_kuteba/app/modules/dashboard/views/dashboard_view.dart';
import 'package:e_kuteba/app/modules/home/views/home_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../../../common/storage/storage.dart';
import '../controllers/home_controller.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  final cont = Get.put(HomeController());
  bool _isLoaded = false;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        setState(() {
          _isLoaded = true;
        });
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _isLoaded
            ? Storage.storage.hasData(Constants.TOKEN)
                ? HomeView()
                : DashboardView()
            : Lottie.asset(
                'assets/json/loading.json',
                controller: _controller,
                onLoaded: (comp) {
                  _controller.duration = comp.duration;
                  _controller.forward();
                },
              ),
      ),
    );
  }
}


// Lottie.asset('assets/loading.json',),