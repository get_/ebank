import 'package:get/get.dart';

import '../../../common/storage/storage.dart';
import '../../../common/util/localization.dart';
import '../../../routes/app_pages.dart';

class HomeController extends GetxController {
  var tabIndex = 0.obs;
  var token = ''.obs;

  RxString chosenValue = 'English'.obs;
  var langCtrl = Get.put(LocaleString());
  RxInt index = 0.obs;
  updateLanguage() {
    Storage.storage.write('lang', chosenValue.value);

    update();
  }

  void onLogoutClick() {
    Get.offAllNamed(Routes.DASHBOARD);
    Storage.clearStorage();
  }

  void changeTabIndex(int index) {
    tabIndex.value = index;
  }

  @override
  void onClose() {}
}
