// ignore_for_file: override_on_non_overriding_member, prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';

import '../../common/util/exports.dart';

class ServerErrorWidget extends StatelessWidget {
  const ServerErrorWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical:
                  MediaQuery.of(context).size.height * 0.3),
          child: Column(
            children: [
              Image(
                  image: AssetImage(
                      'assets/images/intenal_error.png')),
              Text(
                'OOOPS...',
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  color: AppColors.primaryColor,
                ),
              ),
              Text(
                'Server is not responding',
                style: TextStyle(
                  fontSize: 18,
                  color: AppColors.primaryColor,
                ),
              ),
            ],
          ),
        ),
      );
  }
}

