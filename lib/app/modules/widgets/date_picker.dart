import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../common/values/app_colors.dart';

int startDateToEpoch = 0;
DateTime currentDate = DateTime.now();
Future<int> selectStartDate(BuildContext context) async {
  final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: currentDate,
      firstDate: DateTime(2020),
      lastDate: DateTime(2050),
      builder: (context, picker) {
        return Theme(
          //TODO: change colors
          data: ThemeData.dark().copyWith(
            colorScheme: const ColorScheme.dark(
              primary: AppColors.secondaryColor,
              onPrimary: Colors.white,
              surface: AppColors.secondaryColor,
              onSurface: Colors.white,
            ),
            dialogBackgroundColor: AppColors.primaryColor,
          ),
          child: picker!,
        );
      }).then((selectedDate) {
    if (selectedDate != null) {
      var parseEndDate =
          DateTime.parse(selectedDate.add(Duration(hours: 24)).toString());

      var parsedDate = DateTime.parse(selectedDate.toString());
      // startDate = DateFormat("dd-MM-yyyy").format(parsedDate);

      var start_date = (selectedDate).millisecondsSinceEpoch;

      var start = (start_date / 1000).round();

      startDateToEpoch = start;
    }
  });
  return startDateToEpoch;
}
