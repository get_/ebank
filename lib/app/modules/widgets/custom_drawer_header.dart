import 'package:flutter/material.dart';

import '../../common/util/exports.dart';

class CustomDrawerHeader extends StatelessWidget {
  // final HomeController homeController;


  @override
  Widget build(BuildContext context) {
    // var con = Get.put(AccountholderController());
    return Container(
      color: AppColors.primaryColor,
      height: 160.w,
      // padding: EdgeInsets.fromLTRB(15.w, 15.w, 15.w, 7.w),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
              alignment: Alignment.topRight,
              child: IconButton(
                  onPressed: () {
                    // Get.currentRoute == Routes.LIST_ACCOUNTHOLDER
                    //     ? Get.back()
                    //     : Get.toNamed(Routes.LIST_ACCOUNTHOLDER);
                  },
                  icon: const Icon(
                    Icons.edit,
                    color: AppColors.secondaryColor,
                  ))),
          InkWell(
            onTap: () {
              // Get.currentRoute == Routes.LIST_ACCOUNTHOLDER
              //     ? Get.back()
              //     : Get.toNamed(Routes.LIST_ACCOUNTHOLDER);
            },
            child: Center(
              child: CircleAvatar(
                radius: 40,
                backgroundColor: AppColors.secondaryColor,
                child:
                    // con.ds.fullName == ''
                    // ? Text('')
                    // :
                    Text(
                  'Profile'.substring(0, 1).toUpperCase(),
                  style: const TextStyle(
                      color: AppColors.whiteColor,
                      fontSize: 60,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.01),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "con.ds.fullName",
                  style: AppTextStyle.semiBoldStyle.copyWith(
                      fontSize: Dimens.fontSize16, color: AppColors.whiteColor),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
