import 'package:e_kuteba/app/common/values/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

void ShowTopSuccessSnackBar(BuildContext context, String s) {
  showTopSnackBar(
    context,
    CustomSnackBar.success(
      message: s,
    ),
  );
}

void ShowTopErrorSnackBar(BuildContext context, String s) {
  showTopSnackBar(
    context,
    CustomSnackBar.error(
      backgroundColor: AppColors.secondaryColor,
      message: s,
    ),
  );
}
