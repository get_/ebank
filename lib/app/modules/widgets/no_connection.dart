import 'package:flutter/material.dart';
import 'package:get/get.dart';

Column NoConenction() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: const [
      Image(image: AssetImage('assets/images/no_connection.png')),
      Text(
        'No connection',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
      ),
      Text('Please check your internet connection!')
    ],
  );
}

Column InternalServerError() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      const Image(image: AssetImage('assets/images/intenal_error.png')),
      const Text(
        'OOOPS....',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
      ),
      Text('Internal Server Error!'.tr)
    ],
  );
}
