import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:e_kuteba/app/data/interface_controller/api_interface_controller.dart';
import 'package:e_kuteba/app/modules/widgets/custom_retry_widget.dart';

export 'package:e_kuteba/app/common/util/exports.dart';

// ignore: must_be_immutable
class BaseWidget extends GetView<ApiInterfaceController> {
  ///A widget with only custom retry button widget.
  final Widget child;

  BaseWidget({
    Key? key,
    required this.child,
  }) : super(key: key);
  @override
  var controller = Get.put(ApiInterfaceController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Stack(
        children: [
          Positioned.fill(
            child: child,
          ),
          Visibility(
            visible: controller.retry != null && controller.error != null,
            child: Positioned.fill(
              child: Scaffold(
                body: CustomRetryWidget(
                  onPressed: () {
                    controller.error = null;
                    controller.retry!.call();
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
