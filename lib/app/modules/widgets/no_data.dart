import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class NoDataPage extends StatefulWidget {
  const NoDataPage({Key? key}) : super(key: key);

  @override
  _NoDataPageState createState() => _NoDataPageState();
}

class _NoDataPageState extends State<NoDataPage>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  bool _isLoaded = false;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        setState(() {
          _isLoaded = true;
        });
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SizedBox(
          height: 220,
          width: 220,
          child: Lottie.asset(
            'assets/json/no-data.json',
            repeat: true,
            reverse: true,
            animate: true,
          ),
        ),
      ),
    );
  }
}