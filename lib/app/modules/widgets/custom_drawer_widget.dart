// ignore_for_file: prefer_const_constructors

import 'package:e_kuteba/app/common/constants.dart';
import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:e_kuteba/app/modules/basicDetail/controllers/basic_detail_controller.dart';
import 'package:e_kuteba/app/modules/basicDetail/views/basic_detail_view.dart';
import 'package:e_kuteba/app/modules/coopMemberSelf/views/member_basic_detail.dart';
import 'package:e_kuteba/app/modules/widgets/toast.dart';
import 'package:e_kuteba/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// ignore_for_file: non_constant_identifier_names

import 'custom_listtile_widget.dart';

Widget CustomDrawerWidget(BuildContext context, controller) {
  return ClipRRect(
    borderRadius: BorderRadius.vertical(
        top: Radius.circular(15.0), bottom: Radius.circular(5.0)),
    child: Container(
      width: 240,
      child: Drawer(
        child: Container(
          color: AppColors.primaryColor,
          child: Column(
            children: [
              DrawerHeader(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Get.to(() => BasicDetailView());
                        },
                        child: AvatarGlow(
                          endRadius: 80,
                          duration: Duration(seconds: 2),
                          glowColor: Colors.white24,
                          repeat: true,
                          repeatPauseDuration: Duration(seconds: 1),
                          startDelay: Duration(seconds: 1),
                          child: Material(
                            elevation: 30,
                            shape: CircleBorder(),
                            child: CircleAvatar(
                                backgroundColor: AppColors.whiteColor,
                                minRadius: 10,
                                maxRadius: 45,
                                child: Image(
                                  image: AssetImage('assets/images/avator.png'),
                                  width: 70,
                                )),
                          ),
                        ),
                      ),
                    ),
                    Center(
                      child: Text(
                        'Sheger'.tr,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16, color: AppColors.whiteColor),
                      ),
                    ),
                  ],
                ),
              ),
              // Divider(
              //   color: Colors.amberAccent,
              //   height: 10,
              // ),
              Expanded(
                child: ListView(scrollDirection: Axis.vertical, children: [
                  Container(
                    color: AppColors.whiteColor,
                    child: Column(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomListTileWidget(
                              titlecolor: Get.currentRoute == Routes.HOME
                                  ? Colors.orange.withOpacity(0.5)
                                  : Colors.green,
                              leading: FaIcon(
                                FontAwesomeIcons.house,
                                color: AppColors.Primarycolor_one,
                              ),
                              title: 'Home'.tr,
                              onTap: () {
                                Get.currentRoute == Routes.HOME
                                    ? Get.back()
                                    : Get.toNamed(Routes.HOME);
                              },
                            ),
                            CustomListTileWidget(
                              titlecolor: AppColors.white,
                              leading: FaIcon(FontAwesomeIcons.sackDollar,
                                  color: AppColors.Primarycolor_one),
                              title: 'Saving'.tr,
                              onTap: () {
                                Get.currentRoute == Routes.SAVING
                                    ? Get.back()
                                    : Get.toNamed(Routes.SAVING);
                              },
                            ),
                            CustomListTileWidget(
                              titlecolor: Get.currentRoute == Routes.CHILDREN
                                  ? Colors.orange.withOpacity(0.5)
                                  : Colors.green,
                              leading: Icon(
                                Icons.group,
                                color: AppColors.Primarycolor_one,
                              ),
                              title: 'Children'.tr,
                              onTap: () {
                                // AppointmentPage();
                                Get.currentRoute == Routes.CHILDREN
                                    ? Get.back()
                                    : Get.toNamed(Routes.CHILDREN);
                              },
                            ),
                            CustomListTileWidget(
                              titlecolor:
                                  Get.currentRoute == Routes.BENEFICIARIES
                                      ? Colors.orange.withOpacity(0.5)
                                      : AppColors.white,
                              leading: FaIcon(
                                FontAwesomeIcons.arrowsDownToPeople,
                                color: AppColors.Primarycolor_one,
                              ),
                              title: 'Beneficiary'.tr,
                              onTap: () {
                                // AppointmentPage();
                                Get.currentRoute == Routes.BENEFICIARIES
                                    ? Get.back()
                                    : Get.toNamed(Routes.BENEFICIARIES);
                              },
                            ),
                            CustomListTileWidget(
                              titlecolor:
                                  Get.currentRoute == Routes.COOP_SUBSCRIPTION
                                      ? Colors.orange.withOpacity(0.5)
                                      : AppColors.white,
                              leading: Icon(
                                Icons.subscriptions,
                                color: AppColors.Primarycolor_one,
                              ),
                              title: 'Subscription'.tr,
                              onTap: () {
                                // AppointmentPage();
                                Get.currentRoute == Routes.COOP_SUBSCRIPTION
                                    ? Get.back()
                                    : Get.toNamed(Routes.COOP_SUBSCRIPTION);
                              },
                            ),
                            CustomListTileWidget(
                              titlecolor:
                                  Get.currentRoute == Routes.COOP_PURCHASE
                                      ? Colors.orange.withOpacity(0.5)
                                      : AppColors.white,
                              leading: FaIcon(
                                FontAwesomeIcons.moneyCheck,
                                color: AppColors.Primarycolor_one,
                              ),
                              title: 'Purchase'.tr,
                              onTap: () {
                                // AppointmentPage();
                                Get.currentRoute == Routes.COOP_PURCHASE
                                    ? Get.back()
                                    : Get.toNamed(Routes.COOP_PURCHASE);
                              },
                            ),
                            CustomListTileWidget(
                              titlecolor:
                                  Get.currentRoute == Routes.APPROVED_LOANS
                                      ? Colors.orange.withOpacity(0.5)
                                      : AppColors.white,
                              leading: FaIcon(
                                FontAwesomeIcons.circleCheck,
                                color: AppColors.Primarycolor_one,
                              ),
                              title: 'Approved Loan'.tr,
                              onTap: () {
                                Get.currentRoute == Routes.APPROVED_LOANS
                                    ? Get.back()
                                    : Get.toNamed(Routes.APPROVED_LOANS);
                              },
                            ),
                            Divider(),
                          ],
                        ),
                        Container(
                            height: 40,
                            width: 200,
                            margin: EdgeInsets.only(bottom: 10),
                            child: ElevatedButton.icon(
                              icon: FaIcon(FontAwesomeIcons.commentDollar),
                              label: Text(
                                "Loan Request".tr,
                                style: TextStyle(
                                  color: AppColors.whiteColor,
                                  // letterSpacing: 1.5,
                                  // fontSize: 1,
                                ),
                              ),
                              style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10.0, vertical: 10.0),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  primary: AppColors.primaryColor),
                              onPressed: () {
                                Get.currentRoute == Routes.REGISTER_LOAN_REQUEST
                                    ? Get.back()
                                    : Get.toNamed(Routes.REGISTER_LOAN_REQUEST);
                              },
                            )),

                        // Container(
                        //     height: 40,
                        //     width: 200,
                        //     margin: EdgeInsets.only(bottom: 10),
                        //     child: ElevatedButton.icon(
                        //       icon: Icon(Icons.send_outlined),
                        //       label: Text(
                        //         "Payment".tr,
                        //         style: TextStyle(
                        //           color: AppColors.whiteColor,
                        //         ),
                        //       ),
                        //       style: ElevatedButton.styleFrom(
                        //           padding: EdgeInsets.symmetric(
                        //               horizontal: 10.0, vertical: 10.0),
                        //           shape: RoundedRectangleBorder(
                        //             borderRadius: BorderRadius.circular(10.0),
                        //           ),
                        //           primary: AppColors.primaryColor),
                        //       onPressed: () {
                        //         Get.toNamed(Routes.PAYMENT_IN_ONE);
                        //       },
                        //     )),
                        // Container(
                        //     height: 40,
                        //     width: 200,
                        //     margin: EdgeInsets.only(bottom: 10),
                        //     child: ElevatedButton.icon(
                        //       icon: Icon(Icons.schedule),
                        //       label: Text(
                        //         "Saving Request".tr,
                        //         style: TextStyle(
                        //           color: AppColors.whiteColor,
                        //           // letterSpacing: 1.5,
                        //           // fontSize: 1,
                        //         ),
                        //       ),
                        //       style: ElevatedButton.styleFrom(
                        //           padding: EdgeInsets.symmetric(
                        //               horizontal: 10.0, vertical: 10.0),
                        //           shape: RoundedRectangleBorder(
                        //             borderRadius: BorderRadius.circular(10.0),
                        //           ),
                        //           primary: AppColors.primaryColor),
                        //       onPressed: () {
                        //         Get.toNamed(Routes.SAVING_REQUEST);
                        //       },
                        //     )),

                        Container(
                            height: 40,
                            width: 200,
                            margin: EdgeInsets.only(bottom: 10),
                            child: ElevatedButton.icon(
                              icon: FaIcon(FontAwesomeIcons.moneyBillTransfer),
                              label: Text(
                                "Payment".tr,
                                style: TextStyle(
                                  color: AppColors.whiteColor,
                                  // letterSpacing: 1.5,
                                  // fontSize: 1,
                                ),
                              ),
                              style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10.0, vertical: 10.0),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  primary: AppColors.primaryColor),
                              onPressed: () {
                                Get.toNamed(Routes.PAYMENT_REQUEST);
                              },
                            )),
                        Container(
                            height: 40,
                            width: 200,
                            margin: EdgeInsets.only(bottom: 10),
                            child: ElevatedButton.icon(
                              icon: Icon(Icons.message),
                              label: Text(
                                "Send Request".tr,
                                style: TextStyle(
                                  color: AppColors.whiteColor,
                                ),
                              ),
                              style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10.0, vertical: 10.0),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  primary: AppColors.primaryColor),
                              onPressed: () {
                                Get.currentRoute == Routes.SUBMIT_REQUEST
                                    ? Get.back()
                                    : Get.toNamed(Routes.SUBMIT_REQUEST);
                              },
                            )),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.language,
                                color: AppColors.Primarycolor_one,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              DropdownButton<String>(
                                value: controller.chosenValue.value,
                                icon: const Icon(Icons.arrow_drop_down,
                                    color: AppColors.Primarycolor_one),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(
                                    color: AppColors.Primarycolor_one),
                                underline: Container(
                                  height: 2,
                                  color: AppColors.Primarycolor_one,
                                ),
                                onChanged: (String? newValue) {
                                  controller.chosenValue.value = newValue!;
                                  controller.updateLanguage();
                                  if (controller.chosenValue.value ==
                                      "English") {
                                    controller.langCtrl.changeLocale('English');
                                  } else if (controller.chosenValue.value ==
                                      "አማርኛ") {
                                    controller.langCtrl.changeLocale('አማርኛ');
                                  }
                                },
                                items: <String>[
                                  'English',
                                  'አማርኛ',
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                            ]),
                      ],
                    ),
                  )
                ]),
              ),

              ListTile(
                onTap: () => showDialog(
                    context: context,
                    builder: (context) => Dialog(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          elevation: 0,
                          backgroundColor: Colors.transparent,
                          child: contentBox(context, controller),
                        )),
                trailing: Icon(
                  Icons.logout,
                  color: AppColors.whiteColor,
                ),
                title: Text('Logout'.tr,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                      fontSize: 15,
                      color: AppColors.whiteColor,
                    ))),
              )
            ],
          ),
        ),
      ),
    ),
  );
}

contentBox(context, controller) {
  return Stack(
    children: <Widget>[
      Container(
        padding: EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
        // margin: EdgeInsets.only(top: 5),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  color: Colors.black, offset: Offset(0, 10), blurRadius: 10),
            ]),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            // Image(
            //   image: AssetImage(AppImages.logout),
            //   height: MediaQuery.of(context).size.height * 0.1,
            //   // width: MediaQuery.of(context).size.width * 0.1,
            // ),
            Text(
              'Confirm Logout'.tr,
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 15,
            ),
            Flexible(
              child: Text(
                'Are you sure you want to logout?'.tr,
                style: TextStyle(fontSize: 14),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 22,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Align(
                  alignment: Alignment.bottomRight,
                  child: TextButton(
                      onPressed: () {
                        controller.onLogoutClick();
                      },
                      child: Text(
                        'Confirm'.tr,
                        style: TextStyle(
                            fontSize: Dimens.fontSize18,
                            color: AppColors.primaryColor),
                      )),
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: TextButton(
                      onPressed: () {
                        Get.back();
                      },
                      child: Text(
                        'Cancel'.tr,
                        style: TextStyle(
                            fontSize: Dimens.fontSize18,
                            color: AppColors.primaryColor),
                      )),
                ),
              ],
            ),
          ],
        ),
      ),
    ],
  );
}
