// ignore_for_file: prefer_const_constructors

import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

AppBar buildAppBar(context, String title) {
  return AppBar(
    iconTheme:
        IconThemeData(color: Color.fromARGB(255, 255, 255, 255), opacity: 1),
    elevation: 0,
    backgroundColor: AppColors.primaryColor,
    title: Text(
      title.tr,
      style: TextStyle(color: AppColors.whiteColor),
    ),
  );
}
