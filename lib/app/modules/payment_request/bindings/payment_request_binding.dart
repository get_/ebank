import 'package:get/get.dart';

import '../controllers/payment_request_controller.dart';

class PaymentRequestBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PaymentRequestController>(
      () => PaymentRequestController(),
    );
  }
}
