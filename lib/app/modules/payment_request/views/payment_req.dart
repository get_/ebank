import 'package:e_kuteba/app/modules/payment_request/payment_request_model.dart';
import 'package:e_kuteba/app/modules/widgets/appbar.dart';
import 'package:e_kuteba/app/modules/widgets/custom_drawer_widget.dart';
import 'package:e_kuteba/app/modules/widgets/custom_top_snackbar.dart';
import 'package:e_kuteba/app/modules/widgets/date_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:lottie/lottie.dart';

import '../../../common/util/validators.dart';
import '../../../common/values/app_colors.dart';
import '../../basicDetail/controllers/basic_detail_controller.dart';
import '../../home/controllers/home_controller.dart';
import '../../saving_request/banks_model.dart';
import '../controllers/payment_request_controller.dart';

class PaymentReqView extends GetView<PaymentRequestController> {
  TextEditingController totalAmount = TextEditingController();

  TextEditingController crv = TextEditingController();
  TextEditingController purchaseAmount = TextEditingController();
  TextEditingController referance = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  AutovalidateMode autoValidate = AutovalidateMode.disabled;
  final hController = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
        appBar: buildAppBar(context, 'Payment Request'.tr),
        drawer: CustomDrawerWidget(context, hController),
        resizeToAvoidBottomInset: true,
        body: Theme(
            data: ThemeData(
                primarySwatch: Colors.orange,
                colorScheme:
                    ColorScheme.light(primary: AppColors.primaryColor)),
            child: SingleChildScrollView(
                child: Form(
                    key: _formKey,
                    autovalidateMode: autoValidate,
                    child: Column(children: [
                      Obx(() => controller.sending.value
                          ? const SizedBox(
                              height: 7,
                              child: LinearProgressIndicator(
                                backgroundColor: AppColors.primaryColor,
                                color: AppColors.Primarycolor_one,
                              ),
                            )
                          : const Visibility(
                              visible: false,
                              replacement: SizedBox.shrink(),
                              child: SizedBox(),
                            )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          controller: totalAmount,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Enter Total amount'.tr,
                          ),
                          validator: (value) {
                            return Validators.validateDouble(value);
                          },
                        ),
                      ),
                      Obx(() => CheckboxListTile(
                            title: Text('Pay for Saving'.tr),
                            activeColor: AppColors.Primarycolor_one,
                            checkColor: AppColors.primaryColor,
                            value: controller.payments['saving'],
                            onChanged: (newValue) {
                              controller.payments['saving'] = newValue;
                            },
                            controlAffinity: ListTileControlAffinity
                                .leading, //  <-- leading Checkbox
                          )),
                      Obx(() => controller.payments['saving']
                          ? controller.types.value.data.isEmpty
                              ? const Center(child: CircularProgressIndicator())
                              : SizedBox(
                                  height: height * 0.5,
                                  child: Container(
                                    margin: EdgeInsets.fromLTRB(width * 0.05,
                                        height * 0.02, width * 0.05, 0),
                                    child: ListView.builder(
                                        itemCount:
                                            controller.types.value.data.length,
                                        itemBuilder: (context, index) {
                                          return Card(
                                              color: AppColors.lightGray,
                                              child: Obx(
                                                () => SizedBox(
                                                  height: controller.visibles[
                                                          controller.types.value
                                                              .data[index]]
                                                      ? height * 0.26
                                                      : height * 0.12,
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: [
                                                      Obx(() {
                                                        return CheckboxListTile(
                                                          title: Text(controller
                                                              .types
                                                              .value
                                                              .data[index]
                                                              .name),
                                                          activeColor: AppColors
                                                              .primaryColor,
                                                          checkColor: AppColors
                                                              .Primarycolor_one,
                                                          value: controller
                                                                  .visibles[
                                                              controller
                                                                  .types
                                                                  .value
                                                                  .data[index]],
                                                          onChanged:
                                                              (newValue) {
                                                            controller.visibles[
                                                                    controller
                                                                        .types
                                                                        .value
                                                                        .data[index]] =
                                                                newValue;
                                                          },
                                                          controlAffinity:
                                                              ListTileControlAffinity
                                                                  .leading, //  <-- leading Checkbox
                                                        );
                                                      }),
                                                      Obx(() => controller
                                                                  .visibles[
                                                              controller
                                                                  .types
                                                                  .value
                                                                  .data[index]]
                                                          ? SizedBox(
                                                              width:
                                                                  width * 0.7,
                                                              child:
                                                                  TextFormField(
                                                                keyboardType:
                                                                    TextInputType
                                                                        .number,
                                                                controller:
                                                                    controller
                                                                            .inputControllers[
                                                                        index],
                                                                decoration:
                                                                    InputDecoration(
                                                                  border:
                                                                      OutlineInputBorder(),
                                                                  labelText:
                                                                      'amount'
                                                                          .tr,
                                                                ),
                                                                validator:
                                                                    (value) {
                                                                  return Validators
                                                                      .validateDouble(
                                                                          value);
                                                                },
                                                              ),
                                                            )
                                                          : const Visibility(
                                                              child: Text(''),
                                                              visible: false,
                                                            )),
                                                      const SizedBox(
                                                        height: 10,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ));
                                        }),
                                  ),
                                )
                          : Text('')),
                      Obx(() => CheckboxListTile(
                            title: Text('Pay for Loan'.tr),
                            activeColor: AppColors.primaryColor,
                            checkColor: AppColors.Primarycolor_one,
                            value: controller.payments['loan'],
                            onChanged: (newValue) {
                              controller.payments['loan'] = newValue;
                            },
                            controlAffinity: ListTileControlAffinity
                                .leading, //  <-- leading Checkbox
                          )),
                      Obx(() => controller.payments['loan']
                          ? controller.apprrovedLoans.isNotEmpty
                              ? Container(
                                  margin: EdgeInsets.fromLTRB(width * 0.05,
                                      height * 0.02, width * 0.05, 0),
                                  child: SizedBox(
                                    height: height * 0.3,
                                    child: ListView.builder(
                                        itemCount:
                                            controller.apprrovedLoans.length,
                                        itemBuilder: (context, index) {
                                          return Card(
                                              color: AppColors.lightGray,
                                              child: Obx(
                                                () => SizedBox(
                                                  height: controller
                                                              .visibleAprroved[
                                                          controller
                                                                  .apprrovedLoans[
                                                              index]]
                                                      ? height * 0.26
                                                      : height * 0.12,
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: [
                                                      Obx(() {
                                                        return CheckboxListTile(
                                                          title: Text(controller
                                                              .apprrovedLoans[
                                                                  index]
                                                              .approvedAmount
                                                              .toString()),
                                                          activeColor: AppColors
                                                              .primaryColor,
                                                          checkColor: AppColors
                                                              .Primarycolor_one,
                                                          value: controller
                                                                  .visibleAprroved[
                                                              controller
                                                                      .apprrovedLoans[
                                                                  index]],
                                                          onChanged:
                                                              (newValue) {
                                                            controller
                                                                .visibleAprroved[controller
                                                                    .apprrovedLoans[
                                                                index]] = newValue;
                                                          },
                                                          controlAffinity:
                                                              ListTileControlAffinity
                                                                  .leading, //  <-- leading Checkbox
                                                        );
                                                      }),
                                                      Obx(() => controller
                                                                  .visibleAprroved[
                                                              controller
                                                                      .apprrovedLoans[
                                                                  index]]
                                                          ? SizedBox(
                                                              width:
                                                                  width * 0.7,
                                                              child:
                                                                  TextFormField(
                                                                keyboardType:
                                                                    TextInputType
                                                                        .number,
                                                                controller:
                                                                    controller
                                                                            .loanInputControllers[
                                                                        index],
                                                                decoration:
                                                                    InputDecoration(
                                                                  border:
                                                                      OutlineInputBorder(),
                                                                  labelText:
                                                                      'amount'
                                                                          .tr,
                                                                ),
                                                                validator:
                                                                    (value) {
                                                                  return Validators
                                                                      .validateDouble(
                                                                          value);
                                                                },
                                                              ),
                                                            )
                                                          : const Visibility(
                                                              child: Text(''),
                                                              visible: false,
                                                            )),
                                                      const SizedBox(
                                                        height: 10,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ));
                                        }),
                                  ),
                                )
                              : Center(
                                  child: controller.isNotfetched.value
                                      ? CircularProgressIndicator()
                                      : SizedBox(
                                          height: 100,
                                          child: Lottie.asset(
                                            'assets/json/no-data.json',
                                            repeat: true,
                                            reverse: true,
                                            animate: true,
                                          ),
                                        ),
                                )
                          : Text('')),
                      Obx(() => CheckboxListTile(
                            title: Text('pay for purchase'),
                            activeColor: AppColors.primaryColor,
                            checkColor: AppColors.Primarycolor_one,
                            value: controller.payments['purchase'],
                            onChanged: (newValue) {
                              controller.payments['purchase'] = newValue;
                            },
                            controlAffinity: ListTileControlAffinity
                                .leading, //  <-- leading Checkbox
                          )),
                      Obx(() => controller.payments['purchase']
                          ? Container(
                              margin: EdgeInsets.fromLTRB(
                                  width * 0.05, height * 0.02, width * 0.05, 0),
                              child: SizedBox(
                                height: height * 0.35,
                                child: Column(
                                  children: [
                                    TextFormField(
                                      keyboardType: TextInputType.number,
                                      controller: purchaseAmount,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'amount'.tr,
                                      ),
                                      validator: (value) {
                                        return Validators.validateDouble(value);
                                      },
                                    ),
                                    SizedBox(
                                      height: height * 0.04,
                                    ),
                                    TextFormField(
                                      keyboardType: TextInputType.number,
                                      controller: crv,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Cash Recipt Voucher'.tr,
                                      ),
                                      validator: (value) {
                                        return Validators.validateEmpty(value);
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : Text('')),
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: Card(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.5,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5)),
                                  padding: const EdgeInsets.all(10),
                                  child: Obx(
                                    () => DropdownButton<Bank>(
                                      value:
                                          controller.selectedBank.value.id == ''
                                              ? null
                                              : controller.selectedBank.value,
                                      isDense: true,
                                      isExpanded: true,
                                      hint: Text('select bank'.tr),
                                      items: controller.banks.data
                                          .map((Bank item) {
                                        return DropdownMenuItem(
                                          value: item,
                                          child: Text(item.name,
                                              textAlign: TextAlign.start),
                                        );
                                      }).toList(),
                                      onChanged: (newValue) {
                                        controller.selectedBank.value =
                                            newValue!;
                                      },
                                    ),
                                  )),
                              Obx(() => Text(controller.startDate.value)),
                              IconButton(
                                  onPressed: () {
                                    controller.selectStartDate(Get.context!);
                                  },
                                  // tooltip: DateTime.now().toLocal().toString(),
                                  icon: const Icon(
                                    Icons.date_range,
                                    color: AppColors.primaryColor,
                                  )),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          controller: referance,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Transaction referance'.tr,
                          ),
                          validator: (value) {
                            return Validators.validateEmpty(value);
                          },
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    AppColors.primaryColor)),
                            onPressed: () {
                              Get.back();
                            },
                            child: Text('Cancel'.tr),
                          ),
                          const SizedBox(
                            width: 30,
                          ),
                          ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    AppColors.primaryColor)),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                // controller.sendRequest();
                                if (validate()) {
                                  if (controller.selectedBank.value.id != '') {
                                    if (purchase == null) {
                                      controller.sendRequest(
                                          savings, loans, null);
                                    } else {
                                      controller.sendRequest(
                                          savings, loans, purchase);
                                    }

                                    totalAmount.clear();
                                    crv.clear();
                                    purchaseAmount.clear();

                                    referance.clear();
                                  } else {
                                    ShowTopErrorSnackBar(
                                        context, 'Select bank'.tr);
                                  }
                                } else {
                                  ShowTopErrorSnackBar(
                                      context,
                                      'The payment amount is not equal to that on the receipt.'
                                          .tr);
                                }
                              } else {
                                autoValidate =
                                    AutovalidateMode.onUserInteraction;
                              }
                            },
                            child: Text('Submit'.tr),
                          ),
                        ],
                      )
                    ])))));
  }

  List<Saving> savings = [];
  List<Loan> loans = [];
  Purchase? purchase;
  final BasicDetailController _selfController = Get.find();
  validate() {
    bool isValid = false;
    double total = double.parse(totalAmount.text);
    double savingsTotal = 0;
    double loanTotal = 0;
    double purchaseAm = 0;
    if (controller.payments['saving']) {
      for (int i = 0; i <= controller.inputControllers.length - 1; i++) {
        if ((double.tryParse(controller.inputControllers[i].text) != null)) {
          Saving saving1 = Saving(
              bankId: controller.selectedBank.value.id,
              regDate: controller.startDateToEpoch,
              savingAmount:
                  double.parse(controller.inputControllers[i].text).toInt(),
              savingTypeId: controller.types.value.data[0].id,
              transactionReference: referance.text,
              remark: '');
          savings.add(saving1);
          savingsTotal += double.parse(controller.inputControllers[i].text);
        }
      }
    }
    if (controller.payments['purchase']) {
      if ((double.tryParse(purchaseAmount.text) != null)) {
        purchaseAm = double.parse(purchaseAmount.text);
        purchase = Purchase(
            bankId: controller.selectedBank.value.id,
            crv: crv.text,
            purchaseDate: startDateToEpoch.toDouble(),
            totalAmount: purchaseAm);
      }
    }
    if (controller.payments['loan']) {
      for (int i = 0; i <= controller.loanInputControllers.length - 1; i++) {
        if ((double.tryParse(controller.loanInputControllers[i].text) !=
            null)) {
          Loan loan = Loan(
              approvedLoanId: controller.apprrovedLoans[i].id,
              depositAmount:
                  double.parse(controller.loanInputControllers[i].text).toInt(),
              bankId: controller.selectedBank.value.id,
              regDate: controller.startDateToEpoch,
              forMonth: '',
              memberId: _selfController.member.value.id!,
              remark: '');
          loans.add(loan);
          loanTotal += double.parse(controller.loanInputControllers[i].text);
        }
      }
    }
    (total != savingsTotal + purchaseAm + loanTotal) ? isValid : isValid = true;
    return isValid;
  }
}
