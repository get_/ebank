import 'package:e_kuteba/app/modules/payment_request/payment_request_model.dart';
import 'package:e_kuteba/app/modules/saving_request/banks_model.dart';
import 'package:e_kuteba/app/modules/saving_request/saving_types_model.dart';
import 'package:e_kuteba/app/modules/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

import '../../../common/util/connection.dart';
import '../../../common/values/app_colors.dart';
import '../../../data/api_helper.dart';
import '../../../data/errors/api_error_model.dart';
import '../../approvedLoans/approved_loans_model.dart';

class PaymentRequestController extends GetxController {
  final ApiHelper _apiHelperImpl = Get.find();

  RxMap visibles = <SavingType, bool>{}.obs;

  @override
  void onInit() {
    getSavingTypes();
    getBanks();
    fetch();

    // apprrovedLoans.value = approvedLoansFromJson(jsonDecode(jsonString));
    super.onInit();
  }

  List loanInputControllers = [];
  List inputControllers = [];

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  Rx<SavingTypes> types = SavingTypes(data: [], total: 0).obs;

  getSavingTypes() {
    _apiHelperImpl.getSavingTypes().then((value) {
      if (value.statusCode == 200) {
        types.value = savingTypesFromJson(value.body);

        for (SavingType type in types.value.data) {
          visibles[type] = false;
        }
        types.value.data.forEach((type) {
          var textController = TextEditingController();
          inputControllers.add(textController);
        });
      }
    });
  }

  Map inputControllersMap = <SavingType, TextEditingController>{};
  Banks banks = Banks(data: [], total: 0);
  Rx<Bank> selectedBank =
      Bank(id: '', name: '', shortcode: '', description: '').obs;

  getBanks() {
    _apiHelperImpl.getBanks().then((value) {
      if (value.statusCode == 200) {
        banks = banksFromJson(value.body);
        banks.data.isNotEmpty ? selectedBank.value = banks.data[0] : null;
      }
    });
  }

  RxBool sending = false.obs;
  sendRequest(List<Saving> savings, List<Loan> loans, purchase) {
    sending.value = true;
    Payment pay = Payment(loans: loans, purchase: purchase, savings: savings);
    print(pay.toJson());
    _apiHelperImpl.paymentRequest(pay).then((value) {
      print(value.body);
      if (value.statusCode == 204) {
        sending.value = false;
        toast('Submited Successfully');
        Get.back();
      } else {
        toast('try again');
      }
    });
  }

  RxString startDate =
      DateFormat("dd-MM-yyyy").format(DateTime.now()).toString().obs;
  double startDateToEpoch = 0.0;
  Rx<DateTime> currentDate = DateTime.now().obs;
  Future<void> selectStartDate(BuildContext context) async {
    await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: DateTime(2020),
        lastDate: DateTime(2050),
        builder: (context, picker) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: const ColorScheme.dark(
                primary: AppColors.secondaryColor,
                onPrimary: Colors.white,
                surface: AppColors.secondaryColor,
                onSurface: Colors.white,
              ),
              dialogBackgroundColor: AppColors.primaryColor,
            ),
            child: picker!,
          );
        }).then((selectedDate) {
      if (selectedDate != null) {
        var parsedDate = DateTime.parse(selectedDate.toString());
        startDate.value = DateFormat("dd-MM-yyyy").format(parsedDate);
        var start_date = (selectedDate).millisecondsSinceEpoch;
        var start = (start_date / 1000).round();
        startDateToEpoch = start.toDouble();
      }
    });
  }

  RxMap visibleAprroved = <ApprovedLoans, bool>{}.obs;
  RxBool isNotfetched = true.obs;
  RxList<ApprovedLoans> apprrovedLoans = <ApprovedLoans>[].obs;
  fetch() async {
    try {
      check().then((intenet) async {
        if (intenet) {
          _apiHelperImpl.getApprovedLoans().then((value) {
            isNotfetched.value = false;
            if (value.statusCode!.clamp(200, 299) == value.statusCode) {
              apprrovedLoans.value = approvedLoansFromJson(value.body);
              for (ApprovedLoans approved in apprrovedLoans) {
                visibleAprroved[approved] = false;
              }
              apprrovedLoans.forEach((loan) {
                var textController = TextEditingController();
                loanInputControllers.add(textController);
              });
            } else if (value.statusCode!.clamp(500, 599) == value.statusCode) {
              var err = apiErrorFromJson(value.body);
              toast(err.msgs[0]);
            } else if (value.statusCode!.clamp(400, 499) == value.statusCode) {
              toast('Resource not found.');
            } else {
              toast('operation failed');
            }
          }, onError: (err) {});
        } else {
          toast('no connection!');
        }
      });
    } catch (e) {}
  }

  Map payments =
      <String, bool>{'saving': false, 'loan': false, 'purchase': false}.obs;
}
