// To parse this JSON data, do
//
//     final payment = paymentFromJson(jsonString);

import 'dart:convert';

Payment paymentFromJson(String str) => Payment.fromJson(json.decode(str));

String paymentToJson(Payment data) => json.encode(data.toJson());

class Payment {
  Payment({
    this.savings,
    this.loans,
    this.purchase,
  });

  List<Saving>? savings;
  List<Loan>? loans;
  Purchase? purchase;

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        savings:
            List<Saving>.from(json["savings"].map((x) => Saving.fromJson(x))),
        loans: List<Loan>.from(json["loans"].map((x) => x)),
        purchase: Purchase.fromJson(json["purchase"]),
      );

  Map<String, dynamic> toJson() => {
        "savings": List<dynamic>.from(savings!.map((x) => x.toJson())),
        "loans": List<dynamic>.from(loans!.map((x) => x)),
        "purchase": purchase == null ? null : purchase!.toJson(),
      };
}

class Purchase {
  Purchase({
    required this.bankId,
    required this.crv,
    required this.purchaseDate,
    required this.totalAmount,
  });

  String bankId;
  String crv;
  double purchaseDate;
  double totalAmount;

  factory Purchase.fromJson(Map<String, dynamic> json) => Purchase(
        bankId: json["bank_id"],
        crv: json["crv"],
        purchaseDate: json["purchase_date"].toDouble(),
        totalAmount: json["total_amount"],
      );

  Map<String, dynamic> toJson() => {
        "bank_id": bankId,
        "crv": crv,
        "purchase_date": purchaseDate,
        "total_amount": totalAmount,
      };
}

Loan loanFromJson(String str) => Loan.fromJson(json.decode(str));

String loanToJson(Loan data) => json.encode(data.toJson());

class Loan {
  Loan({
    required this.approvedLoanId,
    required this.bankId,
    required this.depositAmount,
    required this.forMonth,
    required this.memberId,
    required this.regDate,
    required this.remark,
  });

  String approvedLoanId;
  String bankId;
  int depositAmount;
  String forMonth;
  String memberId;
  double regDate;
  String remark;

  factory Loan.fromJson(Map<String, dynamic> json) => Loan(
        approvedLoanId: json["approved_loan_id"],
        bankId: json["bank_id"],
        depositAmount: json["deposit_amount"],
        forMonth: json["for_month"],
        memberId: json["member_id"],
        regDate: json["reg_date"].toDouble(),
        remark: json["remark"],
      );

  Map<String, dynamic> toJson() => {
        "approved_loan_id": approvedLoanId,
        "bank_id": bankId,
        "deposit_amount": depositAmount,
        "for_month": forMonth,
        "member_id": memberId,
        "reg_date": regDate,
        "remark": remark,
      };
}

class Saving {
  Saving({
    required this.bankId,
    required this.regDate,
    required this.savingAmount,
    required this.savingTypeId,
    required this.transactionReference,
    required this.remark,
  });

  String bankId;
  double regDate;
  int savingAmount;
  String savingTypeId;
  String transactionReference;
  String remark;

  factory Saving.fromJson(Map<String, dynamic> json) => Saving(
        bankId: json["bank_id"],
        regDate: json["reg_date"].toDouble(),
        savingAmount: json["saving_amount"],
        savingTypeId: json["saving_type_id"],
        transactionReference: json["transaction_reference"],
        remark: json["remark"],
      );

  Map<String, dynamic> toJson() => {
        "bank_id": bankId,
        "reg_date": regDate,
        "saving_amount": savingAmount,
        "saving_type_id": savingTypeId,
        "transaction_reference": transactionReference,
        "remark": remark,
      };
}
