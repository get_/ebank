import 'package:get/get.dart';

import 'package:e_kuteba/app/modules/approvedLoans/bindings/approved_loans_binding.dart';
import 'package:e_kuteba/app/modules/approvedLoans/views/approved_loans_view.dart';
import 'package:e_kuteba/app/modules/basicDetail/bindings/basic_detail_binding.dart';
import 'package:e_kuteba/app/modules/basicDetail/views/basic_detail_view.dart';
import 'package:e_kuteba/app/modules/beneficiaries/bindings/beneficiaries_binding.dart';
import 'package:e_kuteba/app/modules/beneficiaries/views/beneficiaries_view.dart';
import 'package:e_kuteba/app/modules/children/bindings/children_binding.dart';
import 'package:e_kuteba/app/modules/children/views/children_view.dart';
import 'package:e_kuteba/app/modules/coopPurchase/bindings/coop_purchase_binding.dart';
import 'package:e_kuteba/app/modules/coopPurchase/views/coop_purchase_view.dart';
import 'package:e_kuteba/app/modules/coopSubscription/bindings/coop_subscription_binding.dart';
import 'package:e_kuteba/app/modules/coopSubscription/views/coop_subscription_view.dart';
import 'package:e_kuteba/app/modules/dashboard/bindings/dashboard_binding.dart';
import 'package:e_kuteba/app/modules/dashboard/views/dashboard_view.dart';
import 'package:e_kuteba/app/modules/home/bindings/home_binding.dart';
import 'package:e_kuteba/app/modules/home/views/home_view.dart';
import 'package:e_kuteba/app/modules/payment_request/bindings/payment_request_binding.dart';
import 'package:e_kuteba/app/modules/payment_request/views/payment_req.dart';
import 'package:e_kuteba/app/modules/payment_request/views/payment_request_view.dart';
import 'package:e_kuteba/app/modules/registerLoanPayment/bindings/register_loan_payment_binding.dart';
import 'package:e_kuteba/app/modules/registerLoanPayment/views/register_loan_payment_view.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/bindings/register_loan_request_binding.dart';
import 'package:e_kuteba/app/modules/registerLoanRequest/views/register_loan_request_view.dart';
import 'package:e_kuteba/app/modules/registerSavingRequest/bindings/register_saving_request_binding.dart';
import 'package:e_kuteba/app/modules/registerSavingRequest/views/register_saving_request_view.dart';
import 'package:e_kuteba/app/modules/saving/bindings/saving_binding.dart';
import 'package:e_kuteba/app/modules/saving/views/saving_view.dart';

import '../modules/FAQ/bindings/faq_binding.dart';
import '../modules/FAQ/views/faq_view.dart';
import '../modules/coopPurchase/bindings/coop_purchase_binding.dart';
import '../modules/coopPurchase/views/coop_purchase_view.dart';
import '../modules/coopSubscription/bindings/coop_subscription_binding.dart';
import '../modules/coopSubscription/views/coop_subscription_view.dart';
import '../modules/dashboard/bindings/dashboard_binding.dart';
import '../modules/dashboard/views/dashboard_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/saving_request/bindings/saving_request_binding.dart';
import '../modules/saving_request/views/saving_request_view.dart';
import '../modules/submit_request/bindings/submit_request_binding.dart';
import '../modules/submit_request/views/submit_request_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  // ignore: constant_identifier_names
  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.COOP_PURCHASE,
      page: () => CoopPurchaseView(),
      binding: CoopPurchaseBinding(),
    ),
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.COOP_SUBSCRIPTION,
      page: () => CoopSubscriptionView(),
      binding: CoopSubscriptionBinding(),
    ),
    GetPage(
      name: _Paths.DASHBOARD,
      page: () => DashboardView(),
      binding: DashboardBinding(),
    ),
    // GetPage(
    //   name: _Paths.COOP_MEMBER_SELF,
    //   page: () => CoopMemberSelfView(),
    //   binding: CoopMemberSelfBinding(),
    // ),
    GetPage(
      name: _Paths.SUBMIT_REQUEST,
      page: () => SubmitRequestView(),
      binding: SubmitRequestBinding(),
    ),
    GetPage(
      name: _Paths.FAQ,
      page: () => FaqView(),
      binding: FaqBinding(),
    ),
    GetPage(
      name: _Paths.SAVING_REQUEST,
      page: () => SavingRequestView(),
      binding: SavingRequestBinding(),
    ),
    GetPage(
      name: _Paths.CHILDREN,
      page: () => ChildrenView(),
      binding: ChildrenBinding(),
    ),
    GetPage(
      name: _Paths.BENEFICIARIES,
      page: () => BeneficiariesView(),
      binding: BeneficiariesBinding(),
    ),
    GetPage(
      name: _Paths.SAVING,
      page: () => SavingView(),
      binding: SavingBinding(),
    ),
    GetPage(
      name: _Paths.APPROVED_LOANS,
      page: () => ApprovedLoansView(),
      binding: ApprovedLoansBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER_LOAN_REQUEST,
      page: () => RegisterLoanRequestView(),
      binding: RegisterLoanRequestBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER_LOAN_PAYMENT,
      page: () => RegisterLoanPaymentView(),
      binding: RegisterLoanPaymentBinding(),
    ),

    GetPage(
      name: _Paths.REGISTER_SAVING_REQUEST,
      page: () => RegisterSavingRequestView(),
      binding: RegisterSavingRequestBinding(),
    ),
    GetPage(
      name: _Paths.PAYMENT_REQUEST,
      page: () => PaymentReqView(),
      binding: PaymentRequestBinding(),
    ),
    // GetPage(
    //   name: _Paths.PROFILE,
    //   page: () => ProfileView(),
    //   binding: ProfileBinding(),
    // ),
    GetPage(
      name: _Paths.BASIC_DETAIL,
      page: () => BasicDetailView(),
      binding: BasicDetailBinding(),
    ),
  ];
}
