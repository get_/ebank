part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  // ignore: constant_identifier_names
  static const COOP_PURCHASE = _Paths.COOP_PURCHASE;
  // ignore: constant_identifier_names
  static const HOME = _Paths.HOME;
  // ignore: constant_identifier_names
  static const COOP_SUBSCRIPTION = _Paths.COOP_SUBSCRIPTION;
  // ignore: constant_identifier_names
  static const DASHBOARD = _Paths.DASHBOARD;
  // ignore: constant_identifier_names
  // static const COOP_MEMBER_SELF = _Paths.COOP_MEMBER_SELF;
  // ignore: constant_identifier_names
  static const BENEFICIARY = _Paths.BENEFICIARY;
  // ignore: constant_identifier_names
  static const SUBMIT_REQUEST = _Paths.SUBMIT_REQUEST;
  // ignore: constant_identifier_names
  static const APPROVE_APPOINTMENT = _Paths.APPROVE_APPOINTMENT;
  static const FAQ = _Paths.FAQ;
  static const SAVING_REQUEST = _Paths.SAVING_REQUEST;
  static const CHILDREN = _Paths.CHILDREN;
  static const BENEFICIARIES = _Paths.BENEFICIARIES;
  static const SAVING = _Paths.SAVING;
  static const APPROVED_LOANS = _Paths.APPROVED_LOANS;
  static const REGISTER_LOAN_REQUEST = _Paths.REGISTER_LOAN_REQUEST;
  static const REGISTER_LOAN_PAYMENT = _Paths.REGISTER_LOAN_PAYMENT;
  static const PAYMENT = _Paths.PAYMENT;
  static const REGISTER_SAVING_REQUEST = _Paths.REGISTER_SAVING_REQUEST;
  static const WITHDRAWAL_REQUEST = _Paths.WITHDRAWAL_REQUEST;
  static const PAYMENT_REQUEST = _Paths.PAYMENT_REQUEST;
  // static const PROFILE = _Paths.PROFILE;
  static const BASIC_DETAIL = _Paths.BASIC_DETAIL;
  static const PAYMENT_IN_ONE = _Paths.PAYMENT_IN_ONE;
}

abstract class _Paths {
  // ignore: constant_identifier_names
  static const COOP_PURCHASE = '/coop-purchase';
  // ignore: constant_identifier_names
  static const HOME = '/home';
  // ignore: constant_identifier_names
  static const COOP_SUBSCRIPTION = '/coop-subscription';
  // ignore: constant_identifier_names
  static const DASHBOARD = '/dashboard';
  // ignore: constant_identifier_names
  // static const COOP_MEMBER_SELF = '/coop-member-self';
  // ignore: constant_identifier_names
  static const BENEFICIARY = '/beneficiary';
  // ignore: constant_identifier_names
  static const SUBMIT_REQUEST = '/submit-request';
  // ignore: constant_identifier_names
  static const APPROVE_APPOINTMENT = '/approve-appointment';
  static const FAQ = '/faq';
  static const SAVING_REQUEST = '/saving-request';
  static const CHILDREN = '/children';
  static const BENEFICIARIES = '/beneficiaries';
  static const SAVING = '/saving';
  static const APPROVED_LOANS = '/approved-loans';
  static const REGISTER_LOAN_REQUEST = '/register-loan-request';
  static const REGISTER_LOAN_PAYMENT = '/register-loan-payment';
  static const PAYMENT = '/payment';
  static const REGISTER_SAVING_REQUEST = '/register-saving-request';
  static const WITHDRAWAL_REQUEST = '/withdrawal-request';
  static const PAYMENT_REQUEST = '/payment-request';
  // static const PROFILE = '/profile';
  static const BASIC_DETAIL = '/basic-detail';
  static const PAYMENT_IN_ONE = '/payment-in-one';
}
