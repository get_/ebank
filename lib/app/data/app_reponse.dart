// ignore_for_file: avoid_classes_with_only_static_members

import 'dart:async';
import 'dart:convert';

import 'package:e_kuteba/app/common/util/exports.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:logger/logger.dart';

import 'errors/app_errors.dart';

class AppResponse {
  static T? getResponse<T>(Response<T> response) {
    final status = response.status;
    try {
      if (status.connectionError) {
        throw NoConnectionError();
      }

      if (status.isNotFound) {
        throw ApiError(
          message: 'The Server is not avaliable now!,please contact the owner',
        );
      }
      if (response.bodyString == null) {
        throw ApiError(
          message: 'error on sending reqest',
        );
      }
      final res = jsonDecode(response.bodyString!);
      if (response.isOk) {
        if (res is Map &&
            res['status'] != null &&
            ((res['status'] is bool && !res['status']) ||
                res['status'] is String && res['status'] != 'OK')) {
          if (res['msgs'][0] != null && res['msgs'][0].toString().isNotEmpty) {
            throw ApiError(
              message: res['msgs'][0]?.toString() ?? Strings.unknownError,
            );
          } else {
            throw ApiError(
              message: res['msgs'][0]?.toString() ?? Strings.unknownError,
            );
          }
        }
        return response.body;
      } else {
        if (status.isServerError) {
          throw ApiError();
        } else if (status.code == HttpStatus.requestTimeout) {
          throw TimeoutError();
        } else if (response.unauthorized) {
          throw UnauthorizeError(
            message: res['msgs'][0]?.toString() ?? Strings.unauthorize,
          );
        } else {
          if (res['msgs'][0]?.toString() == '') {
            throw ApiError(
              message: Strings.unknownError,
            );
          }
          throw ApiError(
            message: res['msgs'][0]?.toString() ?? Strings.unknownError,
          );
        }
      }
    } on FormatException catch (e) {
      throw ApiError(message: e.toString());
    } on TimeoutException catch (e) {
      throw TimeoutError(
        message: e.message?.toString() ?? Strings.connectionTimeout,
      );
    }
  }
}
