import 'dart:io';
import 'package:e_kuteba/app/modules/payment_request/payment_request_model.dart';
import 'package:e_kuteba/app/modules/saving/withdrawal_request_model.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

import '../common/constants.dart';
import '../common/storage/storage.dart';
import '../modules/registerLoanPayment/loan_payment_model.dart';
import '../modules/registerLoanRequest/loan_request_model.dart';
import 'api_helper.dart';

class ApiHelperImpl extends GetConnect with ApiHelper {
  @override
  void onInit() {
    // Storage.storage.write('token', 'goe5wre1duwfsrbrwkie0l8nxzr3t7');
    httpClient.baseUrl = Constants.BASE_URL;
    httpClient.timeout = Constants.timeout;
    addRequestModifier();
    addResponseModifier();
  }

  // Future<bool> checkConnection() async {
  //   bool connected = false;
  //   try {
  //     final result = await InternetAddress.lookup('example.com');
  //     if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
  //       connected = true;
  //     }
  //   } on SocketException catch (_) {
  //     connected = false;
  //   }
  //   return connected;
  // }

  // void addResponseModifier() {
  //   httpClient.addResponseModifier((request, response) {
  //     return response;
  //   });
  // }

  // void addRequestModifier() {
  //   httpClient.addRequestModifier<dynamic>((request) {
  //     Logger().d(Storage.getValue(Constants.TOKEN));
  //     if (Storage.hasData(Constants.TOKEN)) {
  //       request.headers['Authorization'] =
  //           'Bearer ' + Storage.getValue(Constants.TOKEN);
  //     }
  //     return request;
  //   });
  // }

  void addResponseModifier() {
    httpClient.addResponseModifier((request, response) {
      Logger().d(
          'status code----->: ${response.statusCode}\nDATA----->: ${response.body} ');
      printInfo(
        info: 'Status Code: ${response.statusCode}\n'
            'Data: ${response.bodyString?.toString() ?? ''}',
      );

      return response;
    });
  }

  void addRequestModifier() {
    httpClient.addRequestModifier<dynamic>((request) {
      if (Storage.hasData(Constants.TOKEN)) {
        Logger().d('TOKEN---->: ' + Storage.getValue(Constants.TOKEN));
        request.headers['Authorization'] =
            'Bearer ' + Storage.getValue(Constants.TOKEN);
      }
      String x = 'REQUEST ║ ${request.method.toUpperCase()}\n'.toString();
      var y = 'url: ${request.url}\n'.toString();
      var z = 'Headers: ${request.headers}\n'.toString();
      var xz = 'Body: ${request.files?.toString() ?? ''}\n'.toString();

      Logger().d(x + y + z + xz);

      printInfo(
        info: 'REQUEST ║ ${request.method.toUpperCase()}\n'
            'url: ${request.url}\n'
            'Headers: ${request.headers}\n'
            'Body: ${request.files?.toString() ?? ''}\n',
      );

      return request;
    });
  }

  @override
  Future<Response> getCoopMemberSelf() {
    return get('/member-self');
  }

  @override
  Future<Response> getCoopPurchase() {
    return get('/members-self/cooppurchases');
  }

  @override
  Future<Response> getCoopSubscription() {
    return get('/members-self/coopsubscriptions');
  }

  @override
  Future<Response> getCoopBeneficiaries() {
    return get('/members-self/beneficiarys');
  }

  @override
  Future<Response> submitRequest(request) {
    return post('/member-self/do', {
      "method": "request_general",
      "payload": {"request": request}
    });
  }

  Future<Response> getAllDoctors() {
    return get('/doctors?st=&ps=5&pi=0&physican_type=-1');
  }

  @override
  Future<Response> savingRequest(request) {
    return post('/member-self/do',
        {"method": "request_saving", "payload": request.toJson()});
  }

  @override
  Future<Response> getSavingTypes() {
    return get('/saving_types?st=&ps=50&pi=0&sc=name&so=asc');
  }

  @override
  Future<Response> getBanks() {
    return get('/coopbanks?st=&ps=50&pi=0&sc=name&so=asc');
  }

  @override
  Future<Response> getApprovedLoans() {
    return get("/member-self/approvedloans");
  }

  @override
  Future<Response> getBeneficiaries() {
    return get('/members-self/beneficiarys');
  }

  @override
  Future<Response> getSaving(page) {
    return get("/member-self/savings?st=&ps=50&pi=$page");
  }

  @override
  Future<Response> getchildren() {
    return get("/members-self/childs");
  }

  @override
  Future<Response> postRegisterLoanRequest(LoanRequestModel data) {
    Logger().d(data.toJson());
    return post('/member-self/do',
        {'method': 'loan_request', 'payload': data.toJson()});
  }

  @override
  Future<Response> postLoanRequest(LoanRequestModel data) {
    return post('/loanrequests', data.toJson());
  }

  @override
  Future<Response> withdrawalRequest(WithdrawalRequest wr) {
    return post('/member-self/do',
        {"method": "withdrawal_request", "payload": wr.toJson()});
  }

  @override
  Future<Response> paymentRequest(Payment pay) {
        Logger().d(pay.toJson());
    return post(
        '/member-self/do', {"method": "payment", "payload": pay.toJson()});
  }

  @override
  Future<Response> getMembers(page) {
    return get('/members?st=&ps=10&pi=$page');
  }

  @override
  Future<Response> getLoanTypes() {
    return get('/loantypes');
  }

  @override
  Future<Response> postRegisterLoanPayment(LoanPaymentModel data) {
    return post('/member-self/do', {
      "method": "loan_payment",
      "payload": data.toJson(),
    });
  }
}
