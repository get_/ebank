import 'package:e_kuteba/app/modules/saving_request/saving_request_model.dart';
import 'package:e_kuteba/app/modules/saving/withdrawal_request_model.dart';
import 'package:get/get.dart';

import '../modules/payment_request/payment_request_model.dart';
import '../modules/registerLoanPayment/loan_payment_model.dart';
import '../modules/registerLoanRequest/loan_request_model.dart';

export 'package:e_kuteba/app/common/util/extensions.dart';
export 'package:e_kuteba/app/common/util/utils.dart';

abstract class ApiHelper {
  Future<Response> getCoopSubscription();
  Future<Response> getCoopPurchase();
  Future<Response> getCoopMemberSelf();
  Future<Response> getCoopBeneficiaries();
  Future<Response> submitRequest(String request);
  Future<Response> savingRequest(SavingRequest sr);
  Future<Response> getSavingTypes();
  Future<Response> getBanks();
  Future<Response> getchildren();
  Future<Response> getApprovedLoans();
  Future<Response> getBeneficiaries();
  Future<Response> getSaving(page);
  Future<Response> withdrawalRequest(WithdrawalRequest wr);
  Future<Response> paymentRequest(Payment pay);
  Future<Response> getMembers(page);
  Future<Response> getLoanTypes();
  Future<Response> postRegisterLoanRequest(LoanRequestModel data);
  Future<Response> postLoanRequest(LoanRequestModel data);
  Future<Response> postRegisterLoanPayment(LoanPaymentModel data);
}
