import 'package:e_kuteba/app/modules/home/views/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:e_kuteba/app/common/util/initializer.dart';
import 'package:e_kuteba/app/routes/app_pages.dart';
import 'app/common/util/localization.dart';
import 'app/modules/widgets/base_widget.dart';

void main() {
  Initializer.instance.init(() {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      BoxConstraints(
        maxWidth: Get.width,
        maxHeight: Get.height,
      ),
      designSize: Get.size,
    );
    return GetMaterialApp(
      title: Strings.appName,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        unselectedWidgetColor: Color.fromARGB(255, 0, 0, 0),
        primarySwatch: Colors.purple,
      ),
      translations: LocaleString(),
      locale: const Locale('en', 'US'),
      home: const SplashScreen(),
      getPages: AppPages.routes,
      builder: (_, child) => BaseWidget(
        child: child ?? const SizedBox.shrink(),
      ),
    );
  }
}
